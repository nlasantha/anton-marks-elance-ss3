<?php
include_once("3c-events/includes/Calendar/Calendar.class.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		 <title>Events >> MarksMan Studio</title>	
	
    <?php echo Calendar::css(); ?>  
    <?php echo Calendar::js(); ?> 

<!-- load pshiner plugin code -->
    <script type="text/javascript" src="/AntonMarks/index.php/mysite/javascript/jquery.pixelentity.shiner.min.js"></script>
    
   <style> 
  
   body { background: #000 url('AntonMarks/index.php/themes/marksman/images/main-bg.jpg') top center no-repeat; padding-top: 5px; font: normal 12px/18px arial, helvetica, sans-serif;  ; color: #676767; }
      a { color: #fff;}
    .red { color: #ff0000;}
   .grey { color: #3f3f3f;}
      h1 { font-family: 'DistrictRegular'; text-align: center; font-size: 35px; color: #fff; }
 
/* Fonts
==============================================*/
@font-face {
    font-family: 'DistrictRegular';
    src: url('AntonMarks/index.php/themes/marksman/fonts/district-webfont.eot');
    src: url('AntonMarks/index.php/themes/marksman/fonts/district-webfont.eot?#iefix') format('embedded-opentype'),
         url('AntonMarks/index.php/themes/marksman/fonts/district-webfont.woff') format('woff'),
         url('AntonMarks/index.php/themes/marksman/fonts/district-webfont.ttf') format('truetype'),
         url('AntonMarks/index.php/themes/marksman/fonts/district-webfont.svg#DistrictRegular') format('svg');
    font-weight: normal;
    font-style: normal;

}       
  /* bread crumbs
===============================================*/       
        
   
ul, li {
	list-style-type:none;
	padding:0;
	margin:0;
	
	}		
#breadcrumbs {
	height:2.7em;
	border:0px solid #000000;
	}
#breadcrumbs li {
	float:left;
	line-height:2.7em;
	color:#ffffff;
	padding-left:.85em;
	}		
#breadcrumbs li a {
	background: url('AntonMarks/index.php/themes/marksman/images/main-nav-bg.png') no-repeat right center;
	display:block;
	padding:0 5px 0 0;
	}							
#breadcrumbs li a:link,
#breadcrumbs li a:visited {
	color:#ffffff;
	text-decoration:none;
	}	
a:link, a:visited,	
#breadcrumbs li a:hover,
#breadcrumbs li a:focus {
	color:#f00
	}
        
    /* Layout
===============================================*/
#HeaderWrapper {}
#Header { width: 1046px; margin: 0 auto; position: relative; height:150px; }


#Logo { position: absolute; top:-5px; left: -56px; }
#LogoText { position: absolute; top:46px; left: 157px; }

#MainNav { position: absolute; left: 156px; top: 100px; }
#footerNav { width:650px; text-align:center; margin:20px auto; font-size:12px; }

    
</style>  

<!-- initialing pshiner plugin -->
		<script>
			jQuery(document).ready(function () {
			$(".peShiner").peShiner({duration: 6, reverse: true});
			})
		</script>       
	
</head>


<body>
    
    <div id="HeaderWrapper">
    <div id="Header">
      
             <div id="Logo">
                 <div class="peShiner">
            <a href="#"><img src="AntonMarks/index.php/themes/marksman/images/logo.png" data-color="red" alt="Marksman Studios"/></a> </div>
        </div><!--end div#Logo -->
        <div id="LogoText">
            <a href="#"><img src="AntonMarks/index.php/themes/marksman/images/logo-text.png" alt="Marksman Studios"/></a>
        </div><!--end div#Logo -->
      
        
         <div id="MainNav">
          <ul id="breadcrumbs">
               <li><a href="http://www.anton-marks.com/AntonMarks/index.php/">HOME</a></li>
               <li><a href="http://www.anton-marks.com/AntonMarks/index.php/boomshot">BOOMSHOT</a></li>
               <li><a href="http://www.anton-marks.com/AntonMarks/index.php/photoshot">PHOTOSHOT</a></li>
              <li><a href="http://www.anton-marks.com/AntonMarks/index.php/screenshot">SCREENSHOT</a></li>
              <li><a href="http://www.anton-marks.com/AntonMarks/index.php/futureshot"> FUTURESHOT</a></li>
              <li><a href="http://www.anton-marks.com/AntonMarks/index.php/contact">CONTACT</a></li>
              <li><a href="http://www.anton-marks.com/AntonMarks/index.php/store">STORE</a></li>
              <li><a href="http://www.anton-marks.com/AntonMarks/index.php/about">ABOUT</a></li>
              
            </ul>   
        </div><!--end div#MainNav -->
        
       
    </div><!--end div#Header -->
		<br />
        <H1> Anton Marks Events</H1>

		
	
		

		
		<div id="c-events">
			<?php 
				$calendar_type='big'; //Possible value: mini, big, list
				include("3c-events/calendar.php"); 				
			?>
		</div>
		
		<br />
		<div id="c-events">
			<?php 
				$calendar_type='list'; //Possible value: mini, big, list
				include("3c-events/calendar.php"); 				
			?>
		</div>
		
		<script type="text/javascript" src="3c-events/assets/javascript/init.js"></script>
		
  <div id="footerNav">
          <ul id="breadcrumbs">
               <li><a href="http://www.anton-marks.com/AntonMarks/index.php/">HOME</a></li>
               <li><a href="http://www.anton-marks.com/AntonMarks/index.php/boomshot">BOOMSHOT</a></li>
               <li><a href="http://www.anton-marks.com/AntonMarks/index.php/photoshot">PHOTOSHOT</a></li>
               <li><a href="http://www.anton-marks.com/AntonMarks/index.php/screenshot">SCREENSHOT</a></li>
               <li><a href="http://www.anton-marks.com/AntonMarks/index.php/futureshot"> FUTURESHOT</a></li>
               <li><a href="http://www.anton-marks.com/AntonMarks/index.php/contact">CONTACT</a></li>
               <li><a href="http://www.anton-marks.com/AntonMarks/index.php/store">STORE</a></li>
               <li><a href="http://www.anton-marks.com/AntonMarks/index.php/about">ABOUT</a></li>
          
            </ul>   
      </div>

	</body>
</html>