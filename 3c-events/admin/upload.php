<?php
#### Name of this file: admin/upload.php 
#### Description: upload pictures, class Upload ad functions defined in includes/functions.php.
session_start();

require_once('../includes/config.php');
require_once('../includes/Functions.php');

//not yet connected? direction login.php
if(!isset($_SESSION['username']) || !isset($_SESSION['id_user']))
{
	header('Location: login.php');
}

$confirm='';

if(isset($_POST['add']))
{	
		//transfer the image to upload folder, and display a link to insert into the text field
		$upload = new Upload; //class ad functions defined in includes/functions.php
		$img = $upload->upimg('image', '../upload/');
	
		$confirm = '<span style="color:green; font-weight:bold">Link to picture:</span><br /><input type="text" size="60" value="'.PATH_CAL.'/upload/'.$img .'" />';
}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		 <title>Upload Your Pictures</title>
	</head>
	<body style="width:400px">
		<div id="cont" style="width:400px">
			<?php 
			if(strlen($confirm)>0)
			echo $confirm.'<br />';
			?>
			<form action="upload.php" method="post" enctype="multipart/form-data">
				<p style="text-align:center">
				<fieldset>						
						<label for="image">Attach a picture</label> 
						<input type="file" name="image" id="image" /><br /><br />
				</fieldset>
				<br />

				<div style="text-align:center">	
					<input type="submit" name="add" value="Upload" />
				</div>	
				</p>
			</form>	
		</div>
	</body>
</html>