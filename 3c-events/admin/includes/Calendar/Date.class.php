<?php
#### Name of this file: includes/Calendar/Date.class.php 
#### Description: an important class in the project :
#### regoupe common functions operating dates
#### converting dates in various formats
#### retrieve information from a date (year, month, day, hour)

	class Date
	{
		//days
		protected $days = array(
			'FR' => array(0=>"Dimanche",1=>"Lundi",2=>"Mardi",3=>"Mercredi",4=>"Jeudi",5=>"Vendredi",6=>"Samedi",7=>"Dimanche"),
			'EN' => array(0=>"Sunday",1=>"Monday",2=>"Tuesday",3=>"Wednesday",4=>"Thursday",5=>"Friday",6=>"Saturday",7=>"Sunday"),
			'NL' => array(0=>"Zondag",1=>"Maandag",2=>"Dinsdag",3=>"Woensdag",4=>"Donderdag",5=>"Vrijdag",6=>"Zaterdag",7=>"Zondag"),
			'ES' => array(0=>"Domingo",1=>"Lunes",2=>"Martes",3=>"Miércoles",4=>"Jueves",5=>"Viernes",6=>"Sábado",7=>"Domingo")
			);
		//month	
		protected $month = array(
			'FR' => array(1=>"Janvier",2=>"Février",3=>"Mars",4=>"Avril",5=>"Mai",6=>"Juin",7=>"Juillet",8=>"Août",9=>"Septembre",10=>"Octobre",11=>"Novembre",12=>"Décembre"),
			'EN' => array(1=>"January",2=>"February",3=>"March",4=>"April",5=>"May",6=>"June",7=>"July",8=>"August",9=>"September",10=>"October",11=>"November",12=>"December"),
			'NL' => array(1=>"Januari",2=>"Februari",3=>"Maart",4=>"April",5=>"Mei",6=>"Juni",7=>"Juli",8=>"Augustus",9=>"September",10=>"Oktober",11=>"November",12=>"December"),
			'ES' => array(1=>"Enero",2=>"Febrero",3=>"Marzo",4=>"Abril",5=>"Mayo",6=>"Junio",7=>"Julio",8=>"Agosto",9=>"Septiembre",10=>"Octubre",11=>"Noviembre",12=>"Diciembre")
			);
		//lang of date - value from includes/config.php
		protected $lng = LANG;
	
		public $date = "";
		public $shortDay = false;
		public $shortMonth = false;
		public $hour = false;
		public $shortHour = false;

		//returns the number of day of the week
		//the week starts on Monday (1) and ends on Sunday (7)
		protected function _getNoDayWeek($a_day){
			$day = strtolower($a_day);
			switch ($day)
			{
				case "lundi":		case "lun":
				case "monday":		case "mon":
				case "maandag":		case "maa":
				case "lunes":		case "lun":
					$num = 1;	break;
				case "mardi":		case "mar":
				case "tuesday":		case "tue":
				case "dinsdag":		case "din":
				case "martes":		case "mar":
					$num = 2;	break;
				case "mercredi":	case "mer":
				case "wednesday":	case "wed":
				case "woensdag":	case "woe":
				case "miercoles":   case "mie":
					$num = 3;	break;
				case "jeudi":		case "jeu":
				case "thursday":	case "thu":
				case "donderdag":	case "don":
				case "jueves":      case "jue":
					$num = 4;	break;
				case "vendredi":	case "ven":
				case "friday":		case "fri":
				case "vrijdag":		case "vrij":
				case "viernes":     case "vie":
					$num = 5;	break;
				case "samedi":		case "sam":
				case "saturday":	case "sat":
				case "zaterdag":	case "zat":
				case "sabado":      case "sab":
					$num = 6;	break;
				case "dimanche":	case "dim":
				case "sunday":		case "sun":
				case "zondag":		case "zon":
				case "domingo":     case "dom":
					$num = 7;	break;
			}
			return ($num);
		}
		
		//returns month name corresponding to month number
		protected function _num2Month($a_month)
		{
			$res = $this->shortMonth ? $this->monthC[$this->lng][$a_month] : $this->month[$this->lng][$a_month];
			return($res);
		}
	
		//returns month number corresponding to a month name
		protected function _month2Num($a_month)
		{
			$num="";
			switch (strtolower($a_month))
			{
				case "janvier":			case "jan":
				case "january":
				case "januari":
				case "enero":           case "ene":
					$num = "01";		break;
					
				case "fevrier":			case "février":			case "fev":			case "fév":
				case "february":		case "feb":
				case "februari":
				case "febrero":
					$num = "02";		break;
					
				case "mars":			case "mar":
				case "march":
				case "maart":
				case "marzo":
					$num = "03";		break;
					
				case "avril":			case "avr":
				case "april":			case "apr":
				case "abril":           case "abr":
					$num = "04";		break;
					
				case "mai":
				case "may":
				case "mei":
				case "mayo":
					$num = "05";		break;
					
				case "juin":			case "jun":
				case "june":
				case "juni":
				case "junio":
					$num = "06";		break;
					
				case "juillet":			case "jul":
				case "july":
				case "juli":
				case "julio":
					$num = "07";		break;
					
				case "aout":			case "août":			case "aou":			case "aoû":
				case "august":			case "aug":
				case "augustus":
				case "agosto":          case "ago":
					$num = "08";		break;
					
				case "septembre":		case "sep":
				case "septeber":
				case "september":
				case "septiembre":
					$num = "09";		break;
					
				case "octobre":			case "oct":
				case "october":
				case "oktober":
				case "octubre":
					$num = "10";		break;
					
				case "novembre":		case "nov":
				case "november":
				case "noviembre":
					$num = "11";		break;
					
				case "decembre":		case "décembre":		case "dec":			case "déc":
				case "december":
				case "diciembre":       case "dic":
					$num = "12";		break;
			}
			return($num);

		}
		
		/*
		* parses a date and returns an array:
		* array ['format']
		* array ['year']
		* array ['month']
		* array ['day']
		* array [time]
		* if the date is not specified, class use $date
		*/
		public function _getInfosDate($a_date="")
		{
			$patternFr = "/^(lundi|lun|mardi|mar|mercredi|mer|jeudi|jeu|vendredi|ven|samedi|sam|dimanche|dim)?";
			$patternFr .= "[[:space:]]*([\d]{1,2})";
			$patternFr .= "[[:space:]]*(janvier|jan|fevrier|février|fev|fév|mars|mar|avril|avr|mai|juin|jui|juillet|aout|août|aou|aoû|septembre|sep|octobre|oct|novembre|nov|decembre|décembre|dec|déc)";
			$patternFr .= "[[:space:]]*([\d]{2,4})";
			$patternFr .= "[[:space:]]?([\d]{2}:[\d]{2}(:[\d]{2})*)?";
			$patternFr .= "$/i";

			$patternEn = "/^(monday|mon|tuesday|tue|wednesday|wed|thursday|thu|friday|fri|saturday|sat|sunday|sun)?";
			$patternEn .= "[[:space:]]*([\d]{1,2}(st|nd|rd|th))";
			$patternEn .= "[[:space:]]*(january|jan|february|feb|march|mar|april|apr|may|june|july|august|aug|september|sep|october|oct|november|nov|december|dec)";
			$patternEn .= "[[:space:]]*([\d]{2,4})";
			$patternEn .= "[[:space:]]?([\d]{2}:[\d]{2}(:[\d]{2})*)?";
			$patternEn .= "$/i";
			
			$patternNl = "/^(maandag|maa|dinsdag|din|woensdag|woe|donderdag|don|vrijdag|vri|zaterdag|zat|zondag|zon)?";
			$patternNl .= "[[:space:]]*([\d]{1,2})";
			$patternNl .= "[[:space:]]*(januari|jan|februari|feb|maart|mar|april|apr|mei|juni|juli|augustus|aug|september|sep|oktober|okt|november|nov|december|dec)";
			$patternNl .= "[[:space:]]*([\d]{2,4})";
			$patternNl .= "[[:space:]]?([\d]{2}:[\d]{2}(:[\d]{2})*)?";
			$patternNl .= "$/i";
			
			$patternEs = "/^(lunes|lun|martes|mar|miercoles|mie|jueves|jue|viernes|vie|sabado|sab|domingo|dom)?";
			$patternEs .= "[[:space:]]*([\d]{1,2})";
			$patternEs .= "[[:space:]]*(enero|ene|febrero|feb|marzo|mar|abril|abr|mayo|may|junio|jun|julio|jul|agosto|ago|septiembre|sep|octubre|oct|noviembre|nov|diciembre|dic)";
			$patternEs .= "[[:space:]]*([\d]{2,4})";
			$patternEs .= "[[:space:]]?([\d]{2}:[\d]{2}(:[\d]{2})*)?";
			$patternEs .= "$/i";
		
			$res=array();
			$date = strlen($a_date)>0 ? $a_date : $this->date;

			// timestamp
			if (is_numeric($date))
			{
				$res['format'] = "UNX";
				$res['year'] = date("Y",$date);
				$res['month'] = date("m",$date);
				$res['day'] = date("j",$date);
				$res['hour'] = date("H:i:s",$date);
			}
			// SQL 					2008-08-11 15:30:21
			else if (preg_match("/^([\d]{4})-([\d]{2})-([\d]{2})( [\d]{2}:[\d]{2}:[\d]{2})?$/",$date,$l_date))
			{
				$res['format'] = "SQL";
				$res['year'] = $l_date[1];
				$res['month'] = $l_date[2];
				$res['day'] = $l_date[3];
				$res['hour'] = isset($l_date[4]) ? trim($l_date[4]) : "00:00:00";
			}
			// STR 					11/08/2008 ou 11/08/2008
			else if (preg_match("/^([\d]{1,2})\/([\d]{1,2})\/([\d]{2,4})([[:space:]]?[\d]{2}:[\d]{2}(:[\d]{2})*)?$/",$date,$l_date))
			{
				$res['format'] = "STR";
				$res['year'] = $l_date[3];
				$res['month'] = $l_date[2];
				$res['day'] = $l_date[1];
				$res['hour'] = isset($l_date[4]) ? trim($l_date[4]) : "00:00:00";
			}
			// RSS					Mon, 11 Aug 2008 14:18:58
			else if (preg_match("/^([\w]{3}), ([\d]{1,2}) ([\w]{1,3}) ([\d]{2,4}) ([\d]{2}:[\d]{2}:[\d]{2})$/i",$date,$l_date))
			{
				$res['format'] = "RSS";
				$res['year'] = $l_date[4];
				$res['month'] = $this->_month2Num($l_date[3]);
				$res['day'] = $l_date[2];
				$res['hour'] = isset($l_date[5]) ? trim($l_date[5]) : "00:00:00";
			}
			// FR 					lundi 11 aout 2008 14:18:58
			else if (preg_match($patternFr,$date,$l_date))
			{
				$res['format'] = "FR";
				$res['year'] = $l_date[4];
				$res['month'] = $this->_month2Num($l_date[3]);
				$res['day'] = $l_date[2];
				$res['hour'] = isset($l_date[5]) ? trim($l_date[5]) : "00:00:00";
			}
			// EN 					Monday 11th august 2008 14:18:58
			else if (preg_match($patternEn,$date,$l_date))
			{
				$res['format'] = "EN";
				$res['year'] = $l_date[4];
				$res['month'] = $this->_month2Num($l_date[3]);
				$res['day'] = $l_date[2];
				$res['hour'] = isset($l_date[5]) ? trim($l_date[5]) : "00:00:00";
			}
			// NL 					Maandag 11 augustus 2008 14:18:58
			else if (preg_match($patternNl,$date,$l_date))
			{
				$res['format'] = "NL";
				$res['year'] = $l_date[4];
				$res['month'] = $this->_month2Num($l_date[3]);
				$res['day'] = $l_date[2];
				$res['hour'] = isset($l_date[5]) ? trim($l_date[5]) : "00:00:00";
			}
			// ES 					Lunes 11 agosto 2008 14:18:58
			else if (preg_match($patternEs,$date,$l_date))
			{
				$res['format'] = "ES";
				$res['year'] = $l_date[4];
				$res['month'] = $this->_month2Num($l_date[3]);
				$res['day'] = $l_date[2];
				$res['hour'] = isset($l_date[5]) ? trim($l_date[5]) : "00:00:00";
			}
			return ($res);
		}

		public function __construct($a_date="",$a_format="")
		{
			$this->shortDay=false;
			$this->shortMonth=false;
			$this->hour=false;
			if (strlen($a_date)>0)
				$this->setDate($a_date,$a_format);
		}

		public function setLangue($a_langue){$this->lng=$a_langue;}

		public function setShortDay($a_bool=true){$this->shortDay = $a_bool;}

		public function setShortMonth($a_bool=true){$this->shortMonth = $a_bool;}

		public function setHour($a_bool=true){$this->hour = $a_bool;}

		public function setShortHour($a_bool=true){$this->hour = true;$this->shortHour = $a_bool;}

		public function getDateFrom($a_nbDays,$a_format="",$a_date="")
		{
			$l_date = strlen($a_date)>0 ? $a_date : $this->date;
			if (strlen($a_format)==0)
			{
				$format = $this->_getInfosDate($l_date);
				$a_format = $format['format'];
			}
			$unxDate = $this->convert("UNX",$l_date);
			$unxNewDate = $unxDate + $a_nbDays*24*60*60;
			return ($this->convert($a_format,$unxNewDate));
		}

		public function setDate($a_date,$a_format="")
		{
			$infos = $this->_getInfosDate($a_date);
			if ($infos['hour']!="00:00:00")
				$this->setHour();
			$this->date = $this->convert($a_format,$a_date);
		}

		public function getDate($a_format="")
		{
			$date = $this->convert($a_format);
			return ($date);
		}

		public function getYear($a_date="")
		{
			$date = strlen($a_date)>0 ? $a_date : $this->date;
			$infos = $this->_getInfosDate($date);
			if(isset($infos['year']))
			return($infos['year']);
		}

		public function getMonth($a_date="")
		{
			$date = strlen($a_date)>0 ? $a_date : $this->date;
			$date = $this->convert("UNX",$date);
			return(date("m",$date));
		}

		public function getNameMonth($a_date="")
		{
			$noMonth = intval($this->getMonth($a_date));
			return $this->month[$this->lng][$noMonth];
		}

		public function getWeek($a_date="")
		{
			$date = strlen($a_date)>0 ? $a_date : $this->date;
			$date = $this->convert("UNX",$date);
			return(date("W",$date));
		}

		public function getDay($a_date="")
		{
			$date = strlen($a_date)>0 ? $a_date : $this->date;
			$infos = $this->_getInfosDate($date);
			return($infos['day']);
		}

	/*	public function getNameDay($a_date="")
		{
			$date = strlen($a_date)>0 ? $a_date : $this->date;
			$date = $this->convert("UNX",$date);
			return ($this->shortDay ? $this->daysC[$this->lng][gmdate("w",$date)] : $this->days[$this->lng][gmdate("w",$date)]);
		}*/

		public function getFormat($a_date="")
		{
			$date = strlen($a_date) ? $a_date : $this->date;
			$infos = $this->_getInfosDate($date);
			return($infos['format']);
		}

		public function isLeap($a_year="")
		{
			$date = strlen($a_year) ? gmmktime(1, 0, 0, 1, 1, $a_year) : $this->convert("UNX",$this->date);
			return(date("L",$date));
		}

		public function getDaysMonth($a_month="", $a_year="")
		{
			if (strlen($a_month)>0)
				$month = ($a_month>=1 && $a_month<=12) ? $a_month : $this->_month2Num($a_month);

			$date = strlen($a_month)>0 ? gmmktime(12, 0, 0, intval($month), 1, (strlen($a_year)>0 ? $a_year : $this->getYear())) : $this->convert("UNX",$this->date);
			return (date("t",$date));
		}
		
		/*
		* returns the date of a day (Monday, Tuesday ...) of the week
		* $a_week = number of week
		* $a_year = year
		* $a_day = searched day (default Monday)
		* $a_format = unix default
		*/
		public function getDayWeek($a_week,$a_day="lundi",$a_format="UNX",$a_year="")
		{
			if (strlen($a_year)==0)
				$a_year = $this->getYear();
			if(strftime("%W",gmmktime(0,0,0,01,01,$a_year))==1)
				$mon_mktime = gmmktime(0,0,0,01,(01+(($a_week-1)*7)),$a_year);
			else
				$mon_mktime = gmmktime(0,0,0,01,(01+(($a_week)*7)),$a_year);
			
			// January 4 is still in first week
			// we looking for the day from January 04 to calculate the offset
			$offset = (date("w",$mon_mktime)-1)*60*60*24;
			$quatreJan = date("w",gmmktime(1,0,0,1,4,$a_year));
		
			if ($quatreJan==0 || $quatreJan>=4)
			{
				if (date("w",gmmktime(1,0,0,1,1,$a_year))==1)
					$offset = $offset;
				else 
					$offset = $offset + (7*60*60*24);
			}
			else if (date("w",gmmktime(1,0,0,1,1,$a_year))==0)
			{
				
				$offset = $offset + (7*60*60*24);
			}	
			$noDay = $this->_getNoDayWeek($a_day)-1;
			$day = $mon_mktime - $offset + ($noDay*60*60*24);
			$date = $this->convert($a_format,$day);
			
			return ($date);
		}
		
		//number of days between two dates
		public function getDaysPeriod($a_dateFrom,$a_dateTo)
		{
			$a_dateFrom = $this->convert("UNX",$a_dateFrom);
			$a_dateTo = $this->convert("UNX",$a_dateTo);
			return ( (($a_dateTo-$a_dateFrom)/60/60/24) + 1);
		}
		
		/*
		* converting a date
		* $a_format : desired format (by default the same as the date)
		* $a_date : date to be converted if different from $date
		* SQL		:	YYYY-MM-JJ H:i:s
		* STR		:	JJ/MM/YYYY H:i:s
		* FR		:	Day JJ Mois YYYY
		* EN		:	Day JJth Mois YYYY
		* UNX		:	timestamp unix
		* URL		:	YYYY/MM/JJ
		* USR		:	MM/JJ/YYYY (for command linux useradd)
		* RSS		:	Mon, 11 Aug 2011 14:18:58 (RFC822)
		*/		
		public function convert($a_format="",$a_date="")
		{
			$res = false;

			$l_date = $this->_getInfosDate(strlen($a_date)>0 ? $a_date : $this->date);
			if (strlen($a_format)==0) $a_format = $l_date['format'];
			if (isset($l_date['format']))
			{
				$hours = explode(":",$l_date['hour']);			
				if ($this->hour)
					$timestamp = gmmktime($hours[0],$hours[1],(isset($hours[2]) ? $hours[2] : 0),$l_date['month'],$l_date['day'],$l_date['year']);
				else 
					$timestamp = gmmktime(0,0,0,$l_date['month'],$l_date['day'],$l_date['year']);
				switch($a_format)
				{
					case "SQL":
						$res = gmdate("Y-m-d".($this->hour ? " H:i:s": ""),$timestamp);
						break;
					case "STR":
						$res = gmdate("d/m/Y".($this->hour ? " H:i".(!$this->shortHour ? ":s": ""): ""),$timestamp);
						break;
					case "FR":
						$res =  $this->days['FR'][gmdate("w",$timestamp)] . gmdate(" d ",$timestamp) . 
								$this->month['FR'][gmdate("n",$timestamp)] . 
								gmdate(", Y".($this->hour ? " H:i".(!$this->shortHour ? ":s": ""): ""),$timestamp);
						break;
					case "EN":
						$day = gmdate(" d",$timestamp);
						if (preg_match("/1$/",$day))		$day = $day."st ";
						else if (preg_match("/2$/",$day))	$day = $day."nd ";
						else if (preg_match("/3$/",$day))	$day = $day."rd ";
						else $day = $day."th ";
						$res =  $this->days['EN'][gmdate("w",$timestamp)] . $day.
								$this->month['EN'][gmdate("n",$timestamp)] . 
								gmdate(", Y".($this->hour ? " H:i".(!$this->shortHour ? ":s": ""): ""),$timestamp);
						break;
					case "NL":
						$res =  $this->days['NL'][gmdate("w",$timestamp)] . gmdate(" d ",$timestamp) . 
								$this->month['NL'][gmdate("n",$timestamp)] . 
								gmdate(" Y".($this->hour ? " H:i".(!$this->shortHour ? ":s": ""): ""),$timestamp);
						break;
						case "ES":
						$day = gmdate(" d",$timestamp);
						if (preg_match("/1$/",$day))		$day = $day."st ";
						else if (preg_match("/2$/",$day))	$day = $day."nd ";
						else if (preg_match("/3$/",$day))	$day = $day."rd ";
						else $day = $day."th ";
						$res =  $this->days['EN'][gmdate("w",$timestamp)] .  $day.
								$this->month['EN'][gmdate("n",$timestamp)] . 
								gmdate(", Y".($this->hour ? " H:i".(!$this->shortHour ? ":s": ""): ""),$timestamp);
						break;
					case "UNX":
						$res = $timestamp;
						break;
					case "URL":
						$res = gmdate("Y/m/d",$timestamp);
						break;
					case "USR":
						$res = gmdate("m/d/Y",$timestamp);
						break;
					case "RSS":
						$res = gmdate("D, d M Y H:i:s",$timestamp);
						break;
				}
			}
			return($res);
		}
}	
?>