<?php
#### Name of this file: includes/Calendar/Calendar.class.php 
#### Description: displays dates, events, month ...

$pathscript = isset($valid_ajax) ? '' : '3c-events/'; //path to script

require_once($pathscript.'includes/config.php');
include_once($pathscript."includes/Calendar/Date.class.php");
include_once($pathscript.'includes/lang/'.LANG.'.php');

class Calendar extends Date
{
	private $calName = "cal"; //calendar name, ability to generate multiple calendars in future versions of the script

	private $monthLink = ""; //activate links on the month
	private $weekLink = "";	//activate links on the week
	private $dayLink = ""; //activate links on the day
	private $type = ""; //calendar format : mini, big or events
	
	private $targetLinks = "";
	private $targetNavig = "";
	
	private $dMonth = true;	//display month
	private $cmonth = true;		
	private $cdays = true;
	private $cweeks = false;
	private $cevents = false; //display events

	private $activeMonthLink = false;	
	private $activeWeekLink = false;		
	private $activeDayLink = false;	
	private $futur = false; //activate future days		
	private $passe = false;	//activate days passed		
	private $current = false; //activate current day	
	private $dateMin = ""; //activate the minimum date						
	private $dateMax = "";	//activate the maximum date					
	
	private $events = array(); //list of events			
	private $ajax = false;	//activate ajax					
	private $ajaxDiv = "";	//name of the ajax div					
	private $ajaxScript = ""; // write ajax	script			

	//javascript function to update the calendar using Ajax
	private function _writeAjax()
	{
		global $pathscript;
		$initJS = file_get_contents($pathscript.'assets/javascript/init.js');
		$codeJs = "<script type=\"text/javascript\">function navig".$this->type."Calendar(year,month,type){";
		$codeJs .= "var ajax = new XHR();";
		$codeJs .= "ajax.appendData('year',year);";
		$codeJs .= "ajax.appendData('month',month);";
		$codeJs .= "ajax.appendData('type',type);";
		$codeJs .= "ajax.send('".$this->ajaxScript."');";
		$codeJs .= "ajax.complete = function (xhr){";
		$codeJs .= "document.getElementById('".$this->ajaxDiv."').innerHTML = xhr.responseText;".$initJS."}}</script>";
		print $codeJs;
	}
	
	//outputs the required css
	public static function css(){
		global $pathscript;
		$style = "<link rel='stylesheet' href='".$pathscript."assets/home-skin/calendar.css' media='screen' type='text/css' />\n<link rel='stylesheet' href='".$pathscript."assets/home-skin/plugin.css' type='text/css' media='screen' />\n<link rel='stylesheet' href='".$pathscript."assets/home-skin/color/".SKIN.".css' type='text/css' />";
		
		
		if(NOEVENT==1) $style .= '<style type="text/css">#eventscal div.noevent{ display:none; }</style>';
		
		return $style;
	}
	
	//outputs the required js
	public static function js(){
		global $pathscript;
		return '<script type="text/javascript" src="'.$pathscript.'assets/javascript/jquery-1.7.1.min.js" ></script>
		<script type="text/javascript" src="'.$pathscript.'assets/javascript/plugin.js"></script>
		<script type="text/javascript" src="'.$pathscript.'assets/javascript/tooltip.jquery.js"></script>';
	}

	/*
	* writing the various links
	* $a_link = page
	* a_mois = month number
	* a_year = 4-digit year
	* a_month = day number
	* a_week = week number
	*/
	private function _writeLink($a_link,$a_year,$a_month=0,$a_day=0,$a_week=0)
	{
		$link = PATH_CAL."/events.php?calname=".$this->calName."&amp;year=".$a_year;
		if ($a_month>0)
			$link .= "&amp;month=".$a_month;
		if ($a_day>0)
			$link .= "&amp;day=".$a_day;
		if ($a_week>0)
			$link .= "&amp;week=".$a_week;
		return($link);
	}

	public function __construct($a_name="", $type='mini')
	{
		$this->monthLink = basename($_SERVER['PHP_SELF'])."?";
		$this->weekLink = basename($_SERVER['PHP_SELF'])."?";
		$this->dayLink = basename($_SERVER['PHP_SELF'])."?";
		$this->type = $type;
		if (strlen($a_name)>0)
			$this->calName = $a_name;
	}

	public function setmonthLink ($a_link){$this->monthLink = $a_link;}
	
	public function setType ($type){$this->type = $type;}
	
	public function getType (){return $this->type;}

	public function setweekLink ($a_link){$this->weekLink = $a_link;}

	public function setdayLink ($a_link){$this->dayLink = $a_link;}

	public function setFormatdayLink ($a_format){$this->formatdayLink = $a_format;}

	public function setFormatweekLink ($a_format){$this->formatweekLink = $a_format;}
	
	public function setFormatmonthLink ($a_format){$this->formatmonthLink = $a_format;}

	public function setTargetLinks ($a_target=""){ if (strlen($a_target)>0) $this->targetLinks = " target=\"".$a_target."\"";}

	public function setTargetNavig ($a_target=""){ if (strlen($a_target)>0) $this->targetNavig = " target=\"".$a_target."\"";}

	public function setDateMin($a_date){$this->dateMin = parent::convert("UNX",$a_date);}
	
	public function setDateMax($a_date){$this->dateMax = parent::convert("UNX",$a_date);}

	/*
	* activate ajax
	* $a_divName = name of the div updated by ajax method
	* $a_scriptName = name of the php script generates the calendar
	*/
	public function activeAjax($a_divName,$a_scriptName)
	{
		$this->ajax = true;
		$this->ajaxDiv = $a_divName;
		$this->ajaxScript = $a_scriptName;
		$this->_writeAjax();
	}

	//show or hide the navigation on the months
	public function displayNavigMonth ($a_bool=true){$this->dMonth = $a_bool;}

	//display or not the bar of the month
	public function displayMonth ($a_bool=true)
	{
		$this->cmonth = $a_bool;
		if ($a_bool==false)
			$this->displayNavigMonth (false);
	}

	public function displayWeeks ($a_bool=true){ if($this->type=='mini') $this->cweeks = $a_bool;}

	public function displayDays ($a_bool=true){$this->cdays = $a_bool;}

	public function displayEvents (){$this->cevents = true;}
	
	public function activemonthLink (){$this->activeMonthLink = true;}

	public function activeWeeksLinks (){$this->activeWeekLink = true;}

	public function activeDaysLinks (){$this->activeDayLink = true;}

	public function activeDaysFuturs (){$this->futur = true;}

	public function activeDaysPasses (){$this->passe = true;}

	public function activeDayPresent (){$this->current = true;}

	public function activeDaysEvents (){$this->jevent = true;}

	/**
	* List of Events
	* events are stored in a array:
	* $m_events['AAAA-MM-JJ HH:MM:SS']['event']
	*/
	public function addEvent ($a_date,$a_event)
	{
		$date = parent::convert("SQL",$a_date);
		if (!isset($this->events[$date])) 
			$i=0;
		else 
			$i=count($this->events[$date]);
		$this->events[$date][$i]['event'] = $a_event;
		$this->displayEvents();
	}
	
	// Display the calendar of current month
	public function makeCalendar($a_year,$a_month,$a_type)
	{
		global $home;
		
		$codeEvent="";
		$onclick="";
		
		$this->setType($a_type);
		
		// If we does not display the present and future days, we disables the links on weeks
		if (!$this->futur && !$this->passe)
			$this->activeWeekLink = false;
	
		$date = mktime(0,0,0,$a_month,1,$a_year);
		$premJour = parent::_getInfosDate($date);
		$monthCur = intval($a_month);
		
		if ($this->dateMin)
		{
			$monthMin = intval(parent::getMonth($this->dateMin));
			$weekMin = sprintf("%s-%s",$this->getYear($this->dateMin),$this->getWeek($this->dateMin));
		}
		if ($this->dateMax)
		{
			$monthMax = intval(parent::getMonth($this->dateMax));
			$weekMax = sprintf("%s-%s",$this->getYear($this->dateMax),$this->getWeek($this->dateMax));
		}	

		// First day of month
		$decal = parent::_getNoDayWeek(date("l",$date));

		$max = parent::getDaysMonth($a_month,$a_year);
		if (intval($a_month)==1)
		{ 
			$mprec=12;
			$aprec=$a_year-1;
			$asuiv=$a_year;
			$msuiv=$a_month+1;
		}

		elseif ($a_month==12)
		{ 
			$msuiv=1;
			$asuiv=$a_year+1;
			$aprec=$a_year;
			$mprec=$a_month-1;
		}
		else 
		{
			$asuiv=$a_year;
			$aprec=$a_year;
			$msuiv=$a_month+1;
			$mprec=$a_month-1;
		}
		
		
		$code='';
		
		
		if($this->type=='big')
		{
			$code = "\n<div id='big-3c-events'>";
			if(ES==1)
				$code .= "
				<form onsubmit='return false;' action='' class='text_field' style='position:relative; top:36px'>\n
					<div>\n	
					<input type='text' style='width: 220px;' value='".$home[3]."' onclick=\"if(this.value=='".$home[3]."') this.value=''\" id='eventName' class='ac_input' />\n
					<input type='button' onclick='lookupAjax();' value='Get Value' style='display:none' />\n
					</div>\n
				</form>\n";
		}
		if($this->type=='list')
		{
			$code = "\n<div id='list-3c-events'>";
			if(ES==1)
				$code .= "
				<form onsubmit='return false;' action='' class='text_field' style='margin-bottom:10px'>\n
					<div>\n	
					<input type='text' style='width: 220px;' value='".$home[3]."' onclick=\"if(this.value=='".$home[3]."') this.value=''\" id='eventName' class='ac_input' />\n
					<input type='button' onclick='lookupAjax();' value='Get Value' style='display:none' />\n
					</div>\n
				</form>\n";
		}

		if($this->type=='mini')
			$code .=  "\n<div id='mini-3c-events'><div id='minical'>\n<div class='topmonth'>\n";
		elseif($this->type=='list')
			$code .=  "<div id='eventscal'>\n<div class='topmonth'>\n";
		elseif($this->type=='big')
			$code .=  "<div id='bigcal'>\n<div class='topmonth'>\n";
		$nbCols = $this->cweeks ? 8 : 7;

		// Display month navigation
		if ($this->cmonth)
		{
			if (isset($this->formatmonthLink))
				$monthLink = sprintf($this->formatmonthLink,$a_year,$a_month);
			else
				$monthLink = $this->_writeLink($this->monthLink,$a_year,$a_month);

			if ($this->ajax)
			{
				$monthLinkPrec = $monthLinkSuiv = "";
				$onclickPrec = " onclick=\"navig".$this->type."Calendar('".$aprec."','".$mprec."','".$this->getType()."');\"";
				$onclickSuiv = " onclick=\"navig".$this->type."Calendar('".$asuiv."','".$msuiv."','".$this->getType()."');\"";
			}
			else 
			{
				if (isset($this->formatmonthLink))
				{
					$monthLinkPrec = sprintf($this->formatmonthLink,$aprec,$mprec);
					$monthLinkSuiv = sprintf($this->formatmonthLink,$asuiv,$msuiv);
				}
				else
				{
					$monthLinkPrec = $this->_writeLink($this->monthLink,$aprec,$mprec);
					$monthLinkSuiv = $this->_writeLink($this->monthLink,$asuiv,$msuiv);
				}
				
				$onclickPrec = $onclickSuiv = "";
			}
			$colspan = $this->dMonth ?  ($nbCols-2) : $nbCols ;
			
			// Link to previous month
			if(!$this->dMonth)
				$code .= "";
				
			elseif (($this->dateMin && $this->convert("SQL",$date) <= $this->convert("SQL",$this->dateMin))	|| (!$this->passe && $this->convert("SQL",$date) <= gmdate("Y-m-d")))
				$code .= "<th class='mois'>&nbsp;</th>";	
				
			else
				$code .= "<a id='previousMonth' title='Previous Month' class='previous' ". (strlen($monthLinkPrec)>0 ? "href='".$monthLinkPrec."'" : "") .$onclickPrec.$this->targetNavig.">&nbsp;</a>";
	
			// Link to next month
			if(!$this->dMonth)
				$code .= "";
			elseif (
			($this->dateMax && $this->getDateFrom($max,"SQL",$date) >= $this->convert("SQL",$this->dateMax))
			|| (!$this->futur && $this->getDateFrom($max,"SQL",$date) >= gmdate("Y-m-d"))
			)
				$code .= "<th class='mois'>&nbsp;</th>";	
			else
				$code .= "<a id='nextMonth' title='Next Month' class='next'". (strlen($monthLinkSuiv)>0 ? "href='".$monthLinkSuiv."'" : "") .$onclickSuiv.$this->targetNavig.">&nbsp;</a>";	

			list($w, $h) = explode('x', POPSIZE); 
			
			$code .=  '<span class="cdate">'.($this->activeMonthLink ? "<a class='live-tipsy' title='".$home[0]."' rel='events[month]' href='".$monthLink."&amp;iframe=true&amp;width=$w&amp;height=$h'".$this->targetLinks.">" : "") . sprintf("%s %04d",$this->_num2Month($monthCur),$a_year).($this->activeMonthLink ? "</a>" : "").'</span>';	
		}
		
		if($this->type=='mini' || $this->type=='big')
			$code .=  "</div>\n<div id='cont'>\n<table>\n";
		elseif($this->type=='list')
			$code .=  "</div>\n";
		
		if($this->type=='mini')
			$code .=  "<tbody>\n";
			
		if ($this->cdays && $this->type=='mini')
		{
			$code .=  "<tr>\n";
			if ($this->cweeks)
				$code .=  "<th>&nbsp;</th>";
			for ($j=1; $j<=7; $j++)
			{
				$code .=  "<th class='day'>&nbsp;".mb_substr($this->days[$this->lng][$j],0,LETTER, 'UTF-8')."</th>\n";
			}
			$code .=  "</tr>\n";
		}
		
		if ($this->cdays && $this->type=='big')
		{
			$code .=  "<thead>\n<tr>\n";
			if ($this->cweeks)
				$code .=  "<th>&nbsp;</th>";
			for ($j=1; $j<=7; $j++)
			{
				$code .=  "<th class='day'>&nbsp;".$this->days[$this->lng][$j]."</th>\n";
			}
			$code .=  "</tr>\n</thead>\n\n<tbody>\n<tr>";
		}
		
		if ($this->cweeks)
		{
			$week = gmdate("Y-W",mktime(12,0,0,$a_month,1,$a_year));
			$noWeek = gmdate("W",mktime(12,0,0,$a_month,1,$a_year));
			$curWeek = gmdate("Y-W");
			$year = $a_month==12 && $week==1 ? $a_year+1 : $a_year;
			if (isset($this->formatweekLink))
				$weekLink = sprintf($this->formatweekLink,$a_year,$week);
			else
				$weekLink = $this->_writeLink($this->weekLink,$year,0,0,$noWeek);
			
			if (
			!$this->activeWeekLink
			|| (isset($this->dateMin,$weekMin) && $week < $weekMin)
			|| (isset($this->dateMax,$weekMax) && $week > $weekMax)
			|| (!$this->futur && $week > $curWeek) 
			|| (!$this->passe && $week < $curWeek)
			)
				$code .=  '<tr><td class="nmsem">' . $noWeek . '</td>';
			else
				$code .=  '<tr><th><a href="'.$weekLink.'"'.$this->targetLinks.'>' . $noWeek . '</a></th>';
		}
		
		$nmday=1;
		
		// Write days of month
		for ($i=1; $i<43; $i++)
		{
			$tab='';
			$day = $i-$decal+1;
			$dayStr = sprintf("%04d-%02d-%02d",$a_year,$a_month,$day);
			
			if(strlen($day)==1)
				$day='0'.$day;
			
			if(strlen($a_month)==1)
				$a_month='0'.$a_month;
			
			if(DATEFORMAT==1)
				$dayStr2= $a_month.'/'.$day.'/'.$a_year;
			else
				$dayStr2= $day.'/'.$a_month.'/'.$a_year;
			
			$dayUnx = parent::convert("UNX",$dayStr);
			
			if (isset($this->formatDayLink))
				$dayLink = sprintf($this->formatDayLink,$a_year,$a_month,$day);
			else
				$dayLink = $this->_writeLink($this->dayLink,$a_year,$a_month,$day);

			// Display days	
			if ($day>0 && $day<=$max)
			{
				$divname='';
				$event = false;
		
				if ($this->cevents && $this->type=='big')
				{
					if (isset($this->events[$dayStr]) && is_array($this->events[$dayStr]))
					{
						$divname = "event_".parent::convert("UNX",$dayStr);
						$codeEvent .= '<div id="data_'.$divname.'" class="event" style="display:none;">';
						$codeEvent .= '<div class="dtitle">'.$dayStr2.'</div>';
						foreach ($this->events[$dayStr] as $event)
						{
							$tab .= '<div class="namevent">'.$event['event'].'</div>';
							$tab .= "\n";
						}
						$codeEvent .="</div>\n";
						$event = true;
					}
						$onclick = $event ? "class='live-tipsy' title='".$home[14]."' " : "";
				}
								
				if ($this->cevents && $this->type!='big')
				{
					if (isset($this->events[$dayStr]) && is_array($this->events[$dayStr]))
					{
						$divname = "event_".parent::convert("UNX",$dayStr);
						$codeEvent .= '<div id="data_'.$divname.'" class="event" style="display:none;">';
						$codeEvent .= '<div class="dtitle">'.$dayStr2.'</div>';
						foreach ($this->events[$dayStr] as $event)
						{
							$codeEvent .= '<div class="oev">'.$event['event'].'</div>';
							$tab .= '<li>'.$event['event'].'</li>';
						}
						$codeEvent .="</div>\n";
						$event = true;
					}
					if($this->type=='mini')
						$onclick = $event ? " class='tooltip4' id='$divname'" : "";
					else
						$onclick = $event ? "class='live-tipsy' title='".$home[14]."' " : "";
				}

				if($nmday==8) $nmday=1;				

				$mt = gmdate("D", mktime(12,0,0,$a_month,$day,$a_year)); 
				$mt = mb_substr($this->days[$this->lng][parent::_getNoDayWeek($mt)],0,3,'UTF-8');
				
				// HTML CODE
				if($this->type=='big')
				{
					$codeInactiveDay = "<td class=\"inactif\">".sprintf("%02d",$day)."</td>\n";					
					$codeActiveDay = "<td class=\"actif\"><div class='nday'>";
						
					if ($this->activeDayLink) 						
					$codeActiveDay .= "<a href=\"".$dayLink."\"".$this->targetLinks.">".sprintf("%02d",$day)."</a>";		
					else						
					$codeActiveDay .= sprintf("%02d",$day);			
					$codeActiveDay .= "</div></td>\n";

					$codeActiveDayToday = "<td class=\"today\"><div class='nday'>".sprintf("%02d",$day)."</div></td>\n";					
					
					//day with events
					$htmlEvent = "<td class=\"event\">";										
					$htmlEvent .= "<div class='nday'><a href=\"".$dayLink."&amp;iframe=true&amp;width=$w&amp;height=$h\" rel=\"events[event]\" ".$onclick.$this->targetLinks.">".sprintf("%02d",$day)."</a></div>".$tab;										
					$htmlEvent .= "</td>\n";											
					//today										
					$htmlEventToday = "<td class=\"today\">";					
					$htmlEventToday .= "<div class='nday'><a href=\"".$dayLink."&amp;iframe=true&amp;width=$w&amp;height=$h\" rel=\"events[event]\" ".$onclick.$this->targetLinks.">".sprintf("%02d",$day)."</a></div>".$tab;
					$htmlEventToday .= "</td>\n";
					$codeInactiveToday = "<td class=\"today\"><div class='nday'>".sprintf("%02d",$day)."</div></td>\n";	
				}				
				if($this->type=='mini')
				{
					$codeInactiveDay = "<td class='inactif'>".sprintf("%02d",$day)."</td>\n";
					$codeActiveDay = "<td class=\"actif\">";
					if ($this->activeDayLink) 
						$codeActiveDay .= "<a href=\"".$dayLink."\"".$this->targetLinks.">".sprintf("%02d",$day)."</a>";
					else
						$codeActiveDay .= sprintf("%02d",$day);
					$codeActiveDay .= "</td>\n";

					$codeActiveDayToday = "<td class='today'>".sprintf("%02d",$day)."</td>\n";
				
					//day with events
					$htmlEvent = '<td class="event">';
					$htmlEvent .= "<a href=\"".$dayLink."&amp;iframe=true&amp;width=$w&amp;height=$h\" rel='events[event]' ".$onclick.$this->targetLinks.">".sprintf("%02d",$day)."</a>";
					$htmlEvent .= "</td>\n";

					//today
					$htmlEventToday = "<td class='today'>";
					$htmlEventToday .= "<a href=\"".$dayLink."&amp;iframe=true&amp;width=$w&amp;height=$h\" rel=\"events[event]\" ".$onclick.$this->targetLinks.">".sprintf("%02d",$day)."</a>";

					$htmlEventToday .= "</td>\n";
					
					$codeInactiveToday = "<td class='today'>".sprintf("%02d",$day)."</td>\n";
				}
				elseif($this->type=='list')
				{
					$codeInactiveDay = "<div class='inactif'>".sprintf("%02d",$day)."</div>\n";
					$weekend=array("Sun", "Sat", "Sam", "Dim");
					if(in_array(mb_substr($mt,0,3, 'UTF-8'), $weekend))
						$codeActiveDay = "<div class='actif noevent' style='background:#f5f5f5'>";
					else	
						$codeActiveDay = "<div class='actif noevent'>";
						
					if ($this->activeDayLink) 
						$codeActiveDay .= "<div class='mt lgcal'><a href=\"".$dayLink."\"".$this->targetLinks." class='live-tipsy' title='".$home[14]."'>".$mt.'<br />'.sprintf("%02d",$day)."</a>";
					else
						$codeActiveDay .= '<div class="mt lgcal">'.$mt.'<br />'.sprintf("%02d",$day).'</div>';
					$codeActiveDay .= "</div>\n";

					if ($this->activeDayLink) $codeActiveDayToday = "<div class='actif'>";
					else $codeActiveDayToday = "<div class='actif noevent'>";
					

					$codeActiveDayToday .= "<div class='mt today lgcal'><a href=\"".$dayLink."\"".$this->targetLinks.">".$mt.'<br />'.sprintf("%02d",$day)."</a></div>";
						
					$codeActiveDayToday .= "</div>\n";
					
					// Day with events
					$htmlEvent = "<div class='actif event'>\n<div class='mt lgcal'>";
					$htmlEvent .= "<a href='".$dayLink."&amp;iframe=true&amp;width=$w&amp;height=$h' rel='events[event]' ".$onclick.$this->targetLinks.">".$mt.'<br />'.sprintf("%02d",$day)."</a></div>\n";
					$htmlEvent .= "<ul class='levent'>".$tab."</ul>\n</div>\n";

					// Today
					$htmlEventToday = "<div class='actif'>";
					$htmlEventToday .= "<div class='mt today lgcal' style='background:#FFFFE9'><a href='".$dayLink."&amp;iframe=true&amp;width=$w&amp;height=$h' rel='events[event]' ".$onclick.$this->targetLinks.">".$mt.'<br />'.sprintf("%02d",$day)."</a></div>";
					$htmlEventToday .= '<ul class="levent">'.$tab."</ul></div>\n";

					$codeInactiveToday = "<div class='actif today'>".sprintf("%02d",$day)."</div>\n";
				}
				
				// Display passed days
				if ($dayStr < date("Y-m-d") && $this->passe)
				{
					if ($this->dateMin && $dayStr < $this->convert("SQL",$this->dateMin))
						$code .= $codeInactiveDay;

					elseif (isset($this->jevent) && $event)
						$code .= $htmlEvent;

					else $code .= $codeActiveDay;
				}

				// Display futur days
				elseif ($dayStr > date("Y-m-d") && $this->futur)
				{
					if ($this->dateMax && $dayStr > $this->convert("SQL",$this->dateMax))
						$code .= $codeInactiveDay;

					elseif (isset($this->jevent) && $event)
						$code .= $htmlEvent;

					else $code .= $codeActiveDay;
				}
				// Display current days
				elseif ($dayStr == date("Y-m-d") && $this->current)
				{
					if ( ($this->dateMax && $dayUnx > $this->dateMax) || ($this->dateMin && $dayUnx < $this->dateMin) )
						$code .= $codeInactiveToday;

					elseif (isset($this->jevent) && $event)
						$code .= $htmlEventToday;
					else $code .= $codeActiveDayToday;
				}
				else 
					$code .= $codeInactiveDay;
			}
			elseif($this->type=='mini' || $this->type=='big')
				$code .= "<td class='inactif'>&nbsp;</td>\n";
			
			if ($i%(7)==0)
			{
				if($this->type=='mini' || $this->type=='big')
					$code .=  "</tr>\n";
				if ($i>=($max+$decal-1)) break;
					else{ if($this->type=='big') $code .=  "<tr>\n";}
					
				if ($this->cweeks && $i<42)
				{
					$week = gmdate("Y-W",mktime(12,0,0,$a_month,$day+1,$a_year));
					$noWeek = gmdate("W",mktime(12,0,0,$a_month,$day+1,$a_year));
					$curWeek = gmdate("Y-W");
					$year = $a_month==12 && $week==1 ? $a_year+1 : $a_year;
					
					if (isset($this->formatweekLink))
						$weekLink = sprintf($this->formatweekLink,$a_year,$week);
					else
						$weekLink = $this->_writeLink($this->monthLink,$year,0,0,$noWeek);
					
					if (
					!$this->activeWeekLink
					|| (isset($this->dateMin,$weekMin) && $week < $weekMin)
					|| (isset($this->dateMax,$weekMax) && $week > $weekMax)
					|| (!$this->futur && $week > $curWeek) 
					|| (!$this->passe && $week < $curWeek)
					)
						$code .=  "<tr>\n<td class='nmsem'>" . $noWeek . "</td>\n";
					else
						$code .=  "<tr>\n<th><a href='".$weekLink."'".$this->targetLinks.">" . $noWeek . "</a></th>\n";

				}
			}
		}
		
		if($this->type=='mini')
		{
			$code .=  "\n</tbody>\n</table></div>";

			if(ES==1)
				$code .= "
				<div id='cal_search' style='display:none'>\n
				<form onsubmit='return false;' action='' class='text_field'>\n
				<div>\n	<input type='text' style='width: 220px;' value='".$home[3]."' onclick=\"if(this.value=='".$home[3]."') this.value=''\" id='eventName' class='ac_input' />\n<input type='button' onclick='lookupAjax();' value='Get Value' style='display:none' />\n
				</div>\n</form>\n</div>";
				
			$code .="<div class='bevent'>";
			
			// Display search only if any event exist
			if ($this->cevents)
			{
				if(ES==1)
				{
					global $pathscript;
					$code .= "<div style='float:right; padding:3px 10px 0 0'><a href='#' class='live-tipsy' onclick='show_calsearch(); return false' title='".$home[1]."'><img src='".PATH_CAL."/assets/home-skin/img/search.png' alt='' style='border:0' /></a></div>";
				}
				$code .= '<div id="c_events"><div class="event" id="ctip" style="display:block"><span class="evtip">'.$home[2].'</span></div>';
				$code .= $codeEvent;
				$code .= '</div>';
			}
			
			$code .= "</div></div></div>\n";
		}
		elseif($this->type=='list')
			$code .= "</div>\n";
		elseif($this->type=='big')
			$code .=  "\n</tbody>\n</table>\n</div>\n</div></div>";
			
			
		// Mini Calendar Size		
		if(MINISIZE!='254')
			$code.='<style type="text/css">
			
			#minical,#minical #cont, #minical table, #cal_search, #minical div.bevent, .text_field, .text_field input, #eventName{width: '.MINISIZE.'px !important}
			
			#minical table{width: '.(MINISIZE-6).'px !important}
			.ac_results{width: '.(MINISIZE-18).'px !important}
			
			#minical { background: url("'.PATH_CAL.'/includes/zoom.php?src=assets/home-skin/img/'.ucwords(strtolower(SKIN)).'/month.png&w='.MINISIZE.'&h=44&zc=0") no-repeat scroll 0 0 transparent;}
			
			#minical #cont { background: url("'.PATH_CAL.'/includes/zoom.php?src=assets/home-skin/img/bg-table.png&w='.MINISIZE.'&h=252&zc=0") no-repeat scroll 0 0 transparent;}
			
			#cal_search { background: url("'.PATH_CAL.'/includes/zoom.php?src=assets/home-skin/img/bg-search.png&w='.MINISIZE.'&h=252&zc=0") no-repeat scroll 0 0 transparent;}
			
			.text_field { background: url("'.PATH_CAL.'/includes/zoom.php?src=assets/home-skin/img/field_bg.png&w='.(MINISIZE-20).'&h=24&zc=0") no-repeat scroll 0 0 transparent;}
			
			#minical div.bevent { background: url("'.PATH_CAL.'/includes/zoom.php?src=assets/home-skin/img/bg-footer.png&w='.MINISIZE.'&h=34&zc=0") no-repeat scroll 0 0 transparent;}
			
			</style>';
			
		
		// Big Calendar Size
		if(BIGSIZE!='700')
			$code.='<style type="text/css">#big-3c-events, #bigcal, #bigcal #cont, #bigcal table{width: '.BIGSIZE.'px !important}</style>';	
		
			
		// Events List Size
		if(LISTSIZE!='500')
			$code.='<style type="text/css">#eventscal {width: '.LISTSIZE.'px !important}</style>';			

		// Custom Style
		if(CUSTOMSTYLE!='CUSTOMSTYLE')
			$code.='<style type="text/css">'.CUSTOMSTYLE.'</style>';	

			
		return $code;
	}
}	
?>