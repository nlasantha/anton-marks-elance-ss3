<?php

include('../config.php');
if ((isset($_POST['name'])) && (strlen(trim($_POST['name'])) > 0)) {
	$name = stripslashes(strip_tags($_POST['name']));
} else {$name = 'No name entered';}
if ((isset($_POST['email'])) && (strlen(trim($_POST['email'])) > 0)) {
	$email = stripslashes(strip_tags($_POST['email']));
} else {$email = 'No email entered';}
if ((isset($_POST['phone'])) && (strlen(trim($_POST['phone'])) > 0)) {
	$phone = stripslashes(strip_tags($_POST['phone']));
} else {$phone = 'No phone entered';}
if ((isset($_POST['message'])) && (strlen(trim($_POST['message'])) > 0)) {
	$message = stripslashes(strip_tags($_POST['message']));
} else {$message = 'No message entered';}
if ((isset($_POST['event'])) && (strlen(trim($_POST['event'])) > 0)) {
	$event = stripslashes(strip_tags($_POST['event']));
} 
ob_start();
?>
<html>
<head>
<style type="text/css">
</style>
</head>
<body>
<table width="550" border="1" cellspacing="2" cellpadding="2">
  <tr bgcolor="#ffffff">
    <td>Event Name</td>
    <td><?=$event;?></td>
  </tr>
  <tr bgcolor="#eeeeee">
    <td>Contact Name</td>
    <td><?=$name;?></td>
  </tr>
  <tr bgcolor="#ffffff">
    <td>Contact Email</td>
    <td><?=$email;?></td>
  </tr>
  <tr bgcolor="#eeeeee">
    <td>Contact Phone</td>
    <td><?=$phone;?></td>
  </tr>
    <tr bgcolor="#ffffff">
    <td>Message</td>
    <td><?=nl2br($message);?></td>
  </tr>
</table>
</body>
</html>
<?php
$body = ob_get_contents();

require("phpmailer.php");

$mail = new PHPMailer();

$mail->From     = $email;
$mail->FromName = $name;
$mail->AddAddress(CONTACT,USERNAME);

$mail->WordWrap = 50;
$mail->IsHTML(true);

$mail->Subject  =  "3c-events Contact form : ".$name;
$mail->Body     =  $body;
$mail->AltBody  =  "This is the text-only body";

$mail->Send();
?>