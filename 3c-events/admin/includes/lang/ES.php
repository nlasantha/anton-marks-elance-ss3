<?php

$home = array();
$home[0] = "Vea todos los eventos de este mes"; //See all events of this Month
$home[1] = "Buscar eventos"; //Search Events
$home[2] = "Deslice sobre la fecha marcada para ver eventos"; //Hover over a colored date to view events
$home[3] = "Buscar eventos por nombre"; //Search an event by name
$home[4] = "Eventos de"; //Events Of
$home[5] = "Lugar"; //Place
$home[6] = "Fecha"; //Date
$home[7] = "¿Tiene preguntas sobre este evento?"; //Any question about this Event?
$home[8] = "{up} Me gusta, {down} No"; //{up} Like it, {down} Don't
$home[9] = "Su nombre"; //Your Name
$home[10] = "Su e-mail"; //Your Email
$home[11] = "Teléfono"; //Your Phone
$home[12] = "Enviar"; //Send
$home[13] = "<h2 style='margin-top:0'>¡Correo enviado!</h2>Le responderemos pronto."; //<h2 style='margin-top:0'>Contact Form Submitted!</h2>We will be in touch soon.
$home[14] = "Click para ver todos los eventos de este día"; //Click to view all events of this day
$home[15] = "Lugar del evento con Mapas de Google"; //Event place with Google Map
$home[16] = "Tiempo"; //Time
$home[17] = "a"; //to
?>