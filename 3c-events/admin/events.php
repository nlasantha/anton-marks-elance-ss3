<?php
#### Name of this file: admin/events.php 
#### Description: Administration of the script, events management(add/edit). You have to login first.
session_start();

require_once('../includes/config.php');
require_once('../includes/Functions.php');

//not yet connected? direction login.php
if(!isset($_SESSION['username']) && !isset($_SESSION['id_user']))
{
	header('Location: login.php');
}

try
{
	$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
	$bdd = new PDO('mysql:host='.HOST.';dbname='.DB, DBUSER, DBPASS, $pdo_options); $bdd->query("SET NAMES UTF8"); 
}
catch(Exception $e)
{
		exit('Database Error : '.$e->getMessage());
}

$confirm='';
$error='';

// add/edit an event
if(isset($_POST['add']) && !empty($_POST['name']) && !empty($_POST['date']))
{		
	//attributes of the new event
	$id = intval($_POST['edit']);
	$name = safeText($_POST['name']);
	$place = safeText($_POST['place']);
	$content = slashe($_POST['content']);
	$date = safeText($_POST['date']);
	$stime = safeText($_POST['stime']);
	$etime = safeText($_POST['etime']);
	$icononly= isset($_POST['icononly']) ? 1 : 0;
	$repeat_day = isset($_POST['repeat_day']) ? 1 : 0;
	$repeat_month = isset($_POST['repeat_month']) ? 1 : 0;
	$repeat_year = isset($_POST['repeat_year']) ? 1 : 0;
	$map = isset($_POST['map']) ? 1 : 0;
	$forward = isset($_POST['forward']) ? 1 : 0;
	$id_user = isset($_POST['id_user']) ? intval($_POST['id_user']) : intval($_SESSION['id_user']);

	$icon=!empty($_POST['oldicon']) ? $_POST['oldicon'] : '';
	if(!empty($_FILES['icon']['name'])){		
		$upload = new Upload;		
		$icon = $upload->upimg('icon','../upload/');
	}	
	
	if(isset($_POST['remove'])) $icon='';
		
	$req = $bdd->prepare("SELECT autoapprove FROM 3ce_users WHERE id=:id");
	$req->execute(array('id' => intval($_SESSION['id_user'])));
	$status = $req->fetch();	
	$req->closeCursor();
	
	if($status['autoapprove']==1 || $_SESSION['id_user']==0)
		$status=1;
	elseif($status['autoapprove']==0)	
		$status=0;
	
	//update event, if id is defined (hidden field in the form, retrieved via $_GET['edit'])
	if(!empty($_POST['edit']))
	{	
		$req = $bdd->prepare('UPDATE 3ce_event set name=:name, icon=:icon, icon_only=:icononly, content=:content, place=:place, date=:date, stime=:stime, etime=:etime, repeat_day=:repeat_day, repeat_month=:repeat_month, repeat_year=:repeat_year, map=:map, id_user=:id_user WHERE id=:id');
		$req->execute(array(
			'name' => $name,
			'icon' => $icon,
			'icononly' => $icononly,
			'content' => $content,
			'place' => $place,
			'date' => $date,
			'stime' => $stime,
			'etime' => $etime,
			'id' => $id,
			'map' => $map,
			'repeat_day' => $repeat_day,
			'repeat_month' => $repeat_month,
			'repeat_year' => $repeat_year,
			'id_user' => $id_user,
			));
	
		if($forward && $status)
		{
			require("../includes/Mail/phpmailer.php");
			$mail = new PHPMailer();

			$mail->From = CONTACT;
			$mail->FromName = '';
			$mail->AddAddress(FORWARD,'');

			$mail->WordWrap = 50;
			$mail->IsHTML(true);

			$title = str_replace('{name}', $name, $_POST['title']);
			$body = $_POST['template'];
			$body = str_replace('{name}', $name, $body);
			$body = str_replace('{link}', PATH_CAL.'/events.php?calname=cal&id='.$id, $body);
			$body = str_replace('{place}', $place, $body);
			$cuscontent = str_replace('src="', 'src="http://'.$_SERVER['HTTP_HOST'], $content);
			$body = str_replace('{content}', $cuscontent, $body);
			$body = str_replace('{date}', $date, $body);
			
			$mail->Subject  =  $title;
			$mail->Body     =  nl2br(stripslashes($body));
			$mail->AltBody  =  "";
			
			if(!$mail->Send()) {
				echo 'Mail Error';
			}
		}
	
		$confirm = 'Event Updated';	
	}
	//else if id event not defined (not edit), add a new event
	else
	{
		$req = $bdd->prepare('INSERT INTO 3ce_event(name, content, icon, icon_only, place, date, stime, etime, repeat_day, repeat_month, repeat_year, map, id_user, status) VALUES(:name, :content, :icon, :icononly, :place, :date, :stime, :etime, :repeat_day, :repeat_month, :repeat_year, :map, :id_user, :status)');
		$req->execute(array(
			'name' => $name,
			'content' => $content,
			'icon' => $icon,
			'icononly' => $icononly,
			'place' => $place,
			'date' => $date,
			'stime' => $stime,
			'etime' => $etime,
			'repeat_day' => $repeat_day,
			'repeat_month' => $repeat_month,
			'repeat_year' => $repeat_year,
			'map' => $map,
			'id_user' => $id_user,
			'status' => $status,
			));
	
		if($forward && $status)
		{
			require("../includes/Mail/phpmailer.php");
			$mail = new PHPMailer();

			$mail->From = CONTACT;
			$mail->FromName = '';
			$mail->AddAddress(FORWARD,'');

			$mail->WordWrap = 50;
			$mail->IsHTML(true);
			
			$title = str_replace('{name}', $name, $_POST['title']);
			$idev = $bdd->lastInsertId();	
			$body = $_POST['template'];
			$body = str_replace('{name}', $name, $body);
			$body = str_replace('{link}', PATH_CAL.'/events.php?calname=cal&id='.$idev, $body);
			$body = str_replace('{location}', $place, $body);
			$cuscontent = str_replace('src="', 'src="http://'.$_SERVER['HTTP_HOST'], $content);
			$body = str_replace('{content}', $cuscontent, $body);
			$body = str_replace('{date}', $date, $body);
			
			$mail->Subject  =  $title;
			$mail->Body     =  nl2br(stripslashes($body));
			$mail->AltBody  =  "";
			
			if(!$mail->Send()) {
				echo 'Mail Error';
			}
		}	
		
		header('Location: index.php?add=1');
	}	
}
elseif(isset($_POST['add']) && empty($_POST['name']))
{
	$error = 'Event name must not be empty';
}
elseif(isset($_POST['add']) && empty($_POST['date']))
{
	$error = 'Event date must not be empty';
}

// Form fields are filled with data already sent (from form)
$id=isset($_POST['id']) ? intval($_POST['id']) : '';
$name=isset($_POST['name']) ? safeText($_POST['name']): '';
$icon=isset($_POST['icon']) ? safeText($_POST['icon']): '';
$icononly=isset($_POST['icononly']) ? 1 : 0;
$place=isset($_POST['place']) ? safeText($_POST['place']): '';
$date=isset($_POST['date']) ? safeText($_POST['date']): '';
$stime=isset($_POST['stime']) ? safeText($_POST['stime']): '';
$etime=isset($_POST['etime']) ? safeText($_POST['etime']): '';
$content=isset($_POST['content']) ? safeText($_POST['content']): '';
$map=isset($_POST['map']) ? 1 : 0;
$repeat_day=isset($_POST['repeat_day']) ? 1 : 0;
$repeat_month=isset($_POST['repeat_month']) ? 1 : 0;
$repeat_year=isset($_POST['repeat_year']) ? 1 : 0;
$id_user=isset($_POST['id_user']) ? intval($_POST['id_user']) : '';

// Form fields are filled with data from the event to edit (from id event)
if(!empty($_GET['edit']))
{	
	$id = intval($_GET['edit']);
	if($_SESSION['id_user']==0)
	{
		$req = $bdd->prepare('SELECT * FROM 3ce_event WHERE id=:id');
			$req->execute(array(	
			'id' => $id,
			));
	}
	else
	{
		$req = $bdd->prepare('SELECT * FROM 3ce_event WHERE id=:id AND id_user=:id_user');
		$req->execute(array(	
		'id' => $id,
		'id_user' => $_SESSION['id_user']
		));
	}	

	$resmd = $req->fetch();	
	$req->closeCursor();
	$id=$resmd['id'];
	$name=$resmd['name'];
	$date=$resmd['date'];
	$icon=$resmd['icon'];
	$icononly=$resmd['icon_only'];
	$stime=$resmd['stime'];
	$etime=$resmd['etime'];
	$place=$resmd['place'];
	$content=$resmd['content'];
	$repeat_day=$resmd['repeat_day'];
	$repeat_month=$resmd['repeat_month'];
	$repeat_year=$resmd['repeat_year'];
	$map=$resmd['map'];
	$id_user=$resmd['id_user'];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8;charset=utf-8" /> 
	
	<title>3C-Events :: Administration</title>

	<link type="text/css" href="../assets/admin-skin/style.css" rel="stylesheet" />
	<link rel="stylesheet" href="../assets/home-skin/prettyPhoto.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" type="text/css" media="screen"/>
	
	<script type="text/javascript" src="../assets/javascript/jquery-1.7.1.min.js" ></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.9.1/jquery-ui.js" ></script>
	<script type="text/javascript" src="../assets/javascript/jquery-ui-timepicker-addon.js" ></script>
	
	<script type='text/javascript' src='../assets/javascript/custom.js'></script>
	<script type="text/javascript" src="../assets/javascript/tiny_mce/tiny_mce.js"></script>
	
	<style type="text/css">@import "../assets/admin-skin/jquery.datepick.css";</style> 
	<script type="text/javascript" src="../assets/javascript/jquery.datepick.pack.js"></script>

	<script src="../assets/javascript/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
	<script src="../assets/javascript/jquery.uniform.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="../assets/admin-skin/uniform.default.css" type="text/css" media="screen" charset="utf-8" />
	<script type="text/javascript" charset="utf-8">
      jQuery(function(){
        jQuery("input:text, input:radio, input:checkbox, textarea, select").uniform();
		jQuery('#stime').timepicker({
			timeFormat: "hh:mm tt"
		});
		jQuery('#etime').timepicker({
			timeFormat: "hh:mm tt"
		});
      });
    </script>

	<script type="text/javascript">
		tinyMCE.init({
			// General options
			mode : "exact",
			elements : "content",
			language : "en",
			height : "300",
			theme : "advanced",
			relative_urls : false,
			plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
			

			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Style formats
			style_formats : [
				{title : 'Bold text', inline : 'b'},
				{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
				{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
				{title : 'Example 1', inline : 'span', classes : 'example1'},
				{title : 'Example 2', inline : 'span', classes : 'example2'},
				{title : 'Table styles'},
				{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
			],

			formats : {
				alignleft : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'left'},
				aligncenter : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'center'},
				alignright : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'right'},
				alignfull : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'full'},
				bold : {inline : 'span', 'classes' : 'bold'},
				italic : {inline : 'span', 'classes' : 'italic'},
				underline : {inline : 'span', 'classes' : 'underline', exact : true},
				strikethrough : {inline : 'del'}
			}
		});
	</script>

	<script type="text/javascript">
	function tpl()
	{
		var tpl = document.getElementById('tpl').style;
		tpl.display= (tpl.display=='block') ? 'none' : 'block';
	}
	</script>
</head>

<body>


	<div id="container">
		<div id="bgwrap">
			<div id="primary_left">
				<div class="copy">
					Hello <?php echo $_SESSION['username']; ?> <a href="login.php?logout" style="color:#aaa">[Logout]</a><br />
					Script Version : <?php echo VERSION ?><br />
					Latest Version:
						<?php
								// Get Latest Version
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL, "http://freelanceonweb.com/3c-events/version.txt");
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								$output = curl_exec($ch);
								curl_close($ch);
								
								if($output!=VERSION) echo '<strong style="color:green">'.$output.'</strong>';
								else  echo $output;
						?>
					<br />
					<div style="text-align:center; margin-top:10px; font-size:11px">
						<a href="http://freelanceonweb.com/3c-events" style="color:#2F7ED7; text-decoration:none" target="_blank">&copy; 3c-events</a> - <a href="mailto:contact@freelanceonweb.com" style="color:#2F7ED7; text-decoration:none">Contact US</a>
					</div> 
					<?php if(file_exists('../install/index.php') && $_SESSION['id_user']==0) echo '<strong style="color:red">Please delete or rename install folder</strong>'?>
				</div>
				<div id="logo">
					<a href="<?php echo PATH_ADMIN ?>" title="Administration 3C-Events"><img src="../assets/admin-skin/img/logo.png" alt="" /></a>
				</div> 
				<div id="menu">
					<ul>
						<li><a href="index.php"><img src="../assets/admin-skin/img/events.png" alt="" /><span>Events</span></a></li>
						<li class="current"><a href="events.php"><img src="../assets/admin-skin/img/addevent.png" alt="" /><span class="current">Add Event</span></a></li>
						<?php if($_SESSION['id_user']==0) {?><li><a href="users.php"><img src="../assets/admin-skin/img/musers.png" alt="" /><span>Manage Users</span></a></li>
						<li><a href="settings.php"><img src="../assets/admin-skin/img/settings.png" alt="" /><span>Settings</span></a></li>
						<li><a href="database.php"><img src="../assets/admin-skin/img/db.png" alt="" /><span>Backup/Restore DB</span></a></li>
						<li><a href="http://freelanceonweb.com/forum"><img src="../assets/admin-skin/img/help.png" alt="" /><span>Support/Docs</span></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div id="primary_right">
				<div class="inner">
					<h1>Add/Edit Events</h1>
					
					<?php echo Ok($confirm).Error($error); ?>
						
					<form action="events.php<?php echo !empty($_GET['edit']) ? '?edit='.$_GET['edit'] : '' ?>" method="post" enctype="multipart/form-data">
						<input type="hidden" value="<?php echo $id;?>" name="edit" id="edit" />
						<input type="hidden" value="<?php echo $icon;?>" name="oldicon" />
	
						<label for="name" class="labeladm"><img src="../assets/admin-skin/img/name.png" alt="" style="vertical-align:middle; margin-right:5px"/><strong>Event Name</strong></label>
						<input type="text" name="name" id="name" value="<?php echo $name;?>" /><br /><br />

						<label for="place" class="labeladm"><img src="../assets/admin-skin/img/place.png" alt="" style="vertical-align:middle; margin-right:5px"/><strong>Event Location</strong></label>
						<input type="text" name="place" id="place" value="<?php echo $place;?>" /><br /><br />
						
						<label for="icon" class="labeladm"><img src="../assets/admin-skin/img/icon.png" alt="" style="vertical-align:middle; margin-right:5px"/><strong>Event Icon</strong></label>
						<input type="file" name="icon" id="icon" /><br /><br />
						
						<?php if(!empty($icon)) {
						list($w, $h) = explode('x', ICONSIZE); ?>
						<label class="labeladm">&nbsp;</label>
						<input type="checkbox" name="remove" /> Remove icon?<br /><br />
						<label class="labeladm">&nbsp;</label>
						<img src="../includes/zoom.php?w=<?php echo $w ?>&h=<?php echo $h ?>&src=upload/<?php echo $icon ?>" alt="icon" /><br /><br />
						<?php } ?>
						
						<label class="labeladm">&nbsp;</label>
						<input type="checkbox" name="icononly" <?php echo ($icononly==1) ? 'checked="checked"' : ''; ?> /> Display icon only?<br /><br />
						
						<label for="date" class="labeladm"><img src="../assets/admin-skin/img/date.png" alt="" style="vertical-align:middle; margin-right:5px" /><strong>Event Date</strong></label>
						
						<input type="text" name="date" id="date" value="<?php echo $date;?>" style="width:300px" /><br /><br />
						<script type="text/javascript">jQuery('#date').datepick({ 
							multiSelect: 999, monthsToShow: 2, 
							showTrigger: '#calImg'});
						</script>
						
						<label for="stime_h" class="labeladm"><img src="../assets/admin-skin/img/date.png" alt="" style="vertical-align:middle; margin-right:5px" /><strong>Start Time</strong></label>
						
						<input type="text" name="stime" id="stime" value="<?php echo $stime;?>" style="width:300px" /><br /><br />
						
						<label for="etime_h" class="labeladm"><img src="../assets/admin-skin/img/date.png" alt="" style="vertical-align:middle; margin-right:5px" /><strong>End Time</strong></label>
						
						<input type="text" name="etime" id="etime" value="<?php echo $etime;?>" style="width:300px" /><br /><br />
							
						
						<label for="repeat" class="labeladm"><img src="../assets/admin-skin/img/repeat.png" alt="" style="vertical-align:middle; margin-right:5px"/><strong>Repeat Event Every</strong></label>
	
		<input type="checkbox" name="repeat_day" id="repeat_day" <?php echo ($repeat_day==1) ? 'checked="checked"' : ''; ?> /> Day 		
		
			<input type="checkbox" name="repeat_month" id="repeat_month" <?php echo ($repeat_month==1) ? 'checked="checked"' : ''; ?> /> Month 
			
						<input type="checkbox" name="repeat_year" id="repeat_year" <?php echo ($repeat_year==1) ? 'checked="checked"' : ''; ?> /> Year <br /><br /><br />			
						<a href='upload.php?&iframe=true&width=450&height=300' rel="prettyPhoto" title="">Upload your pictures</a><br /><br />
						<textarea rows="2" cols="20" id="content" name="content"><?php echo $content;?></textarea><br />
						
						<label for="map" class="labeladm"><img src="../assets/admin-skin/img/map.png" alt="" style="vertical-align:middle; margin-right:5px" /><strong>Display Google Map?</strong></label>
						<input type="checkbox" name="map" id="map" <?php echo ($map==1) ? 'checked="checked"' : ''; ?> /> (If checked, Google map will be integrated into the event)<br /><br />

						<label for="forward" class="labeladm"><img src="../assets/admin-skin/img/email.png" alt="" style="vertical-align:middle; margin-right:5px" /><strong>Email Notification?</strong></label>
						<input type="checkbox" name="forward" id="forward" onchange="tpl()"/> (If checked, a notification will be sent to the email address you entered in the <a href="settings.php">Event Notification Settings</a>)<br /><br />

						<div id="tpl" style="display:none">	
						
						<label for="title" class="labeladm"><strong>Notification Title</strong></label>
						<input type="text" id="title" name="title" value="New Event : {name}" /><br /><br /> 
						
						<label for="template" class="labeladm"><strong>Notification Content</strong></label>
						<textarea rows="8" cols="70" style="border: 1px solid #CCCCCC;color: #505050;font-size: 13px;line-height: 13px;padding: 10px 5px;" id="template" name="template"><?php $tpl = @file_get_contents('../includes/Mail/template.php'); echo stripslashes($tpl)?></textarea><br /> 
						<div style="margin-left:150px; margin-top:5px">*HTML Allowed. <strong>Event Shortcode:</strong> {name}, {link}, {location}, {date}, {content}</div><br />
						
						</div>
						
						<?php if($_SESSION['id_user']==0) {?>
						<label for="id_user" class="labeladm"><strong>Author</strong></label>
						<select name="id_user" id="id_user">
							<option value="0"><?php echo USERNAME?></option>
						<?php
						$req = $bdd->query('SELECT * FROM 3ce_users WHERE status=1 ORDER BY username ASC');
						while ($row = $req->fetch())
						{
								if($id_user==$row['id'])
									echo '<option value="'.$row['id'].'" selected="selected">'.$row['username'].'</option>';
								else
									echo '<option value="'.$row['id'].'">'.$row['username'].'</option>';
						}
						?>		
						</select><br /><br /> 
						<?php } ?>
						<br />
						<input type="submit" name="add" value="<?php echo isset($resmd) ? 'Edit Event' : 'Add Event';?>" />
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'pp_default',slideshow:3000, autoplay_slideshow: false});
	});	
	</script>	
</body>
</html>