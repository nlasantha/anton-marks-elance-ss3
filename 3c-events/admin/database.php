<?php
#### Name of this file: admin/database.php 
#### Description: Import/Export Database 
session_start();

require_once('../includes/config.php');
require_once('../includes/Functions.php');

try
{
	$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
	$bdd = new PDO('mysql:host='.HOST.';dbname='.DB, DBUSER, DBPASS, $pdo_options); $bdd->query("SET NAMES UTF8"); 
}
catch(Exception $e)
{
		exit('Database Error : '.$e->getMessage());
}

// Not yet connected? direction login.php
if(!isset($_SESSION['username']) || !isset($_SESSION['id_user']) || $_SESSION['id_user']!=0)
{
	header('Location: login.php');
}

// Backup database
if(isset($_POST['backup_db'])){

	$cdate=date('Y-m-d-H_i_s');
	$file = 'sav-'.$cdate.'.sql';
	
	dump_MySQL(HOST, DBUSER, DBPASS, DB, 2, $file);
	$confirm = 'Database backup created successfully!';
}

// Actions on multiple choices of categories
if(isset($_POST['apply']) && !empty($_POST['choice']))
{	
	switch($_POST['action'])
	{
		case 1:
			$query = file_get_contents($_POST['choice']);
			$req = $bdd->exec($query);
			$confirm = 'Database restored successfully!';
			break;
		case 2:
			unlink($_POST['choice']);
			$confirm = 'Database deleted successfully!';
			break;	
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8;charset=utf-8" /> 
	
	<title>3C-Events :: Administration</title>

	<link type="text/css" href="../assets/admin-skin/style.css" rel="stylesheet" />
	
	<script type="text/javascript" src="../assets/javascript/jquery-1.7.1.min.js" ></script>
	<script type='text/javascript' src='../assets/javascript/custom.js'></script>
		<script src="../assets/javascript/jquery.uniform.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="../assets/admin-skin/uniform.default.css" type="text/css" media="screen" charset="utf-8" />
	<script type="text/javascript" charset="utf-8">
      jQuery(function(){
        jQuery("input:text, input:radio, input:checkbox, textarea, select").uniform();
      });
    </script>
</head>

<body>
	<div id="container">
		<div id="bgwrap">
			<div id="primary_left">
				<div class="copy">
					Hello <?php echo $_SESSION['username']; ?> <a href="login.php?logout" style="color:#aaa">[Logout]</a><br />
					Script Version : <?php echo VERSION ?><br />		
					Latest Version:
						<?php
								// Get Latest Version
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL, "http://freelanceonweb.com/3c-events/version.txt");
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								$output = curl_exec($ch);
								curl_close($ch);
								
								if($output!=VERSION) echo '<strong style="color:green">'.$output.'</strong>';
								else  echo $output;
						?>
					<br />
					<div style="text-align:center; margin-top:10px; font-size:11px">
						<a href="http://freelanceonweb.com/3c-events" style="color:#2F7ED7; text-decoration:none" target="_blank">&copy; 3c-events</a> - <a href="mailto:contact@freelanceonweb.com" style="color:#2F7ED7; text-decoration:none">Contact US</a>
					</div> 
					<?php if(file_exists('../install/index.php') && $_SESSION['id_user']==0) echo '<strong style="color:red">Please delete or rename install folder</strong>'?>
				</div>
				<div id="logo">
					<a href="<?php echo PATH_ADMIN ?>" title="Administration 3C-Events"><img src="../assets/admin-skin/img/logo.png" alt="" /></a>
				</div> 
				<div id="menu">
					<ul>
						<li><a href="index.php"><img src="../assets/admin-skin/img/events.png" alt="" /><span>Events</span></a></li>
						<li><a href="events.php"><img src="../assets/admin-skin/img/addevent.png" alt="" /><span>Add Event</span></a></li>
						<?php if($_SESSION['id_user']==0) {?><li><a href="users.php"><img src="../assets/admin-skin/img/musers.png" alt="" /><span>Manage Users</span></a></li>
						<li><a href="settings.php"><img src="../assets/admin-skin/img/settings.png" alt="" /><span>Settings</span></a></li>
						<li class="current"><a href="database.php"><img src="../assets/admin-skin/img/db.png" alt="" /><span>Backup/Restore DB</span></a></li>
						<li><a href="http://freelanceonweb.com/forum"><img src="../assets/admin-skin/img/help.png" alt="" /><span>Support/Docs</span></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div id="primary_right">
				<div class="inner">

					<h1>Backup / Restore Database</h1>

					<?php echo Ok($confirm); ?>
					
					<form action="database.php" method="post" id="form1">
					
						<fieldset>
							<legend>Backup List</legend>
						

						<input type="submit" name="backup_db" value="Create New Backup" />
						<div style="clear:both"></div><br />
					
						<table class="normal tablesorter" style="width:600px">
							<thead>
							<tr>
								<th>File</th>
								<th>Size</th>
								<th>Restore?</th>
							</tr>
							</thead>
							<tbody>
						<?php 
						if ($handle = opendir('sav')) {
							while (false !== ($file = readdir($handle))) {
							if($file!='..' && $file!='.' && $file!='index.html')
								$tab[] = $file;
							}
							closedir($handle);
						}

						if(!empty($tab)){
						rsort($tab);

						foreach($tab as $file){ 
							$infos = str_replace('sav-','',$file);
							$infos = str_replace('.sql','',$infos);
							$infos = explode('/',$infos);
						?>
							<tr <?php if($i++) echo 'class="odd"'; else echo 'class="even"';?>>
								<td style="text-align:center"><a href="<?php echo 'sav/'.$file; ?>" style="color:#E17009"><?php echo $file; ?></a></td>
								<td style="text-align:center"><?php echo (round(@filesize('sav/'.$file)/1048576*100)/100) ?> Mo</td>
								<td style="text-align:center"><input type="radio" name="choice" value="sav/<?php echo $file; ?>" style="width:20px" /></td>
							</tr>
							<?php if($i==2) $i=0; }} ?>
							</tbody>
						</table>	
					
						<div style="text-align:left; margin-top:5px; margin-left: 20px">
							<select name="action">
								<option selected="selected" value="0">Choose action...</option>
								<option value="1">Restore</option>
								<option value="2">Delete</option>
							</select>
							<input type="submit" name="apply" value="Apply" />
						</div>

						</fieldset>
					</form>
				</div>
			</div> 
		</div> 
	</div> 
	<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery(function(){
			jQuery("#update1").click(function(){
			jQuery(".error").hide();
			var hasError = false;
			var passwordVal = jQuery("#password").val();
			var checkVal = jQuery("#password1").val();
			if (passwordVal == '') {
				jQuery("#password").after('<span class="error">Please enter a password.</span>');
				hasError = true;
			} else if (checkVal == '') {
				jQuery("#password1").after('<span class="error">Please re-enter your password.</span>');
				hasError = true;
			} else if (passwordVal != checkVal ) {
				jQuery("#password1").after('<span class="error">Passwords do not match.</span>');
				hasError = true;
			}
			if(hasError == true) {return false;}
		});
	});

	});	
	</script>	
</body>
</html>