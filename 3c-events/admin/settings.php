<?php
#### Name of this file: admin/settings.php 
#### Description: Administration of the script, settings management (all values in config.php).
session_start();

//not yet connected? direction login.php
if(!isset($_SESSION['username']) || !isset($_SESSION['id_user']) || $_SESSION['id_user']!=0)
{
	header('Location: login.php');
}

//configuration file contents
$config = file_get_contents('../includes/config.php');

//function to update the configuration file
function regval($key, $value)
{
	global $config;
	$config = preg_replace('#define\("'.$key.'", "(.*)"\);#isU','define("'.$key.'", "'.$value.'");', $config);
	file_put_contents('../includes/config.php', $config);
}

$confirm='';

//update calendar settings, if his button is clicked
if(isset($_POST['update1']))
{	
	if($_POST['password']!='password' && ($_POST['password']==$_POST['password1']))
	{
		regval('PASSWORD', $_POST['password']);
	}
		regval('USERNAME', $_POST['login']);
		
	$confirm = 'Admin account updated successfully';
}

//update admin account, if his button is clicked
if(isset($_POST['update2']))
{
	regval('QUICKADMIN', $_POST['quickadmin']);
	regval('SKIN', $_POST['skin']);
	regval('LANG', strtoupper($_POST['lang']));
	regval('DATEFORMAT', $_POST['dateformat']);
	regval('EC', $_POST['enablecalendar']);
	regval('ES', $_POST['enablesearch']);
	regval('MINISIZE', $_POST['minisize']);
	regval('BIGSIZE', $_POST['bigsize']);
	regval('LISTSIZE', $_POST['listsize']);
	regval('POPSIZE', $_POST['popsize']);
	regval('ICONSIZE', $_POST['iconsize']);
	regval('WN', $_POST['weeknumber']);
	regval('NOEVENT', $_POST['noevent']);
	
	$confirm = 'Calendar settings updated successfully';
}

//update events settings, if his button is clicked
if(isset($_POST['update3']))
{
	regval('LETTER', $_POST['letter']);	
	regval('OLDER', $_POST['old']);	
	regval('CONTACT', $_POST['email']);	
	regval('FORWARD', $_POST['notification']);	
	regval('ECONTACT', $_POST['enablecontact']);
	regval('ESHARE', $_POST['enableshare']);
	regval('EVOTE', $_POST['enablevote']);
	$confirm = 'Events settings updated successfully';
}

//update script settings, if his button is clicked
if(isset($_POST['update4']))
{
	regval('HOST', $_POST['dhost']);	
	regval('HOST', $_POST['dhost']);	
	regval('DB', $_POST['dname']);	
	regval('DBUSER', $_POST['duser']);	
	regval('DBPASS', $_POST['dpass']);	
	regval('NEWREG', $_POST['newreg']);	
	regval('PATH_CAL', $_POST['pcal']);	
	regval('PATH_ADMIN', $_POST['padmin']);	
	$confirm = 'Script settings updated successfully';
}

//update Style
if(isset($_POST['update5']))
{
	regval('CUSTOMSTYLE', $_POST['customstyle']);	
	$confirm = 'Calendar style updated successfully';
}

require_once('../includes/config.php');
require_once('../includes/Functions.php');

$skin_array = array("Red", "Green", "Blue", "Orange", "Grey"); //will be displayed in selectbox (skin)
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8;charset=utf-8" /> 
	
	<title>3C-Events :: Administration</title>

	<link type="text/css" href="../assets/admin-skin/style.css" rel="stylesheet" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" type="text/css" media="screen"/>
	
	<script type="text/javascript" src="../assets/javascript/jquery-1.7.1.min.js" ></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.9.1/jquery-ui.js" ></script>
	<script type='text/javascript' src='../assets/javascript/custom.js'></script>
		<script src="../assets/javascript/jquery.uniform.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="../assets/admin-skin/uniform.default.css" type="text/css" media="screen" charset="utf-8" />
	<script type="text/javascript" charset="utf-8">
      jQuery(function(){
        jQuery("input:text, input:radio, input:checkbox, textarea, select").uniform();
		jQuery("#tabs").tabs();
      });
    </script>
</head>

<body>
	<div id="container">
		<div id="bgwrap">
			<div id="primary_left">
				<div class="copy">
					Hello <?php echo $_SESSION['username']; ?> <a href="login.php?logout" style="color:#aaa">[Logout]</a><br />
					Script Version : <?php echo VERSION ?><br />					
					Latest Version:
						<?php
								// Get Latest Version
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL, "http://freelanceonweb.com/3c-events/version.txt");
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								$output = curl_exec($ch);
								curl_close($ch);
								
								if($output!=VERSION) echo '<strong style="color:green">'.$output.'</strong>';
								else  echo $output;
						?>
					<br />	
					<div style="text-align:center; margin-top:10px; font-size:11px">
						<a href="http://freelanceonweb.com/3c-events" style="color:#2F7ED7; text-decoration:none" target="_blank">&copy; 3c-events</a> - <a href="mailto:contact@freelanceonweb.com" style="color:#2F7ED7; text-decoration:none">Contact US</a>
					</div> 
					<?php if(file_exists('../install/index.php') && $_SESSION['id_user']==0) echo '<strong style="color:red">Please delete or rename install folder</strong>'?>
				</div>
				<div id="logo">
					<a href="<?php echo PATH_ADMIN ?>" title="Administration 3C-Events"><img src="../assets/admin-skin/img/logo.png" alt="" /></a>
				</div> 
				<div id="menu">
					<ul>
						<li><a href="index.php"><img src="../assets/admin-skin/img/events.png" alt="" /><span>Events</span></a></li>
						<li><a href="events.php"><img src="../assets/admin-skin/img/addevent.png" alt="" /><span>Add Event</span></a></li>
						<?php if($_SESSION['id_user']==0) {?><li><a href="users.php"><img src="../assets/admin-skin/img/musers.png" alt="" /><span>Manage Users</span></a></li>
						<li class="current"><a href="settings.php"><img src="../assets/admin-skin/img/settings.png" alt="" /><span>Settings</span></a></li>
						<li><a href="database.php"><img src="../assets/admin-skin/img/db.png" alt="" /><span>Backup/Restore DB</span></a></li>
						<li><a href="http://freelanceonweb.com/forum"><img src="../assets/admin-skin/img/help.png" alt="" /><span>Support/Docs</span></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div id="primary_right">
				<div class="inner">

					<h1>Settings</h1>

					<?php echo Ok($confirm); ?>
					
					<form action="settings.php" method="post" id="form1">

				<div id="tabs">	
	<ul>
		<li><a href="#tabs-1"><img src="../assets/admin-skin/img/settings2.png" alt="" /> Calendar Settings</a></li>
		<li><a href="#tabs-2"><img src="../assets/admin-skin/img/settings2.png" alt="" /> Script Settings</a></li>
		<li><a href="#tabs-3"><img src="../assets/admin-skin/img/paint.png" alt="" /> Custom Style</a></li>
		<li><a href="#tabs-4"><img src="../assets/admin-skin/img/help2.png" alt="" /> Support &amp; Credits</a></li>	
	</ul>	

	<div id="tabs-1" style="width:750px">
		<p>	
			<fieldset>
				<legend>Calendar settings</legend>
				
				<label for="quickadmin" class="labelset"><strong>Quick Secret Admin</strong></label>
				<input type="text" name="quickadmin" id="quickadmin" value="<?php echo QUICKADMIN;?>"/><br />
				
				<i>*In the search zone of the calendar, look for this word, for quick access to the admin</i><br /><br />
				
				
				<label for="lang" class="labelset"><strong>Calendar Language</strong></label>
				<select name="lang" id="lang">
				
				<?php 
					if ($handle = opendir('../includes/lang')) {
						while (false !== ($file = readdir($handle))) {
						if($file!='..' && $file!='.' && $file!='index.html' && $file!='index.php')
							$tab[] = str_replace('.php', '', $file);
						}
						closedir($handle);
					}

					sort($tab);

					foreach($tab as $file)
					{
						$checked='';
						if($file==LANG) $checked='selected="selected"';
						echo '<option value="'.$file.'" '.$checked.'>'.$file.'</option>';
						echo "\n";
					}	
				?>
				</select><br /><br />
				
				
				<label for="skin" class="labelset"><strong>Calendar Color</strong></label>
				<div style="float:left">
				<select name="skin" style="float:left; margin-right:10px">
				<?php
				foreach($skin_array as $skin)
				{
					$checked='';
					if(strtolower($skin)==SKIN)
						$checked = 'selected="selected"';
					echo '<option value="'.strtolower($skin).'" '.$checked.'>'.$skin.'</option>';	
				}
				?>
				</select>
				</div>
				<div style="float:left">
				<?php
				foreach($skin_array as $skin)
				{
					$checked='';
					if(strtolower($skin)==SKIN)
						$checked = 'border:4px solid #000';

					echo'<a href="#" class="tooltip" style="float:left; margin-right:5px;'.$checked.'" onclick="return false" title="Preview"><div style="width:20px; height:15px; background:'.$skin.'"></div><span><img src="../assets/admin-skin/img/Preview/'.strtoupper($skin).'.png" alt="" /></span></a> ';
				}
				?>
				</div>
				<div style="clear:both"><br /></div>

		<label for="dateformat" class="labelset"><strong>Date Format</strong></label>
		<select name="dateformat" id="dateformat">
			<option value="1" <?php if(DATEFORMAT==1) echo 'selected="selected"'?> >mm/dd/yyyy</option>
			<option value="2" <?php if(DATEFORMAT==2) echo 'selected="selected"'?> >dd/mm/yyyy</option>
		</select><br /><br />
		
		<label for="enablecalendar" class="labelset"><strong>Enable Calendar?</strong></label>
		<input type="radio" value="1" name="enablecalendar" <?php echo (EC==1) ? 'checked="checked"' : '';?>/> Yes <input type="radio" value="0" name="enablecalendar" <?php echo (EC==0) ? 'checked="checked"' : '';?>/> No<br /><br />
		
		<label for="enablesearch" class="labelset"><strong>Enable Search?</strong></label>
		<input type="radio" value="1" name="enablesearch" <?php echo (ES==1) ? 'checked="checked"' : '';?>/> Yes <input type="radio" name="enablesearch" value="0" <?php echo (ES==0) ? 'checked="checked"' : '';?>/> No<br /><br />
		
		<label for="weeknumber" class="labelset"><strong>Display week number?</strong><br />(mini calendar)</label>
		<input type="radio" value="1" name="weeknumber" <?php echo (WN==1) ? 'checked="checked"' : '';?>/> Yes <input type="radio" name="weeknumber" value="0" <?php echo (WN==0) ? 'checked="checked"' : '';?>/> No<br /><br />
		
		<label for="noevent" class="labelset"><strong>Hide days with no events?</strong><br />(events list)</label>
		<input type="radio" value="1" name="noevent" <?php echo (NOEVENT==1) ? 'checked="checked"' : '';?>/> Yes <input type="radio" value="0" name="noevent" <?php echo (NOEVENT==0) ? 'checked="checked"' : '';?>/> No<br /><br /><br />
			
		<label for="minisize" class="labelset"><strong>Mini Calendar Size</strong></label>
		<input type="text" value="<?php echo MINISIZE ?>" name="minisize"/> <span>(width). Default: 254</span><br /><br />
		
		<label for="bigsize" class="labelset"><strong>Big Calendar Size</strong></label>
		<input type="text" value="<?php echo BIGSIZE ?>" name="bigsize"/> <span>(width). Default: 700</span><br /><br />
				
		<label for="listsize" class="labelset"><strong>Events List Size</strong></label>
		<input type="text" value="<?php echo LISTSIZE ?>" name="listsize"/> <span>(width). Default: 500</span><br /><br />
		
		<label for="popsize" class="labelset"><strong>Popup Size</strong></label>
		<input type="text" value="<?php echo POPSIZE ?>" name="popsize"/> <span>(width x height). Default: 700x500</span><br /><br />
				
		<label for="iconsize" class="labelset"><strong>Icon Size</strong></label>
		<input type="text" value="<?php echo ICONSIZE ?>" name="iconsize"/> <span>(width x height). Default: 20x20</span><br /><br />
		
		<input type="submit" value="Update" name="update2" /><br /><br />
			</fieldset>
		
			<fieldset>
				<legend>Events Settings</legend>
				
				<label for="letter" class="labelset"><strong>Letters of the month name</strong></label>
				<input type="text" name="letter" id="letter"  value="<?php echo LETTER ?>" /><br />
				<label  class="labelset">&nbsp;</label>
				Default: 3<br /><br />						

				<label for="old" class="labelset"><strong>Hide events older than</strong></label>
				<input type="text" name="old" id="old"  value="<?php echo OLDER ?>" /><br />
				<label  class="labelset">&nbsp;</label>
				(Days). *Set 0 to show all<br /><br />		

				<label for="email" class="labelset"><strong>Contact Email</strong></label>
				<input type="text" name="email" id="email" value="<?php echo CONTACT ?>" /><br />
				<label  class="labelset">&nbsp;</label>
				(Where you will receive the questions from your visitors)<br /><br />	
				
				<label for="notification" class="labelset"><strong>Notification Email</strong></label>
				<input type="text" name="notification" id="notification" value="<?php echo FORWARD ?>" /><br />
				<label  class="labelset">&nbsp;</label>
				(Each new event, a notification will be sent to this email address)<br /><br />	
				
				<label class="labelset"><strong>Enable Contact?</strong></label>
				<input type="radio" value="1" name="enablecontact" <?php echo (ECONTACT==1) ? 'checked="checked"' : '';?>/> Yes <input type="radio" value="0" name="enablecontact" <?php echo (ECONTACT==0) ? 'checked="checked"' : '';?>/> No<br /><br />
				
				<label class="labelset"><strong>Enable Vote?</strong></label>
				<input type="radio" value="1" name="enablevote" <?php echo (EVOTE==1) ? 'checked="checked"' : '';?>/> Yes <input type="radio" value="0" name="enablevote" <?php echo (EVOTE==0) ? 'checked="checked"' : '';?>/> No<br /><br />
				
				<label class="labelset"><strong>Enable Share?</strong></label>
				<input type="radio" value="1" name="enableshare" <?php echo (ESHARE==1) ? 'checked="checked"' : '';?>/> Yes <input type="radio" value="0" name="enableshare" <?php echo (ESHARE==0) ? 'checked="checked"' : '';?>/> No<br /><br />
				
				<input type="submit" value="Update" name="update3" /><br /><br />
			</fieldset>
			</p>
		</div>	
		
		<div id="tabs-2" style="width:750px">
			<p>	
				<fieldset>
					<legend>Script Settings</legend>
					
					<label for="dhost" class="labelset"><strong>Database Host</strong></label>
					<input type="text" name="dhost" id="dhost" value="<?php echo HOST ?>" /><br /><br />
					
					<label for="dname" class="labelset"><strong>Database Name</strong></label>
					<input type="text" name="dname" id="dname" value="<?php echo DB ?>" /><br /><br />
					
					<label for="duser" class="labelset"><strong>Database User</strong></label>
					<input type="text" name="duser" id="duser" value="<?php echo DBUSER ?>" /><br /><br />	
					
					<label for="dpass" class="labelset"><strong>Database Password</strong></label>
					<input type="password" name="dpass" id="dpass" value="<?php echo DBPASS ?>" /><br /><br />	
					
					<label for="newreg" class="labelset"><strong>Allow New Registrations?</strong></label>
					<input type="radio" value="1" name="newreg" <?php echo (NEWREG==1) ? 'checked="checked"' : '';?>/> Yes <input type="radio" value="0" name="newreg" <?php echo (NEWREG==0) ? 'checked="checked"' : '';?>/> No<br /><br />
					
					<label for="pcal" class="labelset"><strong>Path To Calendar</strong></label>
					<input type="text" name="pcal" id="pcal" value="<?php echo PATH_CAL?>" /><br />
					<i>*Remember to change the path also via FTP</i><br /><br />			
					
					<label for="padmin" class="labelset"><strong>Path To Admin Folder</strong></label>
					<input type="text" name="padmin" id="padmin" value="<?php echo PATH_ADMIN?>" /><br />
					<i>*Remember to change the path also via FTP</i><br /><br />
					
					<input type="submit" value="Update" name="update4" /><br /><br />
				</fieldset>
				
				<fieldset>
					<legend>Admin Account</legend>
					<label for="login" class="labelset"><strong>Username</strong></label>
					<input type="text" name="login" id="login" value="<?php echo USERNAME;?>"/><br /><br />

					<div style="float:left">
					<label for="password" class="labelset"><strong>Password</strong></label>
					<input type="password" name="password" id="password"/>
					</div>
					<div style="float:left">
					<label for="password1" class="labelset"><strong>Confirm Password</strong></label>
					<input type="password" name="password1" id="password1"/>
					</div>
					<br /><br />
					<input type="submit" value="Update" name="update1" id='update1' /><br /><br />
				</fieldset>
				
			</p>
		</div>
		<div id="tabs-3" style="width:750px">
			<p>					
					<fieldset>
						<legend>Custom Style</legend>
						
						You can define your own CSS, example: <br />#minical a{color:red}<br /><br />
						<textarea name="customstyle" id="customstyle" rows="10" cols="130"><?php echo CUSTOMSTYLE ?></textarea><br /><br />
						<input type="submit" value="Update" name="update5" /><br /><br />
					</fieldset>	

			</p>
		</div>
		
		<div id="tabs-4" style="width:750px">
			<p>Item URL : <a href="http://www.freelanceonweb.com/3c-events/">http://www.freelanceonweb.com/3c-events/</a><br />
			Support Forum : <a href="http://freelanceonweb.com/forum/">http://freelanceonweb.com/forum</a><br />
			Contact US : <a href="mailto:contac@freelanceonweb.com">contac@freelanceonweb.com</a></p>

			<hr style="color:#ccc"/>

			<h4><img src="../assets/admin-skin/img/download.png" alt="" /> Our Latest Scripts</h4>

			<div class="items">

				<a href="http://codecanyon.net/item/3cevents-wordpress-allinone-event-calendar/2382407?ref=devinfo007" title="3C-Events : PHP AJAX Events Calendar">
				<img src="http://0.s3.envato.com/files/27518172/3c-events-logo.jpg" alt="" /></a>
				
				<a href="http://codecanyon.net/item/wplm-wordpress-link-monetizer/2180365?ref=devinfo007" title="WPLM - Wordpress Link Monetizer">
				<img src="http://1.s3.envato.com/files/25213267/thumbnail.png" alt="" /></a>
				
				<a href="http://codecanyon.net/item/fblikestatus-facebook-like-status-script/1605991?ref=devinfo007" title="FbLikeStatus - Facebook Like Status Script">
				<img src="http://0.s3.envato.com/files/18507330/fblikestatus-thumb.jpg" alt="" /></a>
			</div>

			<hr style="color:#ccc"/>

			<h4><img src="../assets/admin-skin/img/rss.png" alt="" /> Support Forum</h4>
			<div class="forumrss">
			<ul>
			<?php	
				// Forum RSS
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "http://freelanceonweb.com/forum/syndication.php?fid=3,6,13,4,5&limit=5");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$output = curl_exec($ch);
				curl_close($ch);
				$xml = new SimpleXmlElement($output, LIBXML_NOCDATA);
				$cnt = count($xml->channel->item);
				for($i=0; $i<$cnt; $i++)
				{
					$url 	= $xml->channel->item[$i]->link;
					$title 	= $xml->channel->item[$i]->title;
					$desc = $xml->channel->item[$i]->description;
					$date = $xml->channel->item[$i]->pubDate;
					$date = strftime("%Y-%m-%d", strtotime($date));

					echo '<li style="width:240px;">
					<a href="'.$url.'">'.$title.'</a>
					<p style="font-size:11px; margin-top:3px; margin-bottom:10px"><strong>'.$date.'</strong> - '.substr(strip_tags($desc),0,50).'...</p>
					</li>';
				}
			?>	
			</ul>
			</div>

		</div>
	
					</form>
				</div>
			</div> 
		</div> 
	</div> 
	<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery(function(){
			jQuery("#update1").click(function(){
			jQuery(".error").hide();
			var hasError = false;
			var passwordVal = jQuery("#password").val();
			var checkVal = jQuery("#password1").val();
			if (passwordVal == '') {
				jQuery("#password").after('<span class="error">Please enter a password.</span>');
				hasError = true;
			} else if (checkVal == '') {
				jQuery("#password1").after('<span class="error">Please re-enter your password.</span>');
				hasError = true;
			} else if (passwordVal != checkVal ) {
				jQuery("#password1").after('<span class="error">Passwords do not match.</span>');
				hasError = true;
			}
			if(hasError == true) {return false;}
		});
	});

	});	
	</script>	
</body>
</html>