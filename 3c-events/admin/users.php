<?php
#### Name of this file: admin/users.php 
#### Description: Administration of the script, users management(add/edit). You have to login first.
session_start();

require_once('../includes/config.php');
require_once('../includes/Functions.php');

//not yet connected? direction login.php
if(!isset($_SESSION['username']) || !isset($_SESSION['id_user']) || $_SESSION['id_user']!=0)
{
	header('Location: login.php');
}

try
{
	$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
	$bdd = new PDO('mysql:host='.HOST.';dbname='.DB, DBUSER, DBPASS, $pdo_options); $bdd->query("SET NAMES UTF8"); 
}
catch(Exception $e)
{
		exit('Database Error : '.$e->getMessage());
}

$confirm='';
$error='';

//delete an user
if(!empty($_GET['delete']))
{	
	$id = intval($_GET['delete']);
	$req = $bdd->prepare('DELETE FROM 3ce_users WHERE id = :id');
	$req->execute(array(
	'id' => $id
	));
	$req = $bdd->prepare('UPDATE 3ce_event set id_user = 0 WHERE id_user = :id_user');
	$req->execute(array(
	'id_user' => $id
	));
	$confirm = 'User Deleted';	
}

//Change status of an user
if(!empty($_GET['status']))
{	
	$id = intval($_GET['status']);
	$req = $bdd->prepare('UPDATE 3ce_users set status = !status WHERE id = :id');
	$req->execute(array(
	'id' => $id
	));
	$confirm = 'User status changed';	
}

//actions on multiple choices of Users : delete, enable, disable
if(isset($_POST['apply']) && !empty($_POST['c']))
{	
	$selection = $_POST['c'];
	$str='';
	
	foreach($selection as $id=>$value)
		$str .= "id = $id OR ";

	$str = rtrim($str, 'OR ');
	
	switch($_POST['action'])
	{
		case 1: //delete selected users
			$req = $bdd->exec('DELETE FROM 3ce_users WHERE '.$str);
			$confirm = $req.' Users Deleted';
			break;

		case 2: //disable selected users
			$req = $bdd->exec('UPDATE 3ce_users SET status=0 WHERE '.$str);
			$confirm = $req.' Users Disabled';
			break;
		
		case 3: //enable selected users
			$req = $bdd->exec('UPDATE 3ce_users SET status=1 WHERE '.$str);
			$confirm = $req.' Users Enabled';
			break;
	}
}

// add/edit an user
if(isset($_POST['add']) && !empty($_POST['username']))
{								
	//attributes of the new user
	$id = intval($_POST['edit']);
	$username = trim($_POST['username']);
	$password = trim($_POST['password']);
	$email = trim($_POST['email']);
	$auto = isset($_POST['auto']) ? 1:0;
	
	//verify if an user already exists with inputed username or email
	$req = $bdd->query("SELECT count(*) as ct FROM 3ce_users WHERE (username='$username' OR email='$email') AND id != $id ");
	$count = $req->fetch();
	if($count['ct'] || $username==USERNAME)
	{
		$error='User already exist';
	}
	else
	{
		//update user, if id is defined (hidden field in the form, retrieved via $_GET['edit'])
		if(!empty($_POST['edit']))
		{	
			$req = $bdd->prepare('UPDATE 3ce_users set username=:username, password=:password, email=:email, autoapprove=:auto WHERE id=:id');
			$req->execute(array(
				'username' => $username,
				'password' => $password,
				'email' => $email,
				'id' => $id,
				'auto' => $auto,
				));
		
			$confirm = 'User Updated';	
		}
		//else if id user not defined (not edit), add a new user
		else
		{
			$req = $bdd->prepare('INSERT INTO 3ce_users(username, password, email, joindate, lastlogin, autoapprove) VALUES(:username, :password, :email, :joindate, :lastlogin, :auto)');
			$req->execute(array(
				'username' => $username,
				'password' => $password,
				'email' => $email,
				'joindate' => date("Y-m-d"),
				'lastlogin' => date("Y-m-d"),
				'auto' => $auto
				));

			require("../includes/Mail/phpmailer.php");
			$mail = new PHPMailer();

			$mail->From = CONTACT;
			$mail->FromName = '';
			$mail->AddAddress($email,'');

			$mail->WordWrap = 50;
			$mail->IsHTML(true);
			
			$title = 'New Account';
			$body = "Your account to manage events<br><br>Your login: $username <br> Your Password: $password <br><br> You can connect here:". PATH_ADMIN;
			
			$mail->Subject  =  $title;
			$mail->Body     =  nl2br(stripslashes($body));
			$mail->AltBody  =  "This is the text-only body";
			
			if(!$mail->Send()) {
				echo 'Mail Error';
			}
			
			header('Location: users.php?add=1');
		}
	}	
}
elseif(isset($_POST['add']) && empty($_POST['username']))
{
	$error = 'Username must not be empty';
}

//fields are filled with data from the user to edit (from id user)
if(!empty($_GET['edit']))
{	
	$id = intval($_GET['edit']);
	$req = $bdd->prepare('SELECT * FROM 3ce_users WHERE id=:id');
	$req->execute(array('id' => $id));
	$resmd = $req->fetch();	
	$req->closeCursor();
	$id=$resmd['id'];
	$username=$resmd['username'];
	$password=$resmd['password'];
	$email=$resmd['email'];
	$auto=$resmd['autoapprove'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8;charset=utf-8" /> 
	
	<title>3C-Events :: Administration</title>

	<link type="text/css" href="../assets/admin-skin/style.css" rel="stylesheet" />
	<link rel="stylesheet" href="../assets/home-skin/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
	
	<script type="text/javascript" src="../assets/javascript/jquery-1.7.1.min.js" ></script>
	<script type='text/javascript' src='../assets/javascript/custom.js'></script>
		<script src="../assets/javascript/jquery.uniform.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="../assets/admin-skin/uniform.default.css" type="text/css" media="screen" charset="utf-8" />
	<script type="text/javascript" charset="utf-8">
      jQuery(function(){
        jQuery("input:text, input:radio, input:checkbox, textarea, select").uniform();
      });
    </script>
</head>

<body>
	<div id="container">
		<div id="bgwrap">
			<div id="primary_left">
				<div class="copy">
					Hello <?php echo $_SESSION['username']; ?> <a href="login.php?logout" style="color:#aaa">[Logout]</a><br />
					Script Version : <?php echo VERSION ?><br />	
					Latest Version:
						<?php
								// Get Latest Version
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL, "http://freelanceonweb.com/3c-events/version.txt");
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								$output = curl_exec($ch);
								curl_close($ch);
								
								if($output!=VERSION) echo '<strong style="color:green">'.$output.'</strong>';
								else  echo $output;
						?>
					<br />
					<div style="text-align:center; margin-top:10px; font-size:11px">
						<a href="http://freelanceonweb.com/3c-events" style="color:#2F7ED7; text-decoration:none" target="_blank">&copy; 3c-events</a> - <a href="mailto:contact@freelanceonweb.com" style="color:#2F7ED7; text-decoration:none">Contact US</a>
					</div> 
					<?php if(file_exists('../install/index.php') && $_SESSION['id_user']==0) echo '<strong style="color:red">Please delete or rename install folder</strong>'?>
				</div>
				<div id="logo">
					<a href="<?php echo PATH_ADMIN ?>" title="Administration 3C-Events"><img src="../assets/admin-skin/img/logo.png" alt="" /></a>
				</div> 
				<div id="menu">
					<ul>
						<li><a href="index.php"><img src="../assets/admin-skin/img/events.png" alt="" /><span>Events</span></a></li>
						<li><a href="events.php"><img src="../assets/admin-skin/img/addevent.png" alt="" /><span>Add Event</span></a></li>
						<?php if($_SESSION['id_user']==0) {?><li class="current"><a href="users.php"><img src="../assets/admin-skin/img/musers.png" alt="" /><span>Manage Users</span></a></li>
						<li><a href="settings.php"><img src="../assets/admin-skin/img/settings.png" alt="" /><span>Settings</span></a></li>
						<li><a href="database.php"><img src="../assets/admin-skin/img/db.png" alt="" /><span>Backup/Restore DB</span></a></li>
						<li><a href="http://freelanceonweb.com/forum"><img src="../assets/admin-skin/img/help.png" alt="" /><span>Support/Docs</span></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div id="primary_right">
				<div class="inner">
					<h1>Manage Users</h1>				 
					<?php echo Ok($confirm); echo Error($error); ?>
					
					<form action="" method="post">
						<input type="hidden" value="<?php echo $id;?>" name="edit" id="edit" />

						<label for="username" class="labeladm"><img src="../assets/admin-skin/img/user.png" alt="" style="vertical-align:middle; margin-right:5px"/><strong>Username</strong></label>
						<input type="text" name="username" id="username" value="<?php echo $username;?>" /><br /><br />

						<label for="password" class="labeladm"><img src="../assets/admin-skin/img/password.png" alt="" style="vertical-align:middle; margin-right:5px"/><strong>Password</strong></label>
						<input type="text" name="password" id="password" value="<?php echo $password;?>" /><br /><br />
						
						<label for="email" class="labeladm"><img src="../assets/admin-skin/img/email.png" alt="" style="vertical-align:middle; margin-right:5px" /><strong>Email</strong></label>
						<input type="text" name="email" id="email" value="<?php echo $email;?>" /><br /><br />
	
						<label for="auto" style="color:#000; font-family:Arial, Helvetica, sans-serif; font-size:12px;"><img src="../assets/admin-skin/img/auto.png" alt="" style="vertical-align:middle; margin-right:5px" /><strong>Events auto-approved </strong></label>
						<input type="checkbox" name="auto" id="auto" <?php echo ($auto==1) ? 'checked="checked"' : '' ?> /><br /><br />
						
						<input type="submit" name="add" value="<?php echo isset($resmd) ? 'Edit User' : 'Add New User';?>" />
					</form>		
					
					<form action="" method="post">
						<table class="normal tablesorter" style="width:800px">
							<thead>
								<tr>
									<th onclick="selectAll()">Select</th>
									<th>No</th>
									<th>Status </th>
									<th>Username</th>
									<th>Email</th>
									<th>Join Date</th>
									<th>Last Login</th>
									<th>Auto-Events</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
								
								// display users
								$req = $bdd->query('SELECT id, username, email, DATE_FORMAT(joindate, "%m-%d-%Y") as joindate, DATE_FORMAT(lastlogin, "%m-%d-%Y") as lastlogin, status, autoapprove FROM 3ce_users ORDER BY id DESC');
								while ($row = $req->fetch())
								{										
									$status = ($row['status']==0) ? 'Off' : 'On' ;
									$auto = ($row['autoapprove']==0) ? 'No' : 'Yes' ;
									$cl = ($i%2) ? 'class="odd"' : '';
									$content = substr(strip_tags($row['content']),0, 70);	
									echo '<tr '.$cl.'>
									<td><input type="checkbox" value="" name="c['.$row['id'].']"/></td>
									<td>'.$row['id'].'</td>
									<td><div class="status'.$row['status'].'"><a href="users.php?status='.$row['id'] . '" class="icon"><span>'.$status.'</span></a></div></td>
									<td><a href="users.php?edit='.$row['id'].'"><strong style="font-size:12px">'.$row['username'].'</strong></a></td>
									<td>'.$row['email'].'</td>
									<td>'.$row['joindate'].'</td>
									<td>'.$row['lastlogin'].'</td>
									<td>'.$auto.'</td>';
									echo'<td>
										<a href="users.php?edit='.$row['id'] . '" title="Edit this event" class="icon"><img src="../assets/admin-skin/img/edit.png" alt="" /></a> 
										<a href="users.php?delete='.$row['id'] . '" title="Delete this user" class="icon" onclick="return confirm(\'You are sure to delete this user?\')"><img src="../assets/admin-skin/img/delete.png" alt="" /></a>
									</td>
									</tr>';
									$i++;
								}
								$req->closeCursor();
								?>
							</tbody>
						</table>
						Action : 
						<select name="action">
							<option selected="selected" value="0">Choose one...</option>
							<option value="1">Delete</option>
							<option value="2">Disable</option>
							<option value="3">Enable</option>
						</select>
						<input type="submit" name="apply" value="Apply" />
					</form>
				</div>
			</div>
			</div>
		</div>
	</div>
</body>
</html>