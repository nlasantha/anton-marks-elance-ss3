<?php
#### Name of this file: admin/login.php 
#### Description: Login/Logout administration pages
session_start();

require_once('../includes/config.php');
require_once('../includes/Functions.php');

$error='';

// Check if both session exists
if(!isset($_SESSION['username']) || !isset($_SESSION['id_user']))
{
	unset($_SESSION['username']);
	unset($_SESSION['id_user']);
}

// Already connected? direction index.php
if(isset($_SESSION['username']) || isset($_SESSION['id_user']))
	header('Location: index.php');
	
try
{
	$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
	$bdd = new PDO('mysql:host='.HOST.';dbname='.DB, DBUSER, DBPASS, $pdo_options); $bdd->query("SET NAMES UTF8"); 
}
catch(Exception $e)
{
		exit('Database Error : '.$e->getMessage());
}
	

// Logout	
if(isset($_GET['logout']))
{
	unset($_SESSION['username']);
	unset($_SESSION['id_user']);
}

// Login	
if (!empty($_POST['username']) && !empty($_POST['password']) && isset($_POST['login']))
{	
	$req = $bdd->prepare('SELECT id, username FROM 3ce_users WHERE username=:username AND password=:password AND status=1');
	$req->execute(array(
		'username' => trim($_POST['username']),
		'password' => trim($_POST['password'])
	));
	$resmd = $req->fetch();	
	$req->closeCursor();
	$id=$resmd['id'];
	$username=$resmd['username'];
	
	if ($_POST['username'] == USERNAME && $_POST['password'] == PASSWORD)
	{
		$_SESSION['username'] = USERNAME;
		$_SESSION['id_user'] = 0;
		header('Location: index.php');
	}
	elseif($id)
	{
		$req = $bdd->prepare('UPDATE 3ce_users set lastlogin = now() WHERE id = :id');
		$req->execute(array(
		'id' => $id
		));
		$_SESSION['username'] = $username;
		$_SESSION['id_user'] = $id;
		header('Location: index.php');
	}
	else
		$error='Username or password incorrect';
}

//new account	
if (!empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['email']) && !isset($_POST['login']) && NEWREG)
{	

	$username= trim(strip_tags($_POST['username']));
	$password= trim(strip_tags($_POST['password']));
	$email= trim(strip_tags($_POST['email']));
		
	//verify if an user already exists with inputed username or email
	$req = $bdd->prepare("SELECT count(*) as ct FROM 3ce_users WHERE (username=:username OR email=:email)");
	$req->execute(array(
		'username' => $username,
		'email' => $email
	));
	$count = $req->fetch();
	$req->closeCursor();
		
	if($count['ct'] || $username==USERNAME)
	{
		$error='User already exist';
	}
	else
	{	
		$req = $bdd->prepare('INSERT INTO 3ce_users(username, password, email) VALUES(:username, :password, :email)');
		$req->execute(array(
			'username' => $username,
			'password' => $password,
			'email' => $email
			));

		require("../includes/Mail/phpmailer.php");
		$mail = new PHPMailer();

		$mail->From = CONTACT;
		$mail->FromName = '';
		$mail->AddAddress($email,'');

		$mail->WordWrap = 50;
		$mail->IsHTML(true);
		
		$body = "Your account to manage events<br><br>Your login: $username <br> Your Password: $password <br><br> You can connect here:". PATH_ADMIN;
		
		$mail->Subject  =  'New Account';
		$mail->Body     =  nl2br(stripslashes($body));
		$mail->AltBody  =  "This is the text-only body";
		
		if(!$mail->Send())
			echo 'Mail Error';
		
		$_SESSION['username'] = $username;
		$_SESSION['id_user'] = $bdd->lastInsertId();
		header('Location: index.php');
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8;charset=utf-8" /> 
	<title>3C-Events :: Login</title>
	<link type="text/css" href="../assets/admin-skin/style.css" rel="stylesheet" />
	<script type="text/javascript">
	function mail()
	{
		var etat=document.getElementById('demail').style;
		if(etat.display=='none')
		{
			etat.display='block';
		}
		else
			document.form1.submit();
	}
	</script>
</head>

<body style="background:#aaa">
	<div class="inner" style="width:400px; margin:auto; margin-top:8%; background:#eee; border:1px solid #505050; padding:20px; border-radius:8px">
		<h1>3c-Events Administration</h1>				 
		<?php echo Error($error); ?>
		<form action="" method="post" name="form1">
			<p>
				<label for="username" style="color:#000; font-family:Arial, Helvetica, sans-serif; font-size:12px;"><strong>Username :</strong></label><br />
				<input type="text" name="username" id="username" style="width:200px" /><br />
				
				<label for="password" style="color:#000; font-family:Arial, Helvetica, sans-serif; font-size:12px;"><strong>Password :</strong></label><br />
				<input type="password" name="password" id="password" style="width:200px" /><br />
				
				<?php if(NEWREG) { ?>
				<div id="demail" style="display:none">
					<label for="email" style="color:#000; font-family:Arial, Helvetica, sans-serif; font-size:12px;"><strong>Email :</strong></label><br />
					<input type="text" name="email" id="email" style="width:200px"/><br /><br />
				</div><?php } ?>
				
				<br />
				<input type="submit" value="Login" name="login" /> 
				<?php if(NEWREG) { ?> <input type="submit" value="New Account" onclick="mail(); return false" name="new" /><?php } ?>
			</p>
		</form>
	</div>
</body>
</html>