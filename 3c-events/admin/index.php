<?php
#### Name of this file: admin/index.php 
#### Description: Administration of the script, events management. You have to login first.
session_start();

require_once('../includes/config.php');
require_once('../includes/Functions.php');

//not yet connected? direction login.php
if(!isset($_SESSION['username']) || !isset($_SESSION['id_user']))
{
	header('Location: login.php');
}
 
try
{
	$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
	$bdd = new PDO('mysql:host='.HOST.';dbname='.DB, DBUSER, DBPASS, $pdo_options); $bdd->query("SET NAMES UTF8"); 
}
catch(Exception $e)
{
		exit('Database Error: '.$e->getMessage());
}

$confirm='';

//actions on multiple choices of Events : delete, enable, disable
if(isset($_POST['apply']) && !empty($_POST['c']))
{	
	$selection = $_POST['c'];
	$str='';
	
	foreach($selection as $id=>$value)
	{
		$str .= "(id = $id";	
		if($_SESSION['id_user']!=0)
		{
			$str.= ' AND id_user='.$_SESSION['id_user'].') OR ';
		}
		else
			$str.= ') OR ';
	}
		

	$str = rtrim($str, 'OR ');
	

	
	switch($_POST['action'])
	{
		case 1: //delete selected event
			$req = $bdd->exec('DELETE FROM 3ce_event WHERE '.$str);
			$confirm = $req.' Events Deleted';
			break;

		case 2: //disable selected events
			$req = $bdd->prepare("SELECT autoapprove FROM 3ce_users WHERE id=:id");
			$req->execute(array('id' => intval($_SESSION['id_user'])));
			$status = $req->fetch();	
			$req->closeCursor();
			
			if($status['autoapprove']==1 || $_SESSION['id_user']==0)
			{
				$req = $bdd->exec('UPDATE 3ce_event SET status=0 WHERE '.$str);
				$confirm = $req.' Events Disabled';
			}
			break;
		
		case 3: //enable selected events
			$req = $bdd->prepare("SELECT autoapprove FROM 3ce_users WHERE id=:id");
			$req->execute(array('id' => intval($_SESSION['id_user'])));
			$status = $req->fetch();	
			$req->closeCursor();
			
			if($status['autoapprove']==1 || $_SESSION['id_user']==0)
			{
				$req = $bdd->exec('UPDATE 3ce_event SET status=1 WHERE '.$str);
				$confirm = $req.' Events Enabled';
			}
			break;
	}
}

//delete an event
if(!empty($_GET['delete']))
{	
	$id = intval($_GET['delete']);
	
	if($_SESSION['id_user']==0)
	{
		$req = $bdd->prepare('DELETE FROM 3ce_event WHERE id = :id');
		$req->execute(array(
		'id' => $id
		));
	}
	else
	{
		$req = $bdd->prepare('DELETE FROM 3ce_event WHERE id = :id AND id_user=:id_user');
		$req->execute(array(
		'id' => $id,
		'id_user' => $_SESSION['id_user']
		));
	}

	if($req->rowCount())
		$confirm = 'Event Deleted';			
}

//Changing Status of an event
if(!empty($_GET['status']))
{	
	$id = intval($_GET['status']);
	
	if($_SESSION['id_user']==0)
	{
		$req = $bdd->prepare('UPDATE 3ce_event set status = !status WHERE id = :id');
		$req->execute(array(
		'id' => $id
		));
		
		if($req->rowCount())
			$confirm = 'Event status changed';	
	}
	else
	{
		$req = $bdd->prepare("SELECT autoapprove FROM 3ce_users WHERE id=:id");
		$req->execute(array('id' => intval($_SESSION['id_user'])));
		$status = $req->fetch();	
		$req->closeCursor();
		
		if($status['autoapprove']==1) $status=1;
		else $status=0;
		
		if($status)
		{
			$req = $bdd->prepare('UPDATE 3ce_event set status = !status WHERE id = :id AND id_user=:id_user');
			$req->execute(array(
			'id' => $id,
			'id_user' => $_SESSION['id_user']
			));
			
			if($req->rowCount())
				$confirm = 'Event status changed';	
		}
	}
}

if(isset($_GET['add']))
	$confirm = 'Event Added';

//Current Page
$url = "index.php";	
if(!isset($_GET['page']))
	$url.='?page=1';
else	
	$url.='?page='.$_GET['page'];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8;charset=utf-8" /> 
	
	<title>3C-Events :: Administration</title>

	<link type="text/css" href="../assets/admin-skin/style.css" rel="stylesheet" />
	<link rel="stylesheet" href="../assets/home-skin/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
	
	<script type="text/javascript" src="../assets/javascript/jquery-1.7.1.min.js" ></script>
	<script type='text/javascript' src='../assets/javascript/custom.js'></script>
	<script src="../assets/javascript/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
		<script src="../assets/javascript/jquery.uniform.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="../assets/admin-skin/uniform.default.css" type="text/css" media="screen" charset="utf-8" />
	<script type="text/javascript" charset="utf-8">
      jQuery(function(){
        jQuery("input:text, input:radio, input:checkbox, textarea, select.action").uniform();
      });
    </script>
</head>

<body>
	<div id="container">		
		<div id="bgwrap">
			<div id="primary_left">
				<div class="copy">
					Hello <?php echo $_SESSION['username']; ?> <a href="login.php?logout" style="color:#aaa">[Logout]</a><br />
					Script Version : <?php echo VERSION ?><br />	
					Latest Version:
						<?php
								// Get Latest Version
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL, "http://freelanceonweb.com/3c-events/version.txt");
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								$output = curl_exec($ch);
								curl_close($ch);
								
								if($output!=VERSION) echo '<strong style="color:green">'.$output.'</strong>';
								else  echo $output;
						?>
					<br />
					<div style="text-align:center; margin-top:10px; font-size:11px">
						
						<a href="http://freelanceonweb.com/3c-events" style="color:#2F7ED7; text-decoration:none" target="_blank">&copy; 3c-events</a> - <a href="mailto:contact@freelanceonweb.com" style="color:#2F7ED7; text-decoration:none">Contact US</a>
						
					</div> 
					<?php if(file_exists('../install/index.php') && $_SESSION['id_user']==0) echo '<strong style="color:red">Please delete or rename install folder</strong>'?>
				</div>
				<div id="logo">
					<a href="<?php echo PATH_ADMIN ?>" title="Administration 3C-Events"><img src="../assets/admin-skin/img/logo.png" alt="Logo" /></a>
				</div>
				<div id="menu">
					<ul>
						<li class="current"><a href="index.php"><img src="../assets/admin-skin/img/events.png" alt="" /><span class="current">Events</span></a></li>
						<li><a href="events.php"><img src="../assets/admin-skin/img/addevent.png" alt="" /><span>Add Event</span></a></li>
						<?php if($_SESSION['id_user']==0) {?><li><a href="users.php"><img src="../assets/admin-skin/img/musers.png" alt="" /><span>Manage Users</span></a></li>
						<li><a href="settings.php"><img src="../assets/admin-skin/img/settings.png" alt="" /><span>Settings</span></a></li>
						<li><a href="database.php"><img src="../assets/admin-skin/img/db.png" alt="" /><span>Backup/Restore DB</span></a></li>
						<li><a href="http://freelanceonweb.com/forum"><img src="../assets/admin-skin/img/help.png" alt="" /><span>Support/Docs</span></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div id="primary_right">
				<div class="inner">
					<h1>Manage Events</h1>				 
					<?php echo Ok($confirm); ?>
					<form action="index.php" method="post">
						<table class="normal tablesorter">
							<thead>
								<tr>
									<th onclick="selectAll()" style="cursor:pointer">&nbsp;Select&nbsp;</th>
									<th style="width:40px"><a href="<?php echo $url?>&f=id<?php echo filter('id',true)?>">&nbsp;No</a> <?php echo filter('id') ?></th>
									<th style="width:70px"><a href="<?php echo $url?>&f=status<?php echo filter('status',true)?>">&nbsp;Status&nbsp;</a> <?php echo filter('status') ?></th>
									<th><a href="<?php echo $url?>&f=name<?php echo filter('name',true)?>">&nbsp;Name&nbsp;</a> <?php echo filter('name') ?></th>
									<th><a href="<?php echo $url?>&f=place<?php echo filter('place',true)?>">&nbsp;Location&nbsp;</a> <?php echo filter('place') ?></th>
									<th style="width:135px"><a href="<?php echo $url?>&f=date<?php echo filter('date',true)?>">&nbsp;Date&nbsp;</a> <?php echo filter('date') ?></th>
									<th style="width:100px">&nbsp;Attending&nbsp;</th>
									<th style="width:100px">&nbsp;Actions&nbsp;</th>
									<th><a href="<?php echo $url?>&f=id_user<?php echo filter('id_user',true)?>">&nbsp;Author&nbsp;</a> <?php echo filter('id_user') ?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								//pagination, functions defined in includes/Functions.php
								$i=0;
								$nb = !empty($_GET['page']) ? ($_GET['page']-1)*PAGINA_LIMIT : '0';
								$pg = !empty($_GET['page']) ? $_GET['page'] : 1;
								$f = isset($_GET['f']) ? $_GET['f'] : 'id';
								$o = isset($_GET['o']) ? $_GET['o'] : 'DESC';
								
								// display events
								$req = $bdd->query('SELECT id, name, place, date, status, id_user FROM 3ce_event ORDER BY '.$f.' '.$o.' LIMIT '.$nb.','.PAGINA_LIMIT);
								while ($row = $req->fetch())
								{
									//Vote like
									$reqv = $bdd->query('SELECT count(vote_value) as vote FROM 3ce_votes WHERE item_id='.$row['id'].' AND vote_value=1');
									$votep = $reqv->fetch(); 
									$votep=($votep['vote']!=0) ? '<img src="../assets/admin-skin/img/thumb_up.png" alt="" style="vertical-align:middle" /> <span style="color:#000; font-size:12px">'.$votep['vote'].'</span>' : '';
									
									//Vote don't like
									$reqv = $bdd->query('SELECT count(vote_value) as vote FROM 3ce_votes WHERE item_id='.$row['id'].' AND vote_value=-1');
									$votem = $reqv->fetch(); 
									$votem=($votem['vote']!=0) ? '<img src="../assets/admin-skin/img/thumb_down.png" alt="" style="vertical-align:middle" /> <span style="color:#000; font-size:12px">'.$votem['vote'].'</span>' : '';
									
									$status = ($row['status']==0) ? 'Off' : 'On' ;
									$cl = ($i%2) ? 'class="odd"' : '';
									$content = substr(strip_tags($row['content']),0, 70);	
									echo '<tr '.$cl.'>';?>
									<td><input type="checkbox" value="" name="c[<?php echo $row['id'] ?>]"/></td>
									<td><?php echo $row['id'] ?></td>
									<td><div class="status<?php echo $row['status'] ?>"><a href="index.php?status=<?php echo $row['id'] ?>" class="icon"><span><?php echo $status ?></span></a></div></td>
									<td><a href="events.php?edit=<?php echo $row['id'] ?>"><strong style="font-size:12px"><?php echo $row['name'] ?></strong></a></td>
									<td><?php echo $row['place'] ?></td>
									<?php
									echo'<td><select>';
									$dates = explode(',', $row['date']);
									$nbr = count($dates);
									foreach($dates as $date)
									{
										list($month, $day, $year) = explode('/', $date);
										if(DATEFORMAT==1) $str = $month.'/'.$day.'/'.$year;
										else $str = $day.'/'.$month.'/'.$year;
										echo '<option>'.$str.'</option>';
									}
									echo '</select> '.$nbr.'</td>';
									echo'<td>'.$votep.' '.$votem.'</td>';
									?>
									<td style="text-align:right">
										<?php 
										if( $row['id_user']==$_SESSION['id_user'] || $_SESSION['id_user']==0) { ?>
										<a href="events.php?edit=<?php echo $row['id'] ?>" title="Edit this event" class="icon"><img src="../assets/admin-skin/img/edit.png" alt="" /></a> 
										<a href="index.php?delete=<?php echo $row['id'] ?>" title="Delete this event" class="icon" onclick="return confirm('You are sure to delete this event?')"><img src="../assets/admin-skin/img/delete.png" alt="" /></a> 
										<?php } ?>
										<a href="<?php echo PATH_CAL ?>/events.php?calname=calendar&amp;id=<?php echo $row['id'] ?>&amp;iframe=true&amp;width=800&amp;height=500" class="icon" rel="prettyPhoto" title=""><img src="../assets/admin-skin/img/preview.png" alt="" title="Preview this event" /></a>
									</td>
									<td><?php 
									if ($row['id_user']==0) echo USERNAME;
									else 
									{
										$qname = $bdd->query('SELECT username FROM 3ce_users WHERE id='.$row['id_user']);
										$qres = $qname->fetch(); 
										echo $qres['username'];
									}?>
									</td>
									</tr>
									<?php
									$i++;
								}
								$req->closeCursor();
								?>
							</tbody>
						</table>
						<?php pagination('3ce_event', $pg); ?>
						Action : 
						<select name="action" class="action">
							<option selected="selected" value="0">Choose one...</option>
							<option value="1">Delete</option>
							<option value="2">Disable</option>
							<option value="3">Enable</option>
						</select>
						<input type="submit" name="apply" value="Apply" />
					</form>
				</div>
			</div>
		</div>
	</div> 
	<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'pp_default',slideshow:3000, autoplay_slideshow: false});
	});	
	
	function selectAll() {
    jQuery("input[type='checkbox']:not([disabled='disabled'])").attr('checked', true);
	}
	</script>	
</body>
</html>