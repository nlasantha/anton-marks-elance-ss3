﻿<?php

include('includes/config.php');
$title='3c-events';
$description='Last Events';
$url = "http://" . $_SERVER["SERVER_NAME"];

@header("Content-type: application/xml");      


echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n
 <rss version='2.0' xmlns:atom='http://www.w3.org/2005/Atom'>\n
 <channel>\n
 <title>$title</title>\n
 <link>$url</link>\n
 <description>$description</description>\n
 <atom:link href='$url/3c-events/rss.php' rel='self' type='application/rss+xml' />
 <language>en</language>\n\n";
 

	try
	{
		$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		$bdd = new PDO('mysql:host='.HOST.';dbname='.DB, DBUSER, DBPASS, $pdo_options); $bdd->query("SET NAMES UTF8");
		
		if(!empty($_GET['id']))
		{
			$id = intval($_GET['id']);
			$req = $bdd->query("SELECT id, name, content, place, date, repeat_year
			FROM 3ce_event
			WHERE status=1 AND id=$id
			ORDER BY id DESC LIMIT 50");
		}
		else
		{
			$req = $bdd->query("SELECT *
			FROM 3ce_event
			WHERE status=1
			ORDER BY id DESC LIMIT 50");
		}

		while ($row = $req->fetch())
		{	
			echo "<item>
			\n<title>".$row['name']."</title>
			\n<link>".PATH_CAL."/events.php?calname=cal&amp;id=".$row['id']."</link>
			\n<guid isPermaLink='true'>".PATH_CAL."/events.php?calname=cal&amp;id=".$row['id']."</guid>
			\n<description>\n<![CDATA[<strong>Date: </strong>".$row['date']."<br/><strong>Location: </strong>".$row['place']."<br/>";

			if(!empty($row['stime']))
				echo '<strong>Time:</strong> '.$row['stime'].' ';
			
			if(!empty($row['etime']))
				echo '<strong>To</strong> '.$row['etime'];
				
			echo "<br/>".$row['content']."]]></description>
			\n</item>\n";
		}

		$req->closeCursor();
	}
	catch(Exception $e)
	{
			exit('Database Error: '.$e->getMessage());
	}


echo "</channel>\n</rss>";
?>