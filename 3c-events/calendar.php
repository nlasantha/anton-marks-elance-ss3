<?php
#### Name of this file: calendar.php 
#### Description: display mini calendar

if(!class_exists('Calendar'))
{
	$valid_ajax='';
	require_once('includes/config.php');
	include_once("includes/Calendar/Date.class.php");
	include_once("includes/Calendar/Calendar.class.php");
}
	
//if calendar is enabled, we display it (you can modify this value in admin>settings)
if(EC==1)
{		
	$type='mini';
	if(isset($_POST['type']))
		$type = $_POST['type'];
	elseif(isset($calendar_type))	
		$type = $calendar_type;
		
	$cal = new Calendar('cal1', $type);
	$cal->displayMonth();

	if(WN==1)
		$cal->displayWeeks(true);
	else
		$cal->displayWeeks(false);
		
	$cal->displayDays(true);
	$cal->displayNavigMonth(true);
		
	$cal->activeMonthLink();

	$cal->activeDaysPasses();
	$cal->activeDayPresent();
	$cal->activeDaysFuturs();
	
	$cal->activeDaysEvents();
	
	
	$day=sprintf("%02d",date("d"));
	$month = isset($_POST['month']) ? intval($_POST['month']) : date("m");
	$month=sprintf("%02d",$month);
	$year = isset($_POST['year']) ? intval($_POST['year']) : date("Y");
	$type = isset($_POST['type']) ? $_POST['type'] : $type;
	
	//loading events
	try
	{
		$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		$bdd = new PDO('mysql:host='.HOST.';dbname='.DB, DBUSER, DBPASS, $pdo_options); $bdd->query("SET NAMES UTF8");
		$req = $bdd->query("SELECT *  
		FROM 3ce_event
		WHERE (
			(date LIKE '%$month/__/____%') OR 
			(date LIKE '%__/__/____%' AND repeat_day) OR
			(date LIKE '%__/__/____%' AND repeat_month) OR
			(date LIKE '%$month/$day/____%' AND repeat_year)
		) AND status=1
		ORDER BY id ASC");
		


		while ($row = $req->fetch())
		{	
			$dates = explode(',', $row['date']);
			
			foreach($dates as $date)
			{
				$date = preg_replace('#(.*)/(.*)/(.*)#','$2/$1/$3',$date);
				$name=$row['name'];
				$icon=$row['icon'];
				$icononly=$row['icon_only'];
				
				if(!empty($icon)){
					list($w, $h) = explode('x', ICONSIZE); 
					$img= '<img src="3c-events/includes/zoom.php?w='.$w.'&h='.$h.'&src=upload/'.$icon.'" class="icon" />';
				} 
				
				if($icononly==1) $name='';
				
				if(OLDER==0)
				{
					if($row['repeat_year']==1)
						$date = preg_replace('#(.*)/(.*)$#', '$1/'.$year, $date);
					if($row['repeat_month']==1)
						$date = preg_replace('#(.*)/(.*)/(.*)$#', '$1/'.$month.'/'.$year, $date);
					if($row['repeat_day']==1)
						for($i=1; $i<32; $i++){
							$date = preg_replace('#(.*)/(.*)/(.*)#', $i.'/'.$month.'/'.$year, $date);
							$cal->addEvent($date, '<a rel="events[events]" href="'.PATH_CAL.'/events.php?calname=cal&amp;id='.$row['id'].'&amp;iframe=true&amp;width=800&amp;height=500">'.$img.$name.'</a>');
						}	

					$cal->addEvent($date, '<a rel="events[events]" href="'.PATH_CAL.'/events.php?calname=cal&amp;id='.$row['id'].'&amp;iframe=true&amp;width=800&amp;height=500">'.$img.$name.'</a>');
				}
				else
				{
					if($row['repeat_year']==1)
						$date = preg_replace('#(.*)/(.*)$#', '$1/'.$year, $date);
					if($row['repeat_month']==1)
						$date = preg_replace('#(.*)/(.*)/(.*)$#', '$1/'.$month.'/'.$year, $date);
						
					$nbday = round((strtotime(date("Y-m-d")) - strtotime(preg_replace('#(.*)/(.*)/(.*)#','$3$2$1',$date)))/(60*60*24)-1);
					if($nbday<$opt['wp3c_older'])
						$cal->addEvent($date, '<a rel="events[events]" href="'.PATH_CAL.'/events.php?calname=cal&amp;id='.$row['id'].'&amp;iframe=true&amp;width=800&amp;height=500">'.$img.$name.'</a>');
				}
			}
		}

		$req->closeCursor();
	}
	catch(Exception $e)
	{
			exit('Database Error: '.$e->getMessage());
	}
	
	if($type=='mini')
		$cal->activeAjax("mini-3c-events",'3c-events/calendar.php');
	if($type=='big')
		$cal->activeAjax("big-3c-events",'3c-events/calendar.php');
	if($type=='list')
		$cal->activeAjax("list-3c-events",'3c-events/calendar.php');

	print ($cal->makeCalendar($year,$month,$type));
}
?>