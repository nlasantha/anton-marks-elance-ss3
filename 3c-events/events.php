<?php		
#### Name of this file: events.php 
#### Description: display events

require('includes/config.php');
include('includes/lang/'.LANG.'.php');
include('includes/Functions.php');

if(EVOTE==1)
	include("includes/Vote/Vote.class.php"); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		 <title>3C-Events :: Events Calendar</title>	
	
		<script type="text/javascript" src="assets/javascript/jquery-1.7.1.min.js" ></script>
		<script type="text/javascript" src="assets/javascript/plugin.js"></script>
		<link rel="stylesheet" href="assets/home-skin/calendar.css"  type="text/css" />
		<link rel="stylesheet" href="assets/home-skin/plugin.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="assets/home-skin/color/<?php echo SKIN ?>.css"  type="text/css" />
		<?php if(EVOTE==1) echo Pulse::css(); ?>  
		<?php if(EVOTE==1) echo Pulse::javascript(); ?>  
		<style type="text/css">body{background:#fff}</style>
	</head>
	<body>
		<div id="c-events" class="prettycal clearfix">
			<?php 
			
			//show events
			if(!empty($_GET['calname']) && EC==1)
			{
				try
				{
					$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
					$bdd = new PDO('mysql:host='.HOST.';dbname='.DB, DBUSER, DBPASS, $pdo_options); $bdd->query("SET NAMES UTF8");
					
					//display the event (result of a search)
					if(!empty($_GET['id']))
					{
						$id = intval($_GET['id']);
						
						$req = $bdd->query("SELECT * 
						FROM 3ce_event 
						WHERE status=1 AND id=$id");
						
					}
					//display all events of a day
					elseif(!empty($_GET['day']))
					{
						$year = intval($_GET['year']);
						$month = sprintf("%02d",intval($_GET['month']));
						$day = sprintf("%02d",intval($_GET['day']));
						
						$mt=convertdate(3, mktime(12,0,0,$month,$day,$year));
						
						echo '<h1 style="border-bottom: 1px solid #bbb;color: #505050;font-family: arial;font-size: 18px;padding-bottom: 4px;">'.$home[4].' '.$mt.'</h1>';
						
						$req = $bdd->query("SELECT * 
						FROM 3ce_event 
						WHERE (
							(date LIKE '%$month/__/____%') OR 
							(date LIKE '%__/__/____%' AND repeat_day) OR
							(date LIKE '%__/$day/____%' AND repeat_month) OR
							(date LIKE '%$month/$day/____%' AND repeat_year)
						) AND status=1
						ORDER BY id ASC");
					}
					//display all events of a month
					else
					{
						$year = intval($_GET['year']);
						$month = intval($_GET['month']);
						
						$mt=convertdate(2, mktime(12,0,0,$month,1,$year));
						
						echo '<h1 style="border-bottom: 1px solid #bbb;color: #505050;font-family: arial;font-size: 18px;padding-bottom: 4px;">'.$home[4].' '.$mt.'</h1>';
						
						$req = $bdd->query("SELECT * 
						FROM 3ce_event 
						WHERE (
							(date LIKE '%$month/__/____%') OR 
							(date LIKE '%__/__/____%' AND repeat_day) OR
							(date LIKE '%__/__/____%' AND repeat_month) OR
							(date LIKE '%$month/$day/____%' AND repeat_year)
						) AND status=1
						ORDER BY id ASC");
					}

					echo '<div class="eventscontainer">';
					$i=0;
					
					if(EVOTE==1)
					{
						$pulse = new Pulse();  
						$pulse->setFormat($home[8]);  
					}
					
					while ($row = $req->fetch())
					{
						if(!empty($_GET['id']))
							echo '<h1 style="border-bottom: 1px solid #bbb;color: #505050;font-family: arial;font-size: 18px;padding-bottom: 4px;">'.$row['name'].'</h1>';
							
						$map=$row['map'];
						
						if(empty($_GET['id']))
							echo'<h2 class="acc_trigger"><a href="#">#'.++$i.' '.$row['name'].'</a></h2><div class="acc_container">';
						else
							echo'<div class="acc_containers">';
						
						echo '<p style="font-size:13px; color:#505050; margin:5px 10px; font-family:Tahoma"><strong>'.$home[5].':</strong> '.$row['place'].'<br /><strong>'.$home[6].':</strong> ';
						
						$dates = explode(',',$row['date']);
						$str='';
						foreach($dates as $date)
						{
							list($month, $day, $year) = explode('/', $date);
							if(DATEFORMAT==1) $str .= $month.'/'.$day.'/'.$year.', ';
							else $str .= $day.'/'.$month.'/'.$year.', ';
						}
						
						echo rtrim($str, ', ');
						
						if(!empty($row['stime']))
							echo '<br /><strong>'.$home[16].':</strong> '.$row['stime'].' ';
						
						if(!empty($row['etime']))
							echo '<strong>'.$home[17].'</strong> '.$row['etime'];
							
						echo '</p><div class="block">'.$row['content'].'<div style="margin-bottom:10px"></div>';
							
						if(EVOTE==1)
							echo '<div style="float:right">'.$pulse->voteHTML($row['id']).'</div>';
						
						if(ESHARE==1){?>
						<div style="float:left;width:140px">
						<!-- AddThis Button BEGIN -->
						<div class="addthis_toolbox addthis_default_style ">
						<a class="addthis_button_preferred_1" addthis:url="<?php echo PATH_CAL.'/events.php?calname=event&amp;id='.$row['id']?>" addthis:title="<?php echo $row['name'] ?>"></a>
						<a class="addthis_button_preferred_2" addthis:url="<?php echo PATH_CAL.'/events.php?calname=event&amp;id='.$row['id']?>" addthis:title="<?php echo $row['name'] ?>"></a>
						<a class="addthis_button_preferred_3" addthis:url="<?php echo PATH_CAL.'/events.php?calname=event&amp;id='.$row['id']?>" addthis:title="<?php echo $row['name'] ?>"></a>
						<a class="addthis_button_preferred_4" addthis:url="<?php echo PATH_CAL.'/events.php?calname=event&amp;id='.$row['id']?>" addthis:title="<?php echo $row['name'] ?>"></a>
						<a class="addthis_button_compact" addthis:url="<?php echo PATH_CAL.'/events.php?calname=event&amp;id='.$row['id']?>" addthis:title="<?php echo $row['name'] ?>"></a>
						<a class="addthis_counter addthis_bubble_style" addthis:url="<?php echo PATH_CAL.'/events.php?calname=event&amp;id='.$row['id']?>" addthis:title="<?php echo $row['name'] ?>"></a>
						</div>
						<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4f37904576a7a3f0"></script>
						<!-- AddThis Button END -->
						</div>
						<div style="clear:both"></div><br />
						<?php }
						
						
						if(ECONTACT==1) { ?>	
						<div style="float:left">
						<a href="#" onclick="document.getElementById('cont<?php echo $row['id']?>').style.display='block'; return false" class="ehelp"><?php echo $home[7] ?></a>
						</div>
						
						<div id="cont<?php echo $row['id']?>" style="display:none; clear:both">
							<br /><div id="contact_form<?php echo $row['id']?>">
							
							  <form name="contact" method="post" action="">
								<div>
								<input name="event" id="event<?php echo $row['id']?>" type="hidden" value="<?php echo $row['name']?>" />
								  <input type="text" name="name" id="name<?php echo $row['id']?>" size="10" value="<?php echo $home[9]?>" style="float:left; margin-right:5px; width:80px" class="formfield" onclick="if(this.value=='<?php echo $home[9]?>') this.value=''" />
								  <label class="error" for="name<?php echo $row['id']?>" id="name_error<?php echo $row['id']?>" style="display:none; float:left; margin-right:4px">*</label>
								  
								  <input type="text" name="email" id="email<?php echo $row['id']?>" size="10" value="<?php echo $home[10]?>" style="float:left; margin-right:5px; width:80px" class="formfield" onclick="if(this.value=='<?php echo $home[10]?>') this.value=''" />
								  <label class="error" for="email<?php echo $row['id']?>" id="email_error<?php echo $row['id']?>" style="display:none; float:left; margin-right:4px">*</label>
								  
								  <input type="text" name="phone" id="phone<?php echo $row['id']?>" size="10" value="<?php echo $home[11]?>" style="float:left; margin-right:5px; width:80px" class="formfield" onclick="if(this.value=='<?php echo $home[11]?>') this.value=''" />
								  <label class="error" for="phone<?php echo $row['id']?>" id="phone_error<?php echo $row['id']?>" style="display:none; float:left; margin-right:4px">*</label>
							
								  <textarea name="message" id="message<?php echo $row['id']?>" rows="5" cols="53" class="formfield" style="margin:5px 0"></textarea>
								  <label class="error" for="message<?php echo $row['id']?>" id="message_error<?php echo $row['id']?>" style="display:none; float:left; margin-right:4px">*</label>
								  
								  <input type="submit" name="submit" class="button" id="submit_btn<?php echo $row['id']?>" value="<?php echo $home[12]?>" style="float:right"/>
								</div>
							  </form>
							  <div style="clear:both"></div>
							</div>
						</div>
						<div style="clear:both"></div>
						
						<?php
						}
						// Display google map, if display map of this event is checked   
						if($map==1)
						{ 
							echo '<br /><strong style="color:#505050">'.$home[15].'</strong><br /><br />'; ?>
							<iframe src="map.php?id_event=<?php echo $row['id'] ?>" style="border:0; width:470px; height:300px; padding:0; margin:0;"></iframe>
						<?php }		
						
						echo '</div></div>';
						
						echo '<script type="text/javascript">
						$(function() {
							$(\'#error'.$row['id'].'\').hide();
							$(\'input.text-input\').css({backgroundColor:"#FFFFFF"});
							$(\'input.text-input\').focus(function(){
							$(this).css({backgroundColor:"#FFDDAA"});
							});
							$(\'input.text-input\').blur(function(){
							$(this).css({backgroundColor:"#FFFFFF"});
							});

							$("#submit_btn'.$row['id'].'").click(function() {
							$(\'#error'.$row['id'].'\').hide();
								
							var name = $("input#name'.$row['id'].'").val();
							if (name == "" || name == "'.$home[9].'") {
								$("label#name_error'.$row['id'].'").show();
								$("input#name'.$row['id'].'").focus();
								return false;}
								
							var email = $("input#email'.$row['id'].'").val();
							if (email == "" || email == "'.$home[10].'") {
							  $("label#email_error'.$row['id'].'").show();
							  $("input#email'.$row['id'].'").focus();
							  return false;}
							  
							var phone = $("input#phone'.$row['id'].'").val();
							if (phone == "" || phone == "'.$home[11].'") {
							  $("label#phone_error'.$row['id'].'").show();
							  $("input#phone'.$row['id'].'").focus();
							  return false;}
							  
							var message = $("#message'.$row['id'].'").val();
							if (message == "") {
							  $("label#message_error'.$row['id'].'").show();
							  $("input#message'.$row['id'].'").focus();
							  return false;}
							  
							var event = $("input#event'.$row['id'].'").val();

							var dataString = \'name=\'+ name + \'&email=\' + email + \'&phone=\' + phone + \'&message=\' + message + \'&event=\' + event;
								
							$.ajax({
							  type: "POST",
							  url: "includes/Mail/process.php",
							  data: dataString,
							  success: function() {
							  
								$(\'#contact_form'.$row['id'].'\').html("<div id=\'message\'></div>");
								$(\'#message\').html("")
								.append("<div class=\'notification success\' style=\'color:green\'>'.$home[13].'</div>")
								.hide()
								.fadeIn(1500, function() {$(\'#message\').append("");});
							  }
							});
							return false;});});</script>';
					}
					echo '</div>';
					$req->closeCursor();
				}
				catch(Exception $e)
				{
						exit('Database Error: '.$e->getMessage());
				}
			}
			?>
		</div>

		<script type="text/javascript">
		$(document).ready(function(){

			$('.acc_container').hide();
			$('.acc_trigger:first').addClass('active').next().show();

			$('.acc_trigger').click(function(){
				if( $(this).next().is(':hidden') ) {
					$('.acc_trigger').removeClass('active').next().slideUp();
					$(this).toggleClass('active').next().slideDown();
				}
				return false;
			});
		});
		</script>
	</body>
</html>