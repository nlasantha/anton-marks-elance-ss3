$(function() {
  $('.error').hide();
  $('input.text-input').css({backgroundColor:"#FFFFFF"});
  $('input.text-input').focus(function(){
    $(this).css({backgroundColor:"#FFDDAA"});
  });
  $('input.text-input').blur(function(){
    $(this).css({backgroundColor:"#FFFFFF"});
  });

  $(".button").click(function() {
		// validate and process form
		// first hide any error messages
    $('.error').hide();
		
	  var name = $("input#name").val();
		if (name == "" || name == "Your Name") {
      $("label#name_error").show();
      $("input#name").focus();
      return false;
    }
		var email = $("input#email").val();
		if (email == "" || email == "Your Email") {
      $("label#email_error").show();
      $("input#email").focus();
      return false;
    }
		var phone = $("input#phone").val();
		if (phone == "" || phone == "Your Phone") {
      $("label#phone_error").show();
      $("input#phone").focus();
      return false;
    }
	
		var dataString = 'name='+ name + '&email=' + email + '&phone=' + phone;
		
		$.ajax({
      type: "POST",
      url: "includes/Mail/process.php",
      data: dataString,
      success: function() {
	  
        $('#contact_form').html("<div id='message'></div>");
        $('#message').html("")
        .append("<div class='notification success' style='color:green'><h2 style='margin-top:0'>Contact Form Submitted!</h2>We will be in touch soon.</div>")
        .hide()
        .fadeIn(1500, function() {
          $('#message').append("");
        });
      }
     });
    return false;
	});
});
