$(document).ready(function(){

	$(".tooltip4").tooltip();

	$("a[rel^='events']").prettyPhoto({animation_speed:'normal',theme:'pp_default',slideshow:3000, autoplay_slideshow: false});

	$('.acc_container').hide();
	$('.acc_trigger:first').addClass('active').next().show();

	$('.acc_trigger').click(function(){
		if( $(this).next().is(':hidden') ) {
			$('.acc_trigger').removeClass('active').next().slideUp();
			$(this).toggleClass('active').next().slideDown();
		}
		return false;
	});
});

function findValue(li) {
if( li == null ) return alert("No match!");
if( !!li.extra ) var sValue = li.extra[0];
else var sValue = li.selectValue;}

function selectItem(li) {findValue(li);}
function formatItem(row) {return row[0];}
function lookupAjax(){var oSuggest = $("#eventName")[0].autocompleter;oSuggest.findValue();return false;}
function lookupLocal(){var oSuggest = $("#CityLocal")[0].autocompleter;oSuggest.findValue();return false;}

$("#eventName").autocomplete(
  "3c-events/includes/Search.php",
  {
		delay:10,
		minChars:1,
		matchSubset:1,
		matchContains:1,
		cacheLength:10,
		width:239,
		onItemSelect:selectItem,
		onFindValue:findValue,
		formatItem:formatItem,
		autoFill:false
	}
);

$(function() {
	$('a.live-tipsy').tipsy({gravity: 'sw', opacity: 0.9}); 
});