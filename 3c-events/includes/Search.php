<?php
#### Name of this file: includes/Search.php 
#### Description: ajax auto complete search events
require_once('./config.php');
$q = mb_strtolower(utf8_decode($_GET["q"]), 'UTF-8');

if (!$q) return;

$items = array();

// Quickadmin ? display it
if($q==QUICKADMIN)
{
	$items[] = 'EXTRA - <a rel="prettyPhoto[results]" href="'.PATH_ADMIN.'/index.php?iframe=true&amp;width=980&amp;height=600">QUICKADMIN</a>';
}
// Search the event names that start with the search term
else
{
	try
	{
		$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		$bdd = new PDO('mysql:host='.HOST.';dbname='.DB, DBUSER, DBPASS, $pdo_options); $bdd->query("SET NAMES UTF8"); 
	}
	catch(Exception $e)
	{
			exit('Database Error : '.$e->getMessage());
	}
    $req = $bdd->prepare('SELECT DISTINCT(name), id, date, place
                      FROM 3ce_event
                      WHERE LOWER(name) LIKE :queryString
                      limit 0,100');
	$req->execute(array(':queryString' => $q . '%'));
	
	while($row = $req->fetch())
	{
		$items[] = '<a rel="prettyPhoto[results]" href="'.PATH_CAL.'/events.php?calname=calsearch&amp;id='.$row['id'].'&amp;iframe=true&amp;width=800&amp;height=500">'.$row['name'].'</a> - '.$row['place'];
	}
	$req->closeCursor();
}

//display results	
foreach ($items as $item) {
		echo "$item\n";
}

?>