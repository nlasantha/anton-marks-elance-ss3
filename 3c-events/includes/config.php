<?php
#### Name of this file: includes/config.php 
#### Description: All principal variables are here, you can change it via admin (settings) or here, be careful ;)

error_reporting(E_ALL-E_NOTICE);
define("VERSION", "1.8");

/************ IMPORTANT SETTINGS ************/
//Hostname of your database; it is localhost in most cases
define("HOST", "db392730456.db.1and1.com");
//Database Name
define("DB", "db392730456");
//Database User
define("DBUSER", "dbo392730456");
//Database Password
define("DBPASS", "zD56nR!Y"); 
//Date Language Format
define("LANG", "EN");
//Skin
define("SKIN", "red");
//Path to calendar
define("PATH_CAL", "http://www.anton-marks.com/3c-events");
//Path to admin folder
define("PATH_ADMIN", "http://www.anton-marks.com/3c-events/admin");
//Path to folder upload
define("PATH_UPLOAD", "../upload/");
//Your Email : (Where you will receive the questions from your visitors)
define("CONTACT", "contact@name.com");
//Notification Email : (Each new event, a notification will be sent to this email address)
define("FORWARD", "forward@name.com");
//Quick Admin
define("QUICKADMIN", "3ce-admin");
//Admin Username
define("USERNAME", "anton-marks");
//Admin Password
define("PASSWORD", "anton09876");


/************ CUSTOM SETTINGS ************/
//Display week number?
define("WN", "1");
//Mini calendar size
define("MINISIZE", "254");
//Big calendar size
define("BIGSIZE", "700");
//Events list size
define("LISTSIZE", "500");
//Popup size
define("POPSIZE", "700x500");
//Icon size
define("ICONSIZE", "100x100");
//Date format
define("DATEFORMAT", "1");
//Hide days with no event
define("NOEVENT", "0");
//Allow new registrations
define("NEWREG", "0");
//Don't show events older than
define("OLDER", "0");
//Enable Calendar
define("EC", "1");
//Enable Search
define("ES", "1");
//Enable Vote
define("EVOTE", "1");
//Enable Contact
define("ECONTACT", "1");
//Enable Share
define("ESHARE", "1");
// Months max letter
define("LETTER", "3");
// Custom style
define("CUSTOMSTYLE", "");

//Check the existence of an installation or the install folder
if(!strlen(DB))
{
	if(file_exists('install/index.php'))
		header('location:install/index.php');
	elseif(file_exists('../install/index.php'))
		header('location:../install/index.php');	
}
?>