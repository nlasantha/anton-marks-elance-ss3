<?php

$home = array();
$home[0] = "Bekijk alle evenementen van deze maand"; //See all events of this Month
$home[1] = "Zoek een evenement"; //Search Events
$home[2] = "Ga je met je muispointer over een gekleurde datum om een event te bekijken"; //Hover over a colored date to view events
$home[3] = "Zoek een evenement bij naam"; //Search an event by name
$home[4] = "Evenementen van"; //Events Of
$home[5] = "Plaats"; //Place
$home[6] = "Datum"; //Date
$home[7] = "Heb je vragen over dit evenement?"; //Any question about this Event?
$home[8] = "{up} Leuk, {down} Vind 't niks"; //{up} Like it, {down} Don't
$home[9] = "Je naam"; //Your Name
$home[10] = "Je e-mail"; //Your Email
$home[11] = "Je telefoon"; //Your Phone
$home[12] = "Verzenden"; //Send
$home[13] = "<h2 style='margin-top:0'>Het bericht is verzonden!</h2>We zullen zo spoedig mogelijk contact met je opnemen."; //<h2 style='margin-top:0'>Contact Form Submitted!</h2>We will be in touch soon.
$home[14] = "Klik hier om alle evenementen van deze dag te bekijken"; //Click to view all events of this day
$home[15] = "Event plaats met Google Map"; //Event place with Google Map
$home[16] = "Tijd"; //Time
$home[17] = "naar"; //to
?>