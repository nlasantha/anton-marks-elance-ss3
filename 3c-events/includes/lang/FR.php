<?php

$home = array();
$home[0] = "Voir tous les évènements de ce mois"; //See all events of this Month
$home[1] = "Rechercher des évènements"; //Search Events
$home[2] = "Survolez une date pour voir les événements"; //Hover over a colored date to view events
$home[3] = "Rechercher un événement par nom"; //Search an event by name
$home[4] = "Evénements du"; //Events Of
$home[5] = "Lieu"; //Place
$home[6] = "Date"; //Date
$home[7] = "Une question sur cet événement?"; //Any question about this Event?
$home[8] = "{up} l'aime, {down} ne l'aime pas"; //{up} Like it, {down} Don't
$home[9] = "Votre Nom"; //Your Name
$home[10] = "Votre Email"; //Your Email
$home[11] = "Votre Téléphone"; //Your Phone
$home[12] = "Envoyer"; //Send
$home[13] = "<h2 style='margin-top:0'>Formulaire de contact Soumis!</h2>Nous vous contacterons bientôt."; //<h2 style='margin-top:0'>Contact Form Submitted!</h2>We will be in touch soon.
$home[14] = "Voir tous les évènements de ce jour"; //Click to view all events of this day
$home[15] = "Lieu de l'évènement localisé par Google Map"; ////Event place with Google Map
$home[16] = "Temps"; //Time
$home[17] = "à"; //to
?>