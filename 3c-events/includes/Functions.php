<?php
#### Name of this file: includes/Functions.php 
#### Description: This file contains some functions used in the script

//class upload pictures, used in admin/upload.php
// Class upload pictures, used in admin/upload.php
class Upload
{
	function upimg($file, $up="./upload/")
	{
		$whitelist  = array("jpg", "jpeg", "png", "gif");
		if (isset($_FILES[$file]) && $_FILES[$file]['error']==0) 
		{					
			$info = pathinfo($_FILES[$file]['name']);
			$extension = $info['extension'];			
			
			list($width, $height, $type, $attr) = getimagesize($_FILES[$file]['tmp_name']);
			if ( ($width > 10 || $height > 10) & in_array($extension, $whitelist) )
			{
				$url = md5(rand()) . '.' . $extension;
				move_uploaded_file($_FILES[$file]['tmp_name'], $up . $url);
				return $url;
			}
		}
	}
}

//pagination, used in admin/idex.php
define('PAGINA_LIMIT', 20);
define('PAGINA_CLASS', 'page');	
define('PAGINA_CURRENT_CLASS', 'active');
define('PAGINA_NEXTLINK_CLASS', 'next');
define('PAGINA_PREVIOUSLINK_CLASS', 'previous');
define('PAGINA_PERPAGE', 20);
define('PAGINA_LINK', 'index.php?page={nb}'); 

function getLink($nb) {
	return str_replace('{nb}', (string) $nb, PAGINA_LINK);
}

function pagination($table, $current = 1) {
	echo '<ul class="pagination">';
	
	try {
		$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		$pdo = new PDO('mysql:host='.HOST.';dbname='.DB, DBUSER, DBPASS, $pdo_options); $pdo->query("SET NAMES UTF8");
	} catch(PDOException $e) {
		echo 'Database Error'.$e->getMessage();
		return;
	}
	
	$sql = 'SELECT COUNT(*) AS nb FROM '.$table;
	$req = $pdo->query($sql);
	$data = $req->fetch();
	$nb = $data['nb'];
	
	$nbPage = ceil($nb / PAGINA_PERPAGE);
	$field = isset($_GET['f']) ? '&f='.$_GET['f'] : '';
	$order = isset($_GET['o']) ? '&o='.$_GET['o'] : '';
	
	if($current != 1) {
		echo '<li><a href="'.getLink($current - 1).'" class="'.PAGINA_PREVIOUSLINK_CLASS.'">Previous</a></li>';
	}
	
	for($i = ($current - PAGINA_LIMIT) ; $i < $current ; $i++) {
		if($i > 0) {
			echo '<li><a href="'.getLink($i).$field.$order.'" class="'.PAGINA_CLASS.'">'.$i.'</a></li>';
		}
	}
	
	echo '<li><a href="'.getLink($current).$field.$order.'" class="'.PAGINA_CURRENT_CLASS.'">'.$current.'</a></li>';
	
	$nb = 0;
	for($i = ($current + 1) ; $i <= $nbPage ; $i++) {
		if($nb < PAGINA_LIMIT) {
			echo '<li><a href="'.getLink($i).$field.$order.'" class="'.PAGINA_CLASS.'">'.$i.'</a></li>';
			$nb++;
		}
	}
	
	if($current < $nbPage) {
		echo '<li><a href="'.getLink($current + 1).$field.$order.'" class="'.PAGINA_NEXTLINK_CLASS.'">Next</a></li>';
	}
	
	echo '</ul>';
}

if(isset($_GET['page']) && (int) $_GET['page'] !== 0) {
	$page = (int) $_GET['page'];
} else {
	$page = 1;
}

//display a beautiful ok box
function Ok($msg){
	if($msg=='') return '';
	return '<div class="notification note-success"><a href="#" class="close" title="Close notification">close</a>
				<p style="color:green"><strong>'.$msg.'</strong></p>
			</div>';
}

//display a beautiful error box
function Error($msg){
	if($msg=='') return '';
	return '<div class="notification note-error"><a href="#" class="close" title="Close notification">close</a>
				<p style="color:red"><strong>'.$msg.'</strong></p>
			</div>';
}

//Add sorting to links
function filter($val, $link=false)
{
	if($link==false)
	{
		if(isset($_GET['f']) && $_GET['f'] == $val)
		{
			if($_GET['o'] == 'desc')
				return '<img src="../assets/admin-skin/img/bottom.gif" alt="" style="vertical-align:middle" />';
			else
				return '<img src="../assets/admin-skin/img/top.gif" alt="" style="vertical-align:middle" />';
		}
		
		if(!isset($_GET['f']) && $val=='id')
			return '<img src="../assets/admin-skin/img/bottom.gif" alt="" style="vertical-align:middle" />';
	}
	elseif($link==true)
	{
		if(isset($_GET['f']) && $_GET['f']==$val && $_GET['o']=='desc')
			return '&o=asc';
		elseif(isset($_GET['f']) && $_GET['f']==$val && $_GET['o']=='asc')
			return '&o=desc';
		else
			return '&o=desc';
	}
}

// Convert dates
function convertdate($format, $time)
{
	if(LANG=='FR' || LANG=='NL' || LANG=='ES')
	{
		if($format==2)
			$format="%B, %Y";
		if($format==3)
			$format="%d %B, %Y";
	
		if(LANG=='FR') setlocale (LC_TIME, 'fr_FR','fra'); 
		if(LANG=='NL') setlocale (LC_TIME, 'nl_NL', 'nld'); 
		if(LANG=='ES') setlocale (LC_TIME, 'es_ES', 'es'); 
		
		$mt = strftime($format, $time);
		return utf8_encode($mt);
	}
	
	if(LANG=='EN')
	{ 
		if($format==2)
			$format="%B, %Y";
		if($format==3)
			$format="%B %d, %Y";
		$mt = strftime($format, $time);
		return $mt;
	}
}

// Strip Slashes
function slashe($text)
{
	if(get_magic_quotes_gpc())
		return stripslashes($text);
	else
		return $text;
}

// Get safe text
function safeText($text)
{
	$text=htmlspecialchars(trim($text));
	if(get_magic_quotes_gpc())
		return stripslashes($text);
	else
		return $text;
}

// Backup database
function dump_MySQL($server, $login, $password, $base, $mode, $file)
{
    $connexion = mysql_connect($server, $login, $password);
    mysql_select_db($base, $connexion);
    
    $entete  = "-- ----------------------\n";
    $entete .= "-- dump of DB ".$base." au ".date("d-M-Y")."\n";
    $entete .= "-- ----------------------\n\n\n";
    $creations = "";
    $insertions = "\n\n";
    
    $listeTables = mysql_query("show tables", $connexion);
    while($table = mysql_fetch_array($listeTables))
    {
        // structure or all DB
        if($mode == 1 || $mode == 2)
        {
            $creations .= "-- -----------------------------\n";
            $creations .= "-- Structure `".$table[0]."`\n";
            $creations .= "-- -----------------------------\n";
            $creations .= "DROP TABLE IF EXISTS `".$table[0]."`;\n";
            $alltables = mysql_query("show create table ".$table[0], 
			$connexion);
            while($creationTable = mysql_fetch_array($alltables))
            {
              $creations .= $creationTable[1].";\n\n";
            }
        }
        // data or all
        if($mode > 1)
        {
            $donnees = mysql_query("SELECT * FROM ".$table[0]);
            $insertions .= "-- -----------------------------\n";
            $insertions .= "-- Content ".$table[0]."\n";
            $insertions .= "-- -----------------------------\n";
            while($nuplet = mysql_fetch_array($donnees))
            {
                $insertions .= "INSERT INTO `".$table[0]."` VALUES(";
                for($i=0; $i < mysql_num_fields($donnees); $i++)
                {
                  if($i != 0)
                     $insertions .=  ", ";
                  if(mysql_field_type($donnees, $i) == "string" || mysql_field_type($donnees, $i) == "time" || mysql_field_type($donnees, $i) == "datetime" || mysql_field_type($donnees, $i) == "date" || mysql_field_type($donnees, $i) == "blob")
                     $insertions .=  "'";
                  $insertions .= addslashes($nuplet[$i]);
                  if(mysql_field_type($donnees, $i) == "string" || mysql_field_type($donnees, $i) == "time" || mysql_field_type($donnees, $i) == "datetime" || mysql_field_type($donnees, $i) == "date" || mysql_field_type($donnees, $i) == "blob")
                    $insertions .=  "'";
                }
                $insertions .=  ");\n";
            }
            $insertions .= "\n";
        }
    }
 
    mysql_close($connexion);
 
    $fileDump = fopen('sav/'.$file, "wb");
    fwrite($fileDump, $entete);
    fwrite($fileDump, $creations);
    fwrite($fileDump, $insertions);
    fclose($fileDump);    
}

?>