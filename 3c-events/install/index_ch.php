<?php
#### Name of this file: install/index.php 
#### Description: Installation file of the script, note that the configuration information will be saved in the file includes/config.php and not in a Database

require_once('../includes/Functions.php');

//configuration file contents
$config = file_get_contents('../includes/config.php');

//function to update the configuration file
function regval($key, $value)
{
	global $config;
	$config = preg_replace('#define\("'.$key.'", "(.*)"\);#isU','define("'.$key.'", "'.$value.'");', $config);
	file_put_contents('../includes/config.php', $config);
}

$error='';
$confirm='';

$path = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; //path to script
$path = preg_replace('#(.+)/install(.*)$#isU', '$1', $path);
$pathdemo = str_replace('/3c-events','/events-list.php',$path); //path to demo
$pathadmin = $path.'/admin'; //path to admin
		
if (!empty($_POST['dname']) && !empty($_POST['duser']))
{
	$dhost = trim($_POST['dhost']); //database host
	$dname = trim($_POST['dname']); //datbase name
	$duser = trim($_POST['duser']); //database user
	$dpassword = trim($_POST['dpassword']); //database password
	$auser = trim($_POST['auser']); //admin username
	$apassword = trim($_POST['apassword']); //admin password
	
	try
	{
		$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		$bdd = new PDO('mysql:host='.$dhost.';dbname='.$dname, $duser, $dpassword, $pdo_options); $bdd->query("SET NAMES UTF8"); 
	}
	catch(Exception $e)
	{
			$error = 'Could not connect to the Database';
	}

	if(empty($error))
	{
		//update file config.php
		regval('HOST', $dhost);
		regval('DB', $dname);
		regval('DBUSER', $duser);
		regval('DBPASS', $dpassword);
		regval('PATH_CAL', $path);
		regval('PATH_ADMIN', $pathadmin);
		regval('USERNAME', $auser);
		regval('PASSWORD', $apassword);		
		
		$tb="DROP TABLE IF EXISTS `3ce_event`;"; $req = $bdd->exec($tb);
		$tb="DROP TABLE IF EXISTS `3ce_users`;"; $req = $bdd->exec($tb);
		$tb="DROP TABLE IF EXISTS `3ce_votes`;"; $req = $bdd->exec($tb);
		
		// creating the event table
		$table = "CREATE TABLE IF NOT EXISTS `3ce_event` (
				  `id` int(11) NOT NULL auto_increment,
				  `name` varchar(300) collate utf8_bin NOT NULL,
				  `icon` varchar(255) collate utf8_bin NOT NULL,
				  `icon_only` tinyint(4) NOT NULL default '0',
				  `content` text collate utf8_bin NOT NULL,
				  `place` varchar(300) collate utf8_bin NOT NULL,
				  `date` varchar(500) collate utf8_bin NOT NULL,
				  `stime` varchar(20) collate utf8_bin NOT NULL,
				  `etime` varchar(20) collate utf8_bin NOT NULL,
				  `repeat_day` tinyint(1) NOT NULL default '0',
				  `repeat_month` tinyint(1) NOT NULL default '0',
				  `repeat_year` tinyint(1) NOT NULL default '0',
				  `status` tinyint(1) NOT NULL default '1',
				  `map` tinyint(4) NOT NULL default '0',
				  `id_user` int(11) NOT NULL default '0',
				  PRIMARY KEY  (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;";
		
		// creating the users table
		$table2 = "CREATE TABLE IF NOT EXISTS `3ce_users` (
		  `id` int(11) NOT NULL auto_increment,
		  `username` varchar(255) NOT NULL,
		  `password` varchar(255) NOT NULL,
		  `email` varchar(255) NOT NULL,
		  `joindate` date NOT NULL,
		  `lastlogin` date NOT NULL,
		  `status` tinyint(1) NOT NULL default '1',
		  `autoapprove` tinyint(1) NOT NULL default '0',
		  PRIMARY KEY  (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;";
			
		// creating the vote table
		$table3 = "CREATE TABLE IF NOT EXISTS `3ce_votes` (
		  `id` int(11) NOT NULL auto_increment,
		  `item_id` int(11) default NULL,
		  `vote_value` int(11) default NULL,
		  `ip` varchar(255) default NULL,
		  `date` timestamp NOT NULL default CURRENT_TIMESTAMP,
		  PRIMARY KEY  (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;";
			
		$req = $bdd->exec($table);
		$req = $bdd->exec($table2);
		$req = $bdd->exec($table3);

		//ok, it's all ;)
		$confirm = 'Congratulations, Script Successfully Installed :)<br /><br />Your Calendar: <a href="'.$pathdemo.'" target="_blank">Click Here</a> - Administration: <a href="'.$pathadmin.'" target="_blank">Click Here</a>';
		$error = 'Please delete or rename install folder from your ftp.<br />';
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8;charset=utf-8" /> 
	
	<title>3C-Events :: Auto Installer</title>

	<link type="text/css" href="../assets/admin-skin/style.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/base/jquery-ui.css" />

	<script src="./jquery.js" type="text/javascript"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/jquery-ui.min.js" type="text/javascript"></script>
	<script src="./jquery.validate.js" type="text/javascript"></script>

<script id="demo" type="text/javascript">
$(document).ready(function() {
	var validator = $("#signupform").validate({
		errorPlacement: function(label, element) {
				label.insertAfter(element);
		}
	});
	
	// overwrite focusInvalid to activate tab with invalid elements
	validator.focusInvalid = function() {
		if( this.settings.focusInvalid ) {
			try {
				var focused = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible");
				focused.focus();
			} catch(e) {
				// ignore IE throwing errors when focusing hidden elements
			}
		}
	};
});
</script>

<style type="text/css">
	label {color:#000; font-family:Arial, Helvetica, sans-serif; font-size:12px;}
	label.error {color:red; margin-left:0em; width:20em; background:none; border:0;}
	.note-success{background-position:17px 35px;}
</style>	

</head>

<body style="background:#ccc">
	<div style="width:600px; margin:2% auto;">
		<img src="../assets/admin-skin/img/logo.png" alt="" />
		<div class="inner" style="background:#eee; border:1px solid #505050; padding:20px; border-radius:8px">
		
			<?php if(strlen($confirm)==0) echo'<h4><img src="icons/smile.png" alt="smile" style="float:left; margin-right:10px" /><u>Thank You For Your Purchase!!</u><br /> Please follow these steps to install the Script.</h4>'; ?>		 
			<?php echo Ok($confirm).Error($error); ?>
			<?php if(strlen($confirm)==0){?>
			<form action="" method="post" id="signupform">
				<div style="border-bottom:1px dashed #aaa; margin-bottom:15px">
					<img src="icons/database_connect.png" style="position:relative; top:2px" alt="icon" />  
					<label for="dhost" ><strong>Database Host</strong></label><br />
					<input type="text" name="dhost" id="dhost" value="localhost" class="required"/><br />
					
					<img src="icons/database.png" style="position:relative; top:2px" alt="icon" /> 
					<label for="dname" ><strong>Database Name</strong></label><br />
					<input type="text" name="dname" id="dname" class="required" /><br />
					
					<div style="float:left">
						<img src="icons/user.png" style="position:relative; top:2px" alt="icon" /> 
						<label for="duser" ><strong>Database User</strong></label><br />
						<input type="text" name="duser" id="duser" style="width:110px" class="required" />
					</div>
					
					<div style="float:left">
						<img src="icons/password.png" style="position:relative; top:2px" alt="icon" /> 
						<label for="dpassword" ><strong>Database Password</strong></label><br />
						<input type="password" name="dpassword" id="dpassword" style="width:110px"/>
					</div>
					<div style="clear:both"></div>
					<br />
				</div>
				
				<div style="border-bottom:1px dashed #aaa; margin-bottom:15px">
					<img src="icons/user.png" style="position:relative; top:2px" alt="icon" /> 
					<label for="auser" ><strong>Admin Username</strong></label><br />
					<input type="text" name="auser" id="auser" class="required" minlength="3" maxlength="20" /><br />
					
					<div style="float:left">
						<img src="icons/password.png" style="position:relative; top:2px" alt="icon" /> 
						<label for="apassword" ><strong>Admin Password</strong></label><br />
						<input type="password" name="apassword" id="apassword" style="width:110px" class="required" minlength="4" maxlength="50" /><br />
					</div>
					
					<div style="float:left">
						<img src="icons/password.png" style="position:relative; top:2px" alt="icon" /> 
						<label for="apassword" ><strong>Confirm Password</strong></label><br />
						<input type="password" name="apassword" id="apassword"  style="width:110px" class="required" equalTo="#apassword" /><br />
					</div>
					<div style="clear:both"></div>
					<br />
				</div>
				
				<div style="border-bottom:1px dashed #aaa; margin-bottom:15px">
					<img src="icons/path.png" style="position:relative; top:2px" alt="icon" /> 
					<label for="path" ><strong>Path To Script</strong></label><br />
					<input type="text" name="path" id="path" class="required" value="<?php echo $path ?>" /><br />
					
					<img src="icons/path.png" style="position:relative; top:2px" alt="icon" /> 
					<label for="pathadmin" ><strong>Path To Folder admin</strong></label><br />
					<input type="text" name="pathadmin" id="pathadmin" class="required" value="<?php echo $pathadmin ?>" /><br /><br />
				</div>
				
				<div style="border-bottom:1px dashed #aaa; margin-bottom:15px">
				<?php
					if ( !extension_loaded('pdo') )
						echo '<strong style="color:red">PDO Unavailable!</strong><br />';
					else
						echo '<strong style="color:green">PDO Available.</strong><br />';
						
					if ( !extension_loaded('curl') )
						echo '<strong style="color:red">CURL Unavailable!</strong><br />';
					else
						echo '<strong style="color:green">CURL Available.</strong><br />';
					
					if(( extension_loaded('pdo') ) && ( extension_loaded('pdo') ))	
						echo '<strong style="color:green">The script can run correctly.</strong>';	
					else	
						echo '<strong style="color:red">The script can not run correctly!</strong>';							
			
				?>
				<br /><br />
				</div>
				
				<input type="submit" value="Install" style="float:right" />
				<br />	
			</form>
			<?php } ?>
		</div>
	</div>
</body>
</html>