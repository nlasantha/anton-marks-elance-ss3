<?php		
#### Name of this file: events.php 
#### Description: display events

require('includes/config.php');
include("includes/GoogleMap/GoogleMap.php");
include("includes/GoogleMap/JSMin.php");
include('includes/lang/'.LANG.'.php');
include('includes/Functions.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		 <title>3C-Events :: Events Calendar</title>	
	
		<script type="text/javascript" src="assets/javascript/jquery-1.7.1.min.js" ></script>
		<script type="text/javascript" src="assets/javascript/plugin.js"></script>
		<link rel="stylesheet" href="assets/home-skin/calendar.css"  type="text/css" />
		<link rel="stylesheet" href="assets/home-skin/plugin.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="assets/home-skin/color/<?php echo SKIN ?>.css"  type="text/css" />
		<style type="text/css">body{background:#fff}</style>
	</head>
	<body style="margin:0; padding:0;">
			<?php 
			
			//show events
				try
				{
					$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
					$bdd = new PDO('mysql:host='.HOST.';dbname='.DB, DBUSER, DBPASS, $pdo_options); $bdd->query("SET NAMES UTF8");
					
					//display the event (result of a search)
					if(!empty($_GET['id_event']))
					{
						$id = intval($_GET['id_event']);
						
						$req = $bdd->query("SELECT * 
						FROM 3ce_event 
						WHERE status=1 AND id=$id");
						$row = $req->fetch();
						$req->closeCursor();
						$map=$row['map'];
						
						// Display google map, if display map of this event is checked 
						if($map==1)
						{
							$MAP_OBJECT = new GoogleMapAPI(); $MAP_OBJECT->_minify_js = isset($_REQUEST["min"])?FALSE:TRUE;
							$MAP_OBJECT->addMarkerByAddress($row['place'],$row['name'], $row['date']);

							$STREETVIEW_ID = "map_streetview";
							$MAP_OBJECT->enableStreetViewControls();
							$MAP_OBJECT->attachStreetViewContainer($STREETVIEW_ID);
							
							echo $MAP_OBJECT->getHeaderJS();
							echo $MAP_OBJECT->getMapJS();
							?>
							<style type="text/css">
							#<?php echo $STREETVIEW_ID;?>{
							width:500px;
							height:300px;
							}
							</style>
							
						<?php echo $MAP_OBJECT->printOnLoad();
						 echo $MAP_OBJECT->printMap();
						}
					}

				}
				catch(Exception $e)
				{
						exit('Database Error: '.$e->getMessage());
				}
			?>
	</body>
</html>