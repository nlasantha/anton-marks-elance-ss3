<?php

class PayerHavingReceipt extends DataExtension{

	private static $db = array(
		'Street' =>		'Varchar',
		'Suburb' =>		'Varchar',
		'CityTown' =>	'Varchar',
		'Country' =>	'Varchar',
	);

	
	function ReceiptMessage(){
		return $this->owner->renderWith('Payer_receipt');
	}
}