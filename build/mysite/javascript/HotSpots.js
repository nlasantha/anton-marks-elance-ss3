(function($) {
    $.entwine('ss', function($){
        function link(){
            if($("select[name=LinkType]").val() == 'Internal') {
                $('#ExternalLink').hide();
                $('#LinkPageID').show();
            } else if($("select[name=LinkType]").val() == 'External') {
                $('#ExternalLink').show();
                $('#LinkPageID').hide();
            } else if($("select[name=LinkType]").val() == 'None') {
                $('#ExternalLink').hide();
                $('#LinkPageID').hide();
            }
        };

        $("select[name=LinkType]").entwine({

            onmatch: function() {
                link();
            },
            onunmatch: function() {
                this._super();
            },
            onclick: function() {
                this.toggle();
            },
            onload: function() {
                link();
            },
            onchange: function() {
                link();
            }
        });
    });
})(jQuery);
