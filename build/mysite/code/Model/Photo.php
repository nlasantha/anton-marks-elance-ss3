<?php
/**
 * Created by PhpStorm.
 * User: Nuwan
 * Date: 5/8/14
 * Time: 5:28 PM
 */
class Photo extends Media{

    private static $db = array(
        'SortOrder'			=>  'Int',
    );
    private static $has_one = array(
        'Image'     => 'MediaImage',

    );

    private static $summary_fields = array(
        'Title',
        'Image',

    );
    private static $default_sort = 'SortOrder';
    public function getCMSFields(){
        $fields = parent::getCMSFields();
        $fields ->removeByName('SortOrder');
        $fields->addFieldsToTab("Root.Main", new TextField('Title', 'Title'));

        FormUtils::ReArrangeFormFieldsInTab('Root.Main',$fields,array(
            'Title',
            'Image',

        ));
        return $fields;
    }

}


class MediaImage extends Image {
    function generateThumbnail(&$gd){
        return $gd->croppedResize(120,90);
    }
}