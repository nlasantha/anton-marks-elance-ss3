<?php
/**
 * Created by PhpStorm.
 * User: Nuwan
 * Date: 5/8/14
 * Time: 5:32 PM
 */

class Video extends Media {

    private static $db = array(
        'VideoCode'     => 'Varchar',
        'VideoType'     => "Enum('YouTube, Vimeo, None','None')",
        'SortOrder'	    =>  'Int',
    );

    private static $summary_fields = array(
        'Title',
        'VideoCode',
        'VideoType',

    );
    private static $default_sort = 'SortOrder';
    public function getCMSFields(){
        $fields = parent::getCMSFields();
        $fields ->removeByName('SortOrder');
        $fields->addFieldsToTab("Root.Main", new TextField('Title', 'Title'));
        $fields->addFieldsToTab("Root.Main", new TextField('VideoCode', 'VideoCode'));
        $fields->addFieldsToTab('Root.Main', new DropdownField('VideoType', 'VideoType', singleton('Video')->dbObject('VideoType')->enumValues()));
        FormUtils::ReArrangeFormFieldsInTab('Root.Main',$fields,array(
            'Title',
            'VideoType',
            'VideoCode',

        ));
        return $fields;
    }

    public function ImageUrl() {
        if ($this->VideoType == 'YouTube') {
            $iurl = "http://img.youtube.com/vi/" . $this->VideoCode . "/default.jpg";
        }
        if ($this->VideoType == 'Vimeo') {
            $imgid = $this->VideoCode;
            $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$imgid.php"));
            $iurl = $hash[0]['thumbnail_medium'];
        }
        return $iurl;
    }

    public function VideoUrl() {
        if ($this->VideoType == 'YouTube') {

            $vurl = "http://www.youtube.com/watch?v=" . $this->VideoCode;
        }
        if ($this->VideoType == 'Vimeo') {
            $vurl = "http://vimeo.com/".$this->VideoCode;

        }
        return $vurl;
    }

}
