<?php
/**
 * Created by PhpStorm.
 * User: Nuwan
 * Date: 5/5/14
 * Time: 4:16 PM
 */
class Slide extends DataObject{

    private static $db = array(
        'Title'             =>  'Varchar',
        'LinkType'          =>  "Enum('None,Internal,External')",
        'ExternalLink'		=>  'Varchar',
        'SortOrder'			=>  'Int',
        'LinkPageID'        =>  'Int',
    );

    private static $has_one = array(
        'Image'             =>  'Image',
        'HomePage'          =>  'HomePage'
    );

    private static $summary_fields = array(
        'Title',
        'Thumbnail',

    );

    private static $default_sort = 'SortOrder';

    public function getCMSFields(){

        Requirements::javascript('mysite/javascript/HotSpots.js');

        $fields = parent::getCMSFields();
        $fields->removeByName('HomePageID');
        $fields ->removeByName('InternalLinkID');
        $fields ->removeByName('SortOrder');
        $fields->addFieldsToTab('Root.Main', new TreeDropdownField('LinkPageID','Link','SiteTree'));
        if($image = $fields->dataFieldByName('Image'))
            $image->setTitle('Image (746 x 233 px)');

        return $fields;
    }


    public function Link(){
        if($this->LinkType == 'Internal' && $this->LinkPageID){
            if($page = SiteTree::get()->byId($this->LinkPageID))
                $strRet = $page->Link();
        }else
            $strRet = 'http://'.$this->ExternalLink;
        return $strRet;
    }

    function Thumbnail(){
        return $this->ImageID ? $this->Image()->CroppedImage(200, 100) : null;
    }
}