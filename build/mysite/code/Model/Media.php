<?php
/**
 * Created by PhpStorm.
 * User: Nuwan
 * Date: 5/8/14
 * Time: 5:32 PM
 */
class Media extends DataObject {
    private static $db = array(
        'Title'         => 'Varchar',
        'Tags'          => 'Text',
        'Description'   => 'HTMLText'
    );

    private static $has_one = array(
        'Page'          => 'Page'
    );

    public function getCMSFields(){
        $fields = parent::getCMSFields();
        $fields ->removeByName('PageID');
        return $fields;
    }
}