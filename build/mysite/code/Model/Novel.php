<?php
/**
 * Created by PhpStorm.
 * User: Nuwan
 * Date: 5/6/14
 * Time: 7:48 AM
 */

class Novel extends DataObject{

    private static $db = array(
        'LeftTitle'         => 'Varchar',
        'LeftContent'       => 'Text',
        'RightTitle'        => 'Varchar',
        'RightContent'      => 'Text',
        'LeftLinkPageID'    => 'Int',
        'RightLinkPageID'   => 'Int',
        'SortOrder'			=> 'Int',
    );

    private  static $has_one = array(
        'LeftImage'         => 'Image',
        'RightImage'        => 'Image',
        'HomePage'          => 'HomePage'
    );

     private static $summary_fields = array(
    'LeftTitle',
    'LeftContent',
    'LeftThumbnail',
    'RightTitle',
    'RightContent',
    'RightThumbnail',

);

    private static $default_sort = 'SortOrder';

    public function getCMSFields(){

        $fields = parent::getCMSFields();

        $fields ->removeByName('SortOrder');

        $fields->addFieldsToTab('Root.Main', new TreeDropdownField('LeftLinkPageID','LeftLink','SiteTree'));
        $fields->addFieldsToTab('Root.Main', new TreeDropdownField('RightLinkPageID','RightLink','SiteTree'));

        FormUtils::ReArrangeFormFieldsInTab('Root.Main',$fields,array(
            'LeftTitle',
            'LeftContent',
            'LeftLinkPageID',
            'LeftImage',
            'RightTitle',
            'RightContent',
            'RightLinkPageID',
            'RightImage',
        ));

        if($image = $fields->dataFieldByName('LeftImage'))
            $image->setTitle('LeftImage (125 x 202 px)');
        if($image = $fields->dataFieldByName('RightImage'))
            $image->setTitle('RightImage (125 x 202 px)');

        return $fields;
    }


    public function LeftLink(){
        if($this->LeftLinkPageID){
            if($page = SiteTree::get()->byId($this->LeftLinkPageID))
                $strRet = $page->Link();
        }
        return $strRet;
    }
    public function RightLink(){
        if($this->RightLinkPageID){
            if($page = SiteTree::get()->byId($this->RightLinkPageID))
                $strRet = $page->Link();
        }
        return $strRet;
    }

    function LeftThumbnail(){
        return $this->LeftImageID ? $this->LeftImage()->CroppedImage(125, 202) : null;
    }
    function RightThumbnail(){
        return $this->RightImageID ? $this->RightImage()->CroppedImage(125, 202) : null;
    }
}
