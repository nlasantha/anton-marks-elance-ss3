<?php
/**
 * Created by Nivanka Fonseka (nivanka@silverstripers.com).
 * User: nivankafonseka
 * Date: 2/22/14
 * Time: 9:04 AM
 * To change this template use File | Settings | File Templates.
 */

class FormUtils {

	/**
	 * @param $fieldsList
	 * @param $arrFields
	 *
	 * Takes a field list in an array and remove all those fields from the
	 * FieldList
	 */
	public static function RemoveFormFields(FieldList $fieldsList, $arrFields){
		foreach($arrFields as $strField)
			$fieldsList->removeByName($strField);
	}

	public static function ReArrangeFormFieldsInTab($strTabName, FieldList $fields, $arrOrder){
		$tab = $fields->findOrMakeTab($strTabName);

		$arrFields = array();
		foreach($tab->Fields() as $formField){
			$arrFields[$formField->getName()] = $formField;
			$tab->removeByName($formField->getName());
		}

		foreach($arrOrder as $strFieldName){
			if(array_key_exists($strFieldName, $arrFields)){
				$tab->Fields()->push($arrFields[$strFieldName]);
			}
		}
	}


	public static function ReArrangeFormFieldsToTabs(FieldList $fields, $arrOrder){
		$arrFields = array();
		if($fields->hasTabSet()){
			$tabSet = $fields->first();
			if(get_class($tabSet) == 'TabSet'){
				foreach($tabSet->Tabs() as $tab){
					foreach($tab->Fields() as $field){
						$arrFields[$field->getName()] = $field;
					}
					$tabSet->removeByName($tab->getName());
				}
			}
		}else{
			foreach($fields as $field){
				$arrFields[$field->getName()] = $field;
			}
		}

		foreach($arrOrder as $strTabName => $arrFieldsForTab){

			$tab = $fields->findOrMakeTab('Root.' . $strTabName);

			foreach($arrFieldsForTab as $strFieldName){
				if(array_key_exists($strFieldName, $arrFields))
					$tab->push($arrFields[$strFieldName]);
			}

		}

	}

	/**
	 * @param FieldList $fields
	 * @param $arrFields
	 * @param $dataObject
	 * Takes a list of field names in array and do a simple replace of those with
	 * literal form fields
	 */
	public static function DoReadOnlyTransformationLiteralFields(FieldList $fields, $arrFields, $dataObject){
		foreach($arrFields as $strField){
			if($field = $fields->dataFieldByName($strField)){
				$strLabel = $strField->Title();
				$strValue = $dataObject->getField($strField);
				$fields->replaceField($strField, new LiteralField($strField, "<div class='field'>
					<label class='left'>{$strLabel}</label>
					<div class='middleColumn'>
						<p>$strValue</p>
					</div>
				</div>"));
			}
		}
	}

	/**
	 * @param $strName
	 * @param $strLabel
	 * @param $strValue
	 * @return LiteralField
	 */
	public static function GetCMSFieldTypeLiteralField($strName, $strLabel, $strValue){
		return new LiteralField($strName, "<div class='field'>
			<label class='left'>{$strLabel}</label>
			<div class='middleColumn'>
				<p>$strValue</p>
			</div>
		</div>");
	}


	/**
	 * @param $strName
	 * @param $strTitle
	 * @param $dlList
	 * @param $strSortColumn
	 *
	 * make and return a grid field with the components for drag and drop reordering
	 */
	public static function MakeDragAndDropGridField($strName, $strTitle, $dlList, $strSortColumn, $strType = 'RelationEditor', $iPagination = 20){

		if($strType == 'RecordEditor'){
			$gridConfigs = new GridFieldConfig_RecordEditor($iPagination);
		}else{
			$gridConfigs = new GridFieldConfig_RelationEditor($iPagination);
		}
		$gridConfigs->addComponent(new GridFieldSortableRows($strSortColumn));
		return new GridField($strName, $strTitle, $dlList, $gridConfigs);
	}

	/**
	 * @param $strName
	 * @param $strTitle
	 * @param $dlList
	 *
	 */
	public static function MakeRelationSelector($strName, $strTitle, $dlList, $iPagination = 20){
		$gridConfigs = new GridFieldConfig_RelationEditor($iPagination);
		$gridConfigs->removeComponentsByType('GridFieldAddNewButton');
		return new GridField($strName, $strTitle, $dlList, $gridConfigs);
	}


}