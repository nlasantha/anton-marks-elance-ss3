<?php
/**
 * Created by PhpStorm.
 * User: Nuwan
 * Date: 5/6/14
 * Time: 4:33 PM
 */
class BlogEntryDecorator extends DataExtension {

    private static $db = array(
        'BlogSummary'        => 'Text',
        'ExtraVideoFile'     => 'Varchar(250)',
    );

    private static $has_one = array(
        'BlogImage'         => 'Image'
    );

    public function updateCMSFields(FieldList $fields) {
        parent::updateCMSFields($fields);
        $fields->addFieldToTab("Root.Main", new UploadField('BlogImage','Featured Image'), 'Content');
        $fields->addFieldToTab("Root.Main", new TextField('ExtraVideoFile', 'Extra Video File (below page title)'),'Content');
        $fields->addFieldToTab('Root.Main', new TextareaField('BlogSummary', 'Blog Summary'),'Content');
    }

    function BlogEntries($number=7) {
        return DataObject::get('BlogEntry', "", "Date DESC", false, $number);
    }
}
