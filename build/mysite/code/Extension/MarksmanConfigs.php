<?php
/**
 * Created by PhpStorm.
 * User: Nuwan
 * Date: 5/4/14
 * Time: 3:51 PM
 */

class MarksmanConfigs extends DataExtension{

    private static $db = array(
        'MainVideo'                     =>  'Varchar',
        'ReadMyShortStoriesLink'        => 'HTMLVarchar(150)',
        'ReadMyShortStoriesLinkType'    => "Enum('External,Internal')",
    );


    public function updateCMSFields(FieldList $fields){
        $fields->addFieldsToTab('Root.MainVideo', array(
            new TextField('MainVideo','Main Video with(http://www)'),
        ));

        $fields->addFieldToTab('Root.ShortStories', new SelectionGroup('ReadMyShortStoriesLinkType', array(
            'External' => new TextField('ReadMyShortStoriesLink', 'Read My Short Stories Link'),
            'Internal' => new TreeDropdownField('ReadMyShortStoriesLinkPageID', 'Read My Short Stories Link', 'SiteTree' )
        )));

    }

}