<?php
/**
 * Created by PhpStorm.
 * User: Nuwan
 * Date: 5/8/14
 * Time: 11:00 AM
 */
class FutureshotPage extends Page{

    private static $singular_name       = 'Futureshot Page';
    private static $plural_name         = 'Futureshot Pages';
    private static $description         = 'Layout for Futureshot Page ';
    private static $can_be_root         = false;

    private static $db = array(
        'Summary'       => 'Text',
        'Date'          => 'Date',
        'VideoURL'      => 'Varchar(400)'
    );

    private static $has_one = array(
        'Image'         => 'Image',
        'Large_Image'   => 'Image',
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab("Root.Main", new TextField('VideoURL', 'Video/Audio URL'),'Content');
        $fields->addFieldToTab('Root.Main', new UploadField('Image', 'Cover Image(245 x 150 px)'),'Content');
        $fields->addFieldToTab('Root.Main', new UploadField('Large_Image', 'Large Cover Image'),'Content');
        $fields->addFieldToTab("Root.Main", new DateField('Date','Date'),'Content');
        $fields->addFieldToTab("Root.Main", new TextareaField('Summary','Featured Summary'),'Content');
        return $fields;
    }

    function ParagraphSummary() {
        return $this->obj('Content')->FirstParagraph('html');
    }
}

class FutureshotPage_Controller extends Page_Controller{
    public function init() {
        parent::init();
        $videoFile = $this->VideoURL;
        $imageFile = $this->Large_Image()->Filename;
        if ($videoFile != '') {
            Requirements::customScript(<<<JS
               jwplayer("BoomshotVideoHolder").setup({
                    flashplayer: "mysite/thirdparty/jwplayer/player.swf",
                    file: "$videoFile",
                    image: "$imageFile",
                    height: 294,
                    width: 480

               });
JS
            );
        }
    }

    function getYouTubeCode() {
        $url = $this->VideoURL;
        if ($url != '') {
            parse_str(parse_url($url, PHP_URL_QUERY), $vars);
            if (isset($vars['v']))
                return $vars['v'];
            return;
        }
    }
}