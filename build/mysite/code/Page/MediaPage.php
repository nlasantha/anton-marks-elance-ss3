<?php
/**
 * Created by PhpStorm.
 * User: Nuwan
 * Date: 5/8/14
 * Time: 6:12 PM
 */

class MediaPage extends Page {

    private static $db = array(
        'Date' => 'Date',
        'Summary' => 'HTMLText'
    );

    private static $has_one = array(
        'EntryImage' => 'Image'
    );

   private static $has_many = array(
        'Video' => 'Video',
        'Photos' => 'Photo'
    );

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $date = new DateField('Date', 'Date');
        $date->setConfig('showcalendar', true);
        $fields->addFieldToTab("Root.Main", $date );
        $fields->addFieldToTab("Root.Main", new HtmlEditorField('Summary', 'Summary'));
        $fields->addFieldToTab('Root.Main', new UploadField('EntryImage', 'Thumbnail Image'));
        $fields->addFieldToTab("Root.Main", new HtmlEditorField('Content', 'Content'));

        $config = new GridFieldConfig_RecordEditor(20);
        $config->addComponent(new GridFieldSortableRows('SortOrder'));
        $fields->addFieldToTab('Root.Video', new GridField('Video', 'Video', $this->Video(), $config));

        $config = new GridFieldConfig_RecordEditor(20);
        $config->addComponent(new GridFieldSortableRows('SortOrder'));
        $fields->addFieldToTab('Root.Photos', new GridField('Photos', 'Photos', $this->Photos(), $config));

        return $fields;
    }

    public function ThumbnailFromPhoto() {
        if ($this->Photos()) {
            $photo = DataObject::get_one('Photo', 'MediaPageID=' . $this->ID, false, 'RAND()');
            if ($photo) {
                $thumb = $photo->Image()->CroppedImage(120,90);
                return $thumb->getAbsoluteURL();
            }
            return false;
        }
        return false;
    }

    public function ThumbnailFromVideo() {
        if ($this->Video()) {
            $video = DataObject::get_one('Video', 'MediaPageID=' . $this->ID, false, 'RAND()');
            if ($video)
                return $video->ImageUrl();
            return false;
        }
        return false;

    }

    public function Thumb(){
        if($this->Photos()){
            return $this->ThumbnailFromPhoto();
        }
        if($this->Video()){
            return $this->ThumbnailFromVideo();
        }
        return false;
    }

}

class MediaPage_Controller extends Page_Controller {

    public function init() {
        parent::init();
        Requirements::javascript("mysite/javascript/jquery.prettyPhoto.js");
        Requirements::themedCSS('prettyPhoto');
    }

    public function SubMediaPages() {
        return DataObject::get('MediaPage', 'ParentID=' . $this->ID);
    }

}