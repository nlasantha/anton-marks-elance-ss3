<?php
/**
 * Created by PhpStorm.
 * User: Nuwan
 * Date: 5/7/14
 * Time: 7:09 AM
 */
class BoomshotHolderPage extends Page {

    private static $singular_name       = 'Boomshot Holder Page';
    private static $plural_name         = 'Boomshot Holder Pages';
    private static $description         = 'Holder page to hold the boomhsot pages';
    private static $allowed_children    = array("BoomshotPage");

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeByName('Content');
        $fields->removeByName('Widgets');
        return $fields;
    }

}

class BoomshotHolderPage_Controller extends Page_Controller {

    public function BoomshotPages() {
        $paginatedItems = new PaginatedList(BoomshotPage::get(), $this->request);
        $paginatedItems->setPageLength(6);
        return $paginatedItems;
    }
}