<?php

class ScreenshotHolderPage extends Page {

    private static $singular_name       = 'Screen shot Holder Page';
    private static $plural_name         = 'Screen shot Holder Pages';
    private static $description         = 'List Screen shot Page';
    private static $allowed_children    = array("ScreenshotPage");

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeByName('Content');
        return $fields;
    }
}

class ScreenshotHolderPage_Controller extends Page_Controller {

    public function ScreenshotPages() {
        $paginatedItems = new PaginatedList(ScreenshotPage::get(), $this->request);
        $paginatedItems->setPageLength(6);
        return $paginatedItems;
    }
}

