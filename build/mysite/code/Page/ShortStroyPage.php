<?php

class ShortStoryPage extends Page {
    private static $singular_name       = 'Short Story Page';
    private static $plural_name         = 'Short Story Pages';
    private static $description         = 'Short Story Page';
    private static $allowed_children    = "none";
    private static $can_be_root         = false;

    static $has_one = array(
        'Banner' => 'Image'
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', new UploadField('Banner', 'Banner Image(746, 250)'),'Content');
        return $fields;
    }
    
    function ParagraphSummary() {
            return $this->obj('Content')->FirstParagraph('html');
    }

}

class ShortStoryPage_Controller extends Page_Controller {

    public function init() {
        parent::init();
    }
}


