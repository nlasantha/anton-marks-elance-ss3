<?php
class Page extends SiteTree {

    private static $has_one = array(
        'Sidebar' => 'WidgetArea'
    );
    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Widgets', new WidgetAreaEditor('Sidebar'));
        return $fields;
    }

}
class Page_Controller extends ContentController {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();
        Requirements::javascript('mysite/javascript/jquery');
        Requirements::javascript('mysite/javascript/modernizr.js');
        Requirements::javascript('mysite/javascript/selectivizr.js');
        Requirements::themedCSS('style.css');
        Requirements::themedCSS('ProductGroup');
        //  Requirements::javascript('mysite/javascript/jquery-1.6.4.min.js');
        Requirements::javascript('mysite/javascript/jquery.pixelentity.shiner.min.js');


        Requirements::javascript('mysite/javascript/jquery-ui-1.8.16.custom.min.js');

        Requirements::customScript(<<<JS
                jQuery(document).ready(function() {
                   jQuery('.peShiner').peShiner({duration: 6, reverse: true});
                   jQuery('#SearchForm_SearchForm_Search').val('Search...');
                   jQuery('#SearchForm_SearchForm_Search').focus( function(){
                        if( jQuery(this).val() == 'Search...'){
                            jQuery(this).val('');
                        }
                    });
                    jQuery('#SearchForm_SearchForm_Search').blur( function(){
                        if( jQuery(this).val() == ''){
                            jQuery(this).val('Search...');
                        }
                    });

                   jQuery('#awf_field-33460937').focus( function(){
                        if( jQuery(this).val() == 'Name'){
                            jQuery(this).val('');
                        }
                    });
                    jQuery('#awf_field-33460937').blur( function(){
                        if( jQuery(this).val() == ''){
                            jQuery(this).val('Name');
                        }
                    });

                    jQuery('#awf_field-33460938').focus( function(){
                        if( jQuery(this).val() == 'Email'){
                            jQuery(this).val('');
                        }
                    });
                    jQuery('#awf_field-33460938').blur( function(){
                        if( jQuery(this).val() == ''){
                            jQuery(this).val('Email');
                        }
                    });

                    jQuery('#BlogArchive').accordion();

               });
JS
        );
        $config = SiteConfig::current_site_config();
        $videoFile = $config->MainVideo;
        Requirements::javascript('mysite/thirdparty/jwplayer/jwplayer.js');
        Requirements::customScript(<<<JS
               jwplayer("HeaderVideo").setup({
                    flashplayer: "mysite/thirdparty/jwplayer/player.swf",
                    file: "$videoFile",
                    height: 294,
                    width: 480
               });
JS
        );
        $extraVideoFile = $this->ExtraVideoFile;
        if ($extraVideoFile != '') {
            Requirements::customScript(<<<JS
               jwplayer("ExtraVideoHolder").setup({
                    flashplayer: "mysite/thirdparty/jwplayer/player.swf",
                    file: "$extraVideoFile",
                    height: 400,
                    width: 600

               });
JS
            );
        }
    }
    function myevents_file_include()
    {
        include_once("http://www.anton-marks.com/3c-events/includes/Calendar/Calendar.class.php");
        echo Calendar::css();
        echo Calendar::js();
    }

    function results($data, $form) {
        $data = array(
            'Results' => $form->getResults(),
            'Query' => $form->getSearchQuery(),
            'Title' => 'Search Results'
        );
        $this->Query = $form->getSearchQuery();

        return $this->customise($data)->renderWith(array('Page_results', 'Page'));
    }
    function NewsletterSignUpForm() {
        $fields = new FieldSet(
            new TextField("FirstName", null, "Name"),
            new EmailField("Email", null, "Email")
        );

        $actions = new FieldSet(
            new FormAction("doform", "Signup")
        );

        $required = new RequiredFields(
            "FirstName",
            "Email"
        );

        return new Form($this, "dosignupform", $fields, $actions, $required);
    }
    public function BlogArchive(){
        $dlBlogItems = BlogItem::get()->sort('Created');
        return GroupedList::create($dlBlogItems);
    }
}
