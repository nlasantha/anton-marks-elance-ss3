<?php

class ShortStoriesHolderPage extends Page {
    private static $singular_name       = 'Short Stories Holder Page';
    private static $plural_name         = 'Short Stories Holder Pages';
    private static $description         = 'List Short Stories ';
    private static $allowed_children    = array("ShortStoryPage");


    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeByName('Content');
        return $fields;
    }

}

class ShortStoriesHolderPage_Controller extends Page_Controller {

    public function ShortStories() {
        $paginatedItems = new PaginatedList(ShortStoryPage::get(), $this->request);
        $paginatedItems->setPageLength(6);
        return $paginatedItems;
    }

}

