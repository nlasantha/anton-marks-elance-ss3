<?php

class ScreenshotPage extends Page {

    private static $singular_name       = 'Screen shot Page';
    private static $plural_name         = 'Screen shot Pages';
    private static $description         = 'Screen shot Page';
    private static $allowed_children    = "none";
    private static $can_be_root         = false;

    private static $db = array(
        'VideoURL' => 'Varchar(400)'
    );
    private static $has_one = array(
        'Image' => 'Image'
    );

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab("Root.Main", new TextField('VideoURL', 'Video/Audio URL'),'Content');
        $fields->addFieldToTab('Root.Main', new UploadField('Image', 'Cover Image'),'Content');
        return $fields;
    }
    
    function ParagraphSummary() {
            return $this->obj('Content')->FirstParagraph('html');
    }

}

class ScreenshotPage_Controller extends Page_Controller {

    public function init() {
        parent::init();
        $videoFile = $this->VideoURL;
        if ($videoFile != '') {
            Requirements::customScript(<<<JS
               jwplayer("BoomshotVideoHolder").setup({
                    flashplayer: "mysite/thirdparty/jwplayer/player.swf",
                    file: "$videoFile",
                    height: 400,
                    width: 600
               });
JS
            );
        }
    }

    function getYouTubeCode() {
        $url = $this->VideoURL;
        if ($url != '') {
            parse_str(parse_url($url, PHP_URL_QUERY), $vars);
            if (isset($vars['v']))
                return $vars['v'];
            return;
        }
    }

}

