<?php
/**
 * Created by PhpStorm.
 * User: Nuwan
 * Date: 5/5/14
 * Time: 1:33 PM
 */
class HomePage extends Page {

    private static $singular_name       = 'Home Page';
    private static $plural_name         = 'Home Pages';
    private static $description         = 'Layout for Home Page ';
    private static $allowed_children    = 'none';

    public static $has_many = array(
        'Slide'         => 'Slide',
        'Novel'         => 'Novel'
    );

    function getCMSFields(){
        $fields = parent::getCMSFields();
        $fields->removeByName('Content');
        $config = new GridFieldConfig_RecordEditor(20);
        $config->addComponent(new GridFieldSortableRows('SortOrder'));
        $fields->addFieldToTab('Root.Slides', new GridField('Slide', 'Slide', $this->Slide(), $config));

        $config = new GridFieldConfig_RecordEditor(20);
        $config->addComponent(new GridFieldSortableRows('SortOrder'));
        $fields->addFieldToTab('Root.Novels', new GridField('Novel', 'Novel', $this->Novel(), $config));

        return $fields;
    }
}


class HomePage_Controller extends Page_Controller {

    public static $allowed_actions = array(
    );

    public function init() {
        parent::init();
        Requirements::javascript('mysite/javascript/jquery.jcarousel.min.js');
        Requirements::customScript(<<<JS
                        jQuery(document).ready(function() {
                            jQuery('#JCarousel').jcarousel({
                                wrap: 'circular'
                            });
                            jQuery('#FeaturedNovelsJCarousel').jcarousel({
                                wrap: 'circular'
                            });
                        });
JS
        );
    }
    function BlogEntries($number=5) {
        return DataObject::get('BlogEntry', "", "Date DESC", false, $number);
    }
}