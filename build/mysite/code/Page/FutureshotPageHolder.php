<?php

class FutureshotPageHolder extends Page {

    private static $singular_name       = 'Futureshot Holder Page';
    private static $plural_name         = 'Futureshot Holder Pages';
    private static $description         = 'Layout for Futureshot Page Holder ';
    private static $allowed_children    = array("FutureshotPage");

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeByName('Content');
        return $fields;
    }

}

class FutureshotPageHolder_Controller extends Page_Controller {

    public function FutureshotPages() {
        $paginatedItems = new PaginatedList(FutureshotPage::get(), $this->request);
        $paginatedItems->setPageLength(6);
        return $paginatedItems;
    }
}

