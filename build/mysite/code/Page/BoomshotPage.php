<?php
/**
 * Created by PhpStorm.
 * User: Nuwan
 * Date: 5/7/14
 * Time: 7:15 AM
 */

class BoomshotPage extends Page {

    private static $singular_name       = 'Boomshot Page';
    private static $plural_name         = 'Boomshot Pages';
    private static $description         = 'Boomshot Page Layout';
    private static $allowed_children    = "none";
    private static $can_be_root         = false;

    private static $db = array(
        'VideoURL'      => 'Varchar(400)'
    );
    private static $has_one = array(
        'Image'         => 'Image',
        'Large_Image'   => 'Image'
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeByName('Widgets');
        $fields->addFieldToTab("Root.Main", new TextField('VideoURL', 'Video/Audio URL'),'Content');
        $fields->addFieldToTab('Root.Main', new UploadField('Image', 'Cover Image(245 x 150 px)'),'Content');
        $fields->addFieldToTab('Root.Main', new UploadField('Large_Image', 'Large Cover Image'),'Content');

        return $fields;
    }

    function ParagraphSummary() {
        return $this->obj('Content')->FirstParagraph('html');
    }

}

class BoomshotPage_Controller extends Page_Controller {

    public function init() {
        parent::init();
        $videoFile = $this->VideoURL;
        $imageFile = $this->Large_Image()->Filename;
        if ($videoFile != '') {
            Requirements::customScript(<<<JS
               jwplayer("BoomshotVideoHolder").setup({
                    flashplayer: "mysite/thirdparty/jwplayer/player.swf",
                    file: "$videoFile",
                    image: "$imageFile",
                    height: 294,
                    width: 480

               });
JS
            );
        }
    }

    function getYouTubeCode() {
        $url = $this->VideoURL;
        if ($url != '') {
            parse_str(parse_url($url, PHP_URL_QUERY), $vars);
            if (isset($vars['v']))
                return $vars['v'];
            return;
        }
    }

}


