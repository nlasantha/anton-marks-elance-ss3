<?php

class FeaturedPage extends Page {

    private static $singular_name       = 'Featured Page';
    private static $plural_name         = 'Featured Pages';
    private static $description         = 'Layout for FeaturedPage ';
    private static $allowed_children    = 'none';

    private static $db = array(
        'PurchaseKindleEditionLink' => 'HTMLVarchar(500)'
    );
    
    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', new TextField('PurchaseKindleEditionLink', 'Purchase Kindle Edition Link'));
        return $fields;
    }
}

class FeaturedPage_Controller extends Page_Controller {
    
}