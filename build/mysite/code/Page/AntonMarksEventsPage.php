<?php

class AntonMarksEventsPage extends Page {

    private static $singular_name       = 'Anton Marks Events Page';
    private static $plural_name         = 'Anton Marks Events Pages';
    private static $description         = 'List the Events ';
    private static $allowed_children    = "none";

    function getCMSFields(){
        $fields = parent::getCMSFields();
        $fields->removeByName('Content');
        $fields->removeByName('Widgets');
        return $fields;
    }
}

class AntonMarksEventsPage_Controller extends Page_Controller {

    public function init() {
        parent::init();
        $this->redirect('http://www.anton-marks.com/AntonMarksEvents.php');
    }

}
       
  

    

