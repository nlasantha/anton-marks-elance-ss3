<?php
/**
 * Created by PhpStorm.
 * User: Nuwan
 * Date: 5/7/14
 * Time: 12:26 PM
 */

class PhotoGalleryPage extends Page {

    private static $db = array(
        'Album_Title'   => 'Varchar(400)',
        'Album_url'     => 'Varchar(400)'
    );


    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab("Root.Album", new TextField('Album_Title', 'Album Title'));
        $fields->addFieldToTab("Root.Album", new TextField('Album_url', 'Album URL'));
        return $fields;
    }


}

class PhotoGalleryPage_Controller extends Page_Controller {

    public function init() {
        parent::init();
        //$config = SiteConfig::current_site_config();
        $Album_link = $this->Album_url;
        Requirements::javascript("http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js");
        Requirements::customScript(<<<JS
                var flashvars = {
                    feed: "$Album_link"
                };
                 var params = {
                        allowFullScreen: "true",
                        allowscriptaccess: "always"
                 };
                swfobject.embedSWF("http://apps.cooliris.com/embed/cooliris.swf?t=1307582197&style=black","Cooliris", "746", "500", "9.0.0", "", flashvars, params);
JS
        );
    }



}