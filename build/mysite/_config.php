<?php

global $project;
$project = 'mysite';

global $databaseConfig;
$databaseConfig = array(
	"type" => 'MySQLDatabase',
	"server" => 'localhost',
	"username" => 'root',
	"password" => '',
	"database" => 'mark',
	"path" => '',
);

// Set the site locale
i18n::set_locale('en_US');
SiteTree::enable_nested_urls();

FulltextSearchable::enable();
Director::set_environment_type('dev');

Security::setDefaultAdmin('admin', 'admin');
CommentingController::add_extension('CommentSpamProtection');

Object::add_extension('BlogEntry', 'BlogEntryDecorator');
//PayPalPayment::set_config_details(
//    'seller_1316080398_biz_api1.silverstripers.com',
//    '1316080435',
//    'AFC5E7Njz2SxtK83ku.fCKKkckkRA2e5scJjDmWaOpgf3dx0Rm3VRQaN'
//);
//if(Director::isDev() || Director::isTest()){
//    PayPalExpressCheckoutPayment::set_test_config("nuwan.ucsc-developer_api1.gmail.com","1400661484","AQU0e5vuZCvSg-XJploSa.sGUDlpAmCbuud2e6N90ukjVlhZSUBWn.en");
//}
//elseif(Director::isLive()){
//    PayPalExpressCheckoutPayment::set_live_config("nuwan.ucsc-developer_api1.gmail.com","1400661484","AQU0e5vuZCvSg-XJploSa.sGUDlpAmCbuud2e6N90ukjVlhZSUBWn.en",false);
//}
//
//username: 'nuwan.ucsc-developer_api1.gmail.com'
//      password: '1400661484'
//      signature: 'AQU0e5vuZCvSg-XJploSa.sGUDlpAmCbuud2e6N90ukjVlhZSUBWn.en'