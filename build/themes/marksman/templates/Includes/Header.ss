<div id="HeaderWrapper">
    <div id="Header">
        <div id="TopSearch">
            $SearchForm
        </div><!--end div#TopSearch -->
           <div id="Logo">
              <div class="peShiner">
                  <a href="$BaseHref">
                      <img src="{$ThemeDir}/images/logo.png"  data-color="red" alt="Marksman Studios"/>
                 </a>
              </div>
            </div> <!--end div#Logo -->
        <div id="LogoText">
            <a href="$BaseHref"><img src="{$ThemeDir}/images/logo-text.png" alt="Marksman Studios"/></a>
        </div><!--end div#Logo -->
        <div id="MainNav">
            <ul>
                <li><a href="{$BaseHref}" id="HomeNav"><span class="text">Home</span><span class="seperator"></span></a></li>
                <li><a href="{$BaseHref}boomshot" id="BoomshotNav"><span class="text">Boomshot</span><span class="seperator"></span></a></li>
                <li><a href="{$BaseHref}photoshot" id="PhotoshotNav"><span class="text">Photoshot</span><span class="seperator"></span></a></li>
                <li><a href="{$BaseHref}screenshot" id="ScreenshotNav"><span class="text">Screenshot</span><span class="seperator"></span></a></li>
                <li><a href="{$BaseHref}futureshot" id="FutureshotNav"><span class="text">Futureshot</span><span class="seperator"></span></a></li>
                <li><a href="{$BaseHref}store" id="StoreNav"><span class="text">Store</span><span class="seperator"></span></a></li>
                <li><a href="{$BaseHref}contact" id="ContactNav"><span class="text">Contact</span><span class="seperator"></span></a></li>
                <li><a href="{$BaseHref}about" id="AboutNav"><span class="text">About</span></a></li>
            </ul>
        </div><!--end div#MainNav -->
        <div id="HeaderVideo">
        </div>
    </div><!--end div#Header -->
</div><!--end div#HeaderWrapper -->
