<div id="FooterWrapper">
    <div id="Footer">
        <div class="hook1">
            <div id="FooterNav">
                <ul>
                    <% loop Menu(1) %>
                    <li><a href="$Link" title="Go to the $Title.XML page" class="$LinkingMode">$MenuTitle.XML</a></li>
                    <% end_loop %>
                </ul>
            </div><!--end div#FooterNav -->
            <div id="FooterCopy">
                <p>
                    <span class="red">&copy; 2011</span> Marksman Studios  <span class="grey">All rights reserved</span>
                </p>
            </div><!--end div#FooterCopy -->
        </div><!--end div.hook1 -->
    </div><!--end div#Footer -->
</div><!--end div#FooterWrapper -->
