<!DOCTYPE html>
<html lang="$ContentLocale">
    <head>
        <% base_tag %>
		<title><% if MetaTitle %>$MetaTitle<% else %>$Title<% end_if %> &raquo; $SiteConfig.Title</title>
		$MetaTags(false)
       	<link rel="shortcut icon" href="{$ThemeDir}/images/favicon.ico" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
                <% require themedCSS(ui-lightness/jquery-ui-1.8.16.custom.css) %>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4f788a0a4f3b5801"></script>
        <%--<script type="text/javascript" src="{$ThemeDir}/js/EcomCart.js"/>--%>
        <link rel="stylesheet" type="text/css" href="themes/marksman/css/style.css" />
        <!--[if IE]>
        <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css" />
        <![endif]-->
        <!--[if IE 8]>
        <link rel="stylesheet" type="text/css" media="screen" href="ie8.css" />
        <![endif]-->
        <!--[if IE 7]>
        <link rel="stylesheet" type="text/css" media="screen" href="css/ie7.css" />
        <![endif]-->
        <!--[if IE 8]>
        <link rel="stylesheet" type="text/css" media="screen" href="ie8.css" />
        <![endif]-->
    </head>
    <body>
        <% include Header %>
        $Layout
        <% include Footer %>
    </body>
</html>
