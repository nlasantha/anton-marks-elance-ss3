<div id="BodyWrapper">
    <div id="Body">
        <div id="LeftColumn">
            <div class="content">
                <% include BreadCrumbs %>
                <h1>$Title</h1>
                <div id="ExtraVideoHolder"></div>
                $TopSummaryContent
                $Content
                $Form
                $Comment
            </div>
            <% if SubMediaPages %>
                <ul>
                    <% loop SubMediaPages %>
                        <li>
                            <div class="blogSummary">
                                <% if EntryImage %>
                                    <div class="left">
                                        $EntryImage.SetSize(245,150)
                                    </div>
                                    <div class="right">
                                        <h2 class="postTitle"><a href="$Link">$MenuTitle</a></h2>
                                        <p class="authorDate">$Date.Long</p>
                                        <% if Summary %>
                                            $Summary
                                        <% else %>
                                            $ParagraphSummary
                                        <% end_if %>
                                        <a href="$Link" title="Read Full Post" class="readmore">Read More</a>
                                    </div>
                                <% else %>
                                    <div class="right fullWidth">
                                        <h2 class="postTitle"><a href="$Link">$MenuTitle</a></h2>
                                        <p class="authorDate">$Date.Long</p>
                                        <% if Summary %>
                                            $Summary
                                        <% else %>
                                            $ParagraphSummary
                                        <% end_if %>
                                        <a href="$Link" title="Read Full Post" class="readmore">Read More</a>
                                    </div>
                                <% end_if %>
                            </div>
                        </li>
                    <% end_loop %>
                </ul>
            <% end_if %>
            <div class="content">
            <% if Photos %>
                <div class="mediaPhotoContainer">
                    <h3>Photos</h3>
                    <ul>
                        <% loop Photos %>
                            <li>
                                <a href="$Image.getAbsoluteURL" rel="prettyPhoto[pp_gal]">
                                    <img src="<% loop Image.Thumbnail %>$getAbsoluteURL<% end_loop %>" alt="$Title"  />
                                </a>
                            </li>
                        <% end_loop %>
                    </ul>
                </div>
             <% end_if %>
            <% if Video %>
                <div class="mediaVideoContainer">
                    <h3>Video</h3>
                    <ul>
                        <% loop Video %>
                        <li>
                            <a href="$VideoUrl" rel="prettyPhoto[pp_vidgal]">
                                <img src="$ImageUrl" alt="$Title" width="120" height="90"/>
                            </a>
                        </li>
                        <% end_loop %>
                    </ul>
                </div>
            <% end_if %>
        </div><!--end div.content -->
    </div><!--end div#LeftColumn -->
    <% include Sidebar %>
</div><!--end div#Body -->
</div><!--end div#BodyWrapper -->